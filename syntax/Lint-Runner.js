var LintRoller = require('../node_modules/lintroller');

var config = {
    verbose          : false,
    stopOnFirstError : false,

    //recursively include JS files in these folders
    filepaths  : [
        '../www/'
    ],

    //but ignore anything in these folders
    exclusions : [
        '../node_modules/',
        '../resources/',
        '../www/.sencha/',
        '../www/touch/',
        '../www/build/',
        '../www/cordova/',
        '../www/bootstrap.js',
        '../www/resources/sass/'
    ],

    linters: [{
        type : 'jslint'
    }, {
        type : 'jshint'
    }, {
        type : 'esprima'
    }]
};

try {
    LintRoller.init(config);
}
catch (e) {
    console.log('\nAn error has been caught:\n\n');
    console.log(e);
    process.exit(1);
}
