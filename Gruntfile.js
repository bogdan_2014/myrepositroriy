module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    connect: {
      server: {
        options: {
          port: 7000,
          base: 'www',
          logger: 'dev',
          hostname: '0.0.0.0',
          //hostname: '192.168.0.103',
          //hostname: '10.10.1.171',
          keepalive: true,
          middleware: function (connect, options) {
            var proxy = require('grunt-connect-proxy/lib/utils').proxyRequest;

            return [
              // the proxy should go first
              proxy,
              // serve static files from www
              connect.static(options.base)
            ];
          }
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-connect-proxy');

  grunt.registerTask('server', function (target) {
    grunt.task.run([
        'configureProxies:server',
        'connect:server:keepalive'
    ]);
  });

  grunt.registerTask('default', ['server']);
};
