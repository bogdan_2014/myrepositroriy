Ext.define('Td.model.Project', {
	extend: 'Ext.data.Model',

	config: {
		fields: [
			{name: 'projectName', type: 'string'}
		],
		identifier:'uuid', // needed to avoid console warnings!
		proxy: {
            type: 'localstorage',
            id: 'td_project'
		},
		autoLoad : true,
		autoSync : true
	}
});
