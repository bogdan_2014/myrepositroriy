Ext.define('Td.model.Tasks', {
	extend: 'Ext.data.Model',

	config: {
		fields: [
			{name: 'projectId', type: 'string'},
			{name: 'id', type: 'string'},
			{name: 'taskDate', type: 'date'},
			{name: 'tasksName', type: 'string'},
			{name: 'taskArea', type: 'string'},
			{name: 'taskType', type: 'string'},
			{name: 'taskStatus', type: 'string'},
			{name: 'taskColor', type: 'string'}

		],
		identifier:'uuid', // needed to avoid console warnings!
		proxy: {
            type: 'localstorage',
            id: 'td_tasks'
		},
		autoLoad : true,
		autoSync : true
		}
});