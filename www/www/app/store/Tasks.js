Ext.define('Td.store.Tasks', {
	extend:'Ext.data.Store',
	requires: [
		'Td.model.Tasks'
	],
	config: {
		model: 'Td.model.Tasks',
		storeId: 'tasksStore'
	}

});