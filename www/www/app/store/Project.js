Ext.define('Td.store.Project', {
	extend:'Ext.data.Store',
	requires: [
		'Td.model.Project'
	],
	config: {
		model: 'Td.model.Project',
		storeId: 'projectStore'
	}

});
