Ext.define('Td.view.TaskList', {
    extend: 'Ext.List',
    config: {
        fullscreen: true,
        itemId: 'taskList',
        itemTpl: new Ext.XTemplate(
            "<div>{tasksName}</div>"
        ),
        rightButton:  null,
        deferEmptyText : true,
        projectId: null,
        emptyText : 'Список пуст, нажмите на кнопку + чтобы добавить задачу'
    }
});
