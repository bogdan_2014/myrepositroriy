Ext.define('Td.view.ProjectList', {
    extend: 'Ext.List',
    config: {
        fullscreen: true,
        itemId: 'projectList',
        itemTpl: new Ext.XTemplate(
            "<div>{projectName}</div>"
        ),
        rightButton:  null,
        deferEmptyText : true,
        emptyText : 'Список пуст, нажмите на кнопку + чтобы добавить проект'
    }
});
