Ext.define('Td.view.TaskForm', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.DatePicker',
    ],
    config: {
        title: 'Редактирование задачи',
        cls: 'taskFormCls',
        layout:'vbox',
        itemId: 'taskForm',
        items: [{
            xtype: 'hiddenfield',
            name: 'taskId'

        },{
            xtype: 'datepickerfield',
            name: 'taskDate',
            label: 'Дата создания',
            labelAlign: 'top',
            value: new Date(),
            disabled: true,
        },{
            xtype: 'textfield',
            name : 'tasksName',
            label: 'Имя задачи',
            labelAlign: 'top',
        },{
            xtype: 'textareafield',
            name: 'taskArea',
            maxRows: 4,
            label: 'Текстовое поле',
            labelAlign: 'top',
        },{
            xtype: 'selectfield',
            name: 'taskType',
            label: 'Тип задачи',
            labelAlign: 'top',
            itemId: 'type',
            options: [
                {text: 'Bug',  value: 'Bug'},
                {text: 'Feature', value: 'Feature'},
            ]
        },{
            xtype:'selectfield',
            name: 'taskStatus',
            label: 'Статус задачи',
            labelAlign: 'top',
            itemId: 'status',
            options: [
                {text: 'New',  value: 'New'},
                {text: 'Fixed', value: 'Fixed'},
            ]
        },{
            xtype: 'textfield',
            name: 'taskColor',
            label: 'Выберите цвет задачи',
            labelAlign: 'top',
        },{
            xtype: 'button',
            name: 'taskSubmit',
            itemId: 'submit',
            docked: 'bottom',
            text: 'Сохранить',
        }]
    }
});
