Ext.define('Td.view.Main', {
    extend: 'Ext.NavigationView',
    alias: 'widget.main',
    config: {
        useTitleForBackButtonText: false,
        defaultBackButtonText: 'Назад',
        navigationBar: {
        },
        layout: {
            type: 'card',
            animation: {
                duration: 300,
                easing: 'ease-out',
                type: 'slide',
                direction: 'left'
            }
        }
    },
    // @private
    doSetActiveItem: function(activeItem, oldActiveItem) {
        var me = this,
            navigationBar = me.getNavigationBar();

        if (!activeItem) {
            return;
        }

        if (navigationBar) {
            navigationBar.rightBox.removeAll();
            if ( activeItem.getRightButton ){
                navigationBar.add(activeItem.getRightButton());
            }
        }
        me.callParent(arguments);
    },
    removePreviousViews: function() {
        var me = this,
            innerItems = me.getInnerItems(),
            previousView = innerItems[innerItems.indexOf(me.getActiveItem()) - 1],
            backButtonStack = me.getNavigationBar().backButtonStack;

        while (me.getInnerItems().length > 0) {
            me.removeInnerAt(0);
        }
        Ext.Array.erase(backButtonStack, 0, backButtonStack.length);
    }
});
