Ext.define('Td.controller.Main', {
	extend: 'Ext.app.Controller',

	requires: [
		'Td.store.Project',
		'Td.store.Tasks',
		'Td.view.ProjectList',
		'Td.view.TaskList',
        'Td.view.TaskForm',
		'Ext.MessageBox',
        'Ext.ActionSheet',
	],

	 config: {
        refs: { //список ссылок
            projectList: "main #projectList",
            showPromtProjectBtn: "main #showPromtProject", //main это навигатионвиев - определено через alias
            taskList: "main #taskList",
            showActionTaskBtn: "main #showActionTask",
            actionAddTaskBtn: "actionsheet #actionAddTask",
            canselAddTaskBtn: "actionsheet #canselAddTask",
            removeProjectBtn: "actionsheet #actionRemoveProject",

            taskForm: "main #taskForm",
            taskTypeBtn: '#taskForm #type',
            taskStatus: '#taskForm #status',
            taskSubmitBtn: "#taskForm #submit",
        },
        control: { //список управления
            projectList: {
                select: 'onSelectProjectList'
            },
            showPromtProjectBtn: {
                tap: 'showPromtProject' //событие и метод который должен произойти по этому событию в ЭТОМ контроллере
            },
            taskList: {
                select: 'onSelectTaskList'
            },
            showActionTaskBtn: {
                tap: 'showActionTask'
            },
            actionAddTaskBtn: {
                tap: 'showAddTask'
            },
            canselAddTaskBtn: {
                tap: 'canselShowAddTask'
            },
            taskTypeBtn: {
                change: 'changeTypeTask'
            },
            taskSubmitBtn: {
                tap: 'addTaskSubmit'
            },
            removeProjectBtn: {
                tap: 'onRemoveProject'
            },
        }
    },

    showProject: function () {
    	var projectStore;
    	if(Ext.getStore('projectStore')) {
    		projectStore = Ext.getStore('projectStore');
    	} else {
    		projectStore = Ext.create('Td.store.Project');
   		    projectStore.load();
        }
        var project = Ext.create('Td.view.ProjectList', {
            title: 'Проекты', //вот это новое - так как у всех items в nfvigationview должен быть title
        	store: projectStore,
            rightButton:  {    //это кастомная кнопка она суется в navigationview через самописный метод
                xtype: 'button',
                iconCls: 'add2',
                cls: 'show-btn',
                 //замени иконку на плюсик
                itemId: 'showPromtProject',
                align: 'right'  //вот это не трогай так как кнопка должна быть справа, слева будет кнопка Назад
            }

        });
        Td.nav.push(project);
    },

    showPromtProject: function () {
    	var me = this;
    	Ext.Msg.prompt('Создайте проект', 'Пожалуйста введите новое имя проекта.', function(btn_text, value) {
            if(btn_text == 'ok'){
    			if(value.length > 4){
                    me.addProject(value);
    			}else{
                      Ext.defer(function () {
                        Ext.Msg.alert('Предупреждение', 'Пожалуйста заполните поля', function(){
                            Ext.defer(function() {
                                me.showPromtProject();
                            }, 300);
                        });
                    }, 300);
                }
            }
		});
    },

    addProject: function (value) {  //данный метод нужно переписать чтоб он получал реальные данные которые введет пользовательaddProjectBtn
        //получаем уже созданный стор
        var projectStore;
        if(Ext.getStore('projectStore')) {
            projectStore = Ext.getStore('projectStore');  //по идее мы сюда должны попасть
        } else {
            projectStore = Ext.create('Td.store.Project');
            projectStore.load();
        }
        //и суем в него данные
        projectStore.add({
            projectName: value
        });
        projectStore.sync();
    },


    onSelectProjectList: function (list,record) {
        var me = this,
            data = record.getData(); //record это объект записи в модели, в сторе, у него есть data - вот это зачастую и используем

        me.showTask(data);

        //data получается объект с полями из модели, id - уникальный для localStorage
        //но его можно использовать как параметр для списка Задач
        //то есть при создании задачи у нее указывать projectId = data.id
        //таким образом у нас будет большой список задач но мы сможем их все сортировать для определенной выбранной задачи

        //Шаг 1
        //объект data передать в другой мето

        //шаг 2
        //в этом методе создать стор Задач, загрузить его,

        //шаг 3
        //создать список Задач  и задать ему уникальный параметр
        //projectId: data.projectId так же в конфиге как ты задаеш title или itemId

        //Шаг 4 - отобразить список задач - у нее так же прописать правую кнопку и повесить на нее
        //событие по добавлени ЗАДАЧ

        //шаг 5 при сохранении задачи из пункта 3 брать параметр projectId и сохранять его у созданной записи
        //для этого потребуется поправить модель Задач - и там еще localStorage прописать
    },

    showTask: function (data) {
        var me = this,
            tasksStore,
            projectId = data.id,
            projectList = me.getProjectList();

        if(Ext.getStore('tasksStore')) {
            tasksStore = Ext.getStore('tasksStore');
        } else {
            tasksStore = Ext.create('Td.store.Tasks');
            tasksStore.load();
        }
        tasksStore.clearFilter();
        tasksStore.filter('projectId', projectId);


        // projectId: data.id,
        //data.id вынести в отдельную переменную в данном методе, так как мы ее будем
        //использовать 2-а раза - при фильтрации
        //и при создании списка Задач

        // так как мы будем не однократно обращаться к одному и тому же стору
        //и мы будем его фильтровать
        //то состояние после фильтрации сохраняется и нам первым делом нужно его очистить
        // у стора вызывается метод clearFilter()
        //затем мы его будем фильтровать
        //есть много методов фильтрации от функций до простой фильтрации по полю-значению
        //мы используем легкий способ
        //стурктура записи такая: store.filter('nameOfModelField', 'value');
        //итого у тебя должно получиться ВСЕГО 2-е строчки - очистка фильтра и сам фильтр

        var taskList = Ext.create('Td.view.TaskList', {
            title: 'Задачи', //вот это новое - так как у всех items в nfvigationview должен быть title
            projectId: projectId,
            store: tasksStore,
            rightButton:  {    //это кастомная кнопка она суется в navigationview через самописный метод
                xtype: 'button',
                iconCls: 'add', //замени иконку на плюсик
                itemId: 'showActionTask',
                align: 'right'  //вот это не трогай так как кнопка должна быть справа, слева будет кнопка Назад
            }
        });
        Td.nav.push(taskList);

        Ext.defer(function () {
            projectList.deselectAll();
        }, 300);

        // после того как мы отобразили новый список, на прошлом списке нужно убрать выделение
        //у списка вызывается метод deselectAll()
        // когда мы возвращаемся назад через кнопку Назад - у нас прошлая запись
        //остается выделенной и мы ее не можем снова нажать
        //поэтому выделение и снимается - но только после того
        //как выьюшка становится невидимой
        //как ты в методе addTask получил ссылку на список Задач,
        //так же тебе здесь нужно получить ссылку на список Проектов и выполнить у него этот
        //метод
        //при этом метод нужно выполнить с задержкой из-за анимации в 300 миллисекунд
        //для этого есть такая контсрукция
        /*


        */
        // и не забываем про область видимости - поэтому
        //список проектов определяем перед этой конструкции с задержкой а уже внутри
        // конструкции вызываем метод у списка Проектов
    },

    showActionTask: function () {
        if(!Td.actionSheetTask){
            var actionSheet = Ext.create('Ext.ActionSheet', {
                items: [{
                    text: 'Создать задачу',
                    ui: 'decline',
                    itemId: 'actionAddTask'
                },{
                    text: 'Удалить проект',
                    ui: 'decline',
                    itemId: 'actionRemoveProject'
                },{
                    text: 'Отмена',
                    ui  : 'confirm',
                    itemId: 'canselAddTask'
                }]
            });
            Ext.Viewport.add(actionSheet);
            Td.actionSheetTask = actionSheet;
        }

        Td.actionSheetTask.show();


        //выше по коду мы делали вывод окна в который пользователь вводил только
        //имя ЗАДАЧИ
        //теперь мы расширим функционал
        //замени иконку кнопочки на другую - теперь это не будет просто плюсик - добавить задачу
        //по нажатию на эту кнопку нужно будет выводить actionsheet
        //в нем будут кнопки
        //удалить проект
        //добавить задачу
        //первое пока не будем делать а вот добавления задачи расширим
        //теперь у задачи будет не только имя, будут и другие поля
        //1)имя - textfield 2)комментарий - textarea 3)тип - selectfield 4)цвет
        // http://docs.sencha.com/touch/2.3.1/#!/api/Ext.field.Text
        // http://docs.sencha.com/touch/2.3.1/#!/api/Ext.field.TextArea
        // http://docs.sencha.com/touch/2.3.1/#!/api/Ext.field.Select
        // последнее мы оставим на потом, пока просто сделай так же текстовое поле и словами вводи
        //в него red, blue, green, yellow

        //1) - расширить модель задачи - хорошо продумай имена полей
        // 2) создать новый шаблон окна в котором будут накиданы все эти поля для запонения
        // шаблон должен быть extend: 'Ext.form.Panel'
        // http://docs.sencha.com/touch/2.3.1/#!/api/Ext.form.Panel
        // 3) вместо вывода сообщения выводить окно для ввода данных
        // 4) на окне должна быть кнопка Сохранить которая выполняет метод в котором
        // ты получиш ссылку на текущее окно и возмеш у него значение
        // у formpanel есть метод getValues - который возвращает объект со значение полей на этой форме
    },

    showAddTask: function () {
        Td.actionSheetTask.hide();
        var taskForm = Ext.create('Td.view.TaskForm');
        Ext.defer(function () {
            Td.nav.push(taskForm);
        },300);
    },

    canselShowAddTask: function () {
        Td.actionSheetTask.hide();
    },

    changeTypeTask: function (curSelect,newValue) {
        var me = this,
        taskStatus = me.getTaskStatus();
        //TODO
        if(newValue == 'Bug'){
            taskStatus.setOptions([
                {text: 'New',  value: 'New'},
                {text: 'Fixed', value: 'Fixed'},
            ])
        }else{
            taskStatus.setOptions([
                {text: 'New',  value: 'New'},
                {text: 'Implemented', value: 'Implemented'},
            ])
        }
    },

    addTaskSubmit: function () {
        var me = this,
            taskForm = me.getTaskForm(),
            taskList = me.getTaskList(),
            projectId = taskList.getProjectId(),
            values = taskForm.getValues();
        values.projectId = projectId;
        me.addTask(values);
    },

    addTask: function (values) {  //данный метод нужно переписать чтоб он получал реальные данные которые введет пользовательaddProjectBtn
        //получаем уже созданный стор
        var me = this,
            tasksStore;

        if(Ext.getStore('tasksStore')) {
            tasksStore = Ext.getStore('tasksStore');  //по идее мы сюда должны попасть
        } else {
            tasksStore = Ext.create('Td.store.Tasks');
            tasksStore.load();
        }
        if(values.taskId) {
            values.id = values.taskId;
            //редактирование
            //TODO
            // почитай вот это
            //  http://docs.sencha.com/touch/2.3.1/#!/api/Ext.data.Store-method-findRecord
            // логика такая - ты должен найти конкретную запись в сторе у которой taskId = values.taskId
            //вместо того чтоб бежать по каждой записи - есть метод у стора
            // запись похожа на вот это  tasksStore.filter('projectId', projectId);
            // ты такое уже писал - поэтому убью блин если не правильно кавычки расставиш и значения!

            // после того как ты найдеш запись в сторе - сохрани ее в переменной, назовем ее к примеру record
            // у нее есть куча методов, например
            // record.set('fieldName', fieldValue) - так переписывается одно конкретное поле
            // record.setData(newValues) - а так у записи переписываются все значения которые будут в объекте newValues
            // так что по сути тебе нужно написать 2-е строчки

            // так как у нас не предыдушем экране есть список и он отображает данные из этого стора
            // его нужно обновить
            // list.refresh();
            // ссылку на список получить я думаю у тебя не будет проблем
            // так что плюс еще две строчки - итого с тебя 4-е строчки
            record = tasksStore.findRecord('id', values.id),
            taskList = me.getTaskList();
            record.setData(values);
            taskList.refresh();
        } else {
        //и суем в него данные
            tasksStore.add(values);
        }
        tasksStore.sync();
        Td.nav.pop();

        //
        //1) чтоб закрыть-унижтожить текущую вьюху, так сказать подняться назад
        //нужно у navigationview выполнить метод pop
        //это как убрать с колоды карт верхнюю карту
        //в этот метод передается сколько раз нужно подняться, по умолчанию значение равно одному
        //то есть можно писать Td.nav.pop()  или Td.nav.pop(1) - одно и тоже

        //обясняю что происходит - мы уничтожаем форму и возвращаемся на список задач - причем
        //заметь что список пополнится новой задачей!
        //следующая наша задача - это редактирование созданной задачи

        //- ты засранец - пишу почему ниже
        //у тебя объвлен метод onSelectTaskList но он не где не реализован
        //а я тебе писал что если ты объявляем метод его нужно обязательно прописать
        //даже если он будет пустой
        //пример:
        /*
            someMethod: function(){
                // - сделай то се
            }
        /*

        //onSelectTaskList - это метод у события select следовательно оно получает
        //(list, record) - посмотри что у тебя сделано в onSelectProjectList
        // по сути тебе нужно получить примерно следующие данные:

        /*
        {
            taskType: "third", taskColor: "ываываыаыва"…}
            id: "63560707-91d1-4856-bed6-1c11237c4cb3"
            projectId: "39a9d7e1-f62a-4f0a-a0b2-cb2c23d5ca44"
            taskArea: "ываыаываываываываыва"
            taskColor: "ываываыаыва"
            taskType: "third"
            tasksName: "ываываыва"
        }
        */
        // это объект значений у данной ЗАДАЧИ
        // у формы СОЗДАНИЯ задачи можно не только getValues, но и setValues
        // так что тебе нужно заново создать вьюху форму создания задачи и перед тем как push
        // засунуть в нее значения
        //пока все

    },

    onSelectTaskList: function (list,record) {
        var me = this,
            taskForm = Ext.create('Td.view.TaskForm'),
            data = record.getData();
        data.taskId = data.id;
        taskForm.setValues(data);

        Td.nav.push(taskForm);

        Ext.defer(function () {
            list.deselectAll();
        }, 300);
    },
    onRemoveProject: function () {
        Td.actionSheetTask.hide();
        var me = this;
        Ext.defer(function () {
            Ext.Msg.confirm('Вы уверены что хотите удалить Проект', 'Вместе с проектом будут удалены все задачи', function(buttonId) {
                if(buttonId == 'yes') {
                    Ext.defer(function () {
                        me.acceptRemoveProject();
                    }, 300);
                }
            });
        },300);

        //TODO
        // с задержкой показать окно с вопросом - вы уверены что хотите удлаить Проект - вместе с проектом будут удалены все задачи
        //в случаи согласия вызывать новый метод
        //в этом методе стоит следующая задача -
        //нужно пройтись по каждой запись в сторе ЗАДАЧ
        /*
            store.each(function (item, index) {
                //todo check and remove
            });
        */
        // получить индекс (уже есть во входных параметрах - index) записи у которой projectId такой же как у текущего списка ЗАДАЧ
        // выполнить метод http://docs.sencha.com/touch/2.3.1/#!/api/Ext.data.Store-method-removeAt
        // после прохождения по всем записям в сторе, нужно получить стор ПРОЕКТОВ и удалить конкретный проект
        //затем pop()
        //и опять же обновить список ПРОЕКТОВ refresh()
    },

    acceptRemoveProject: function () {


        var me = this,
        tasksStore = Ext.getStore('tasksStore'),
        projectStore = Ext.getStore('projectStore'),
        taskList = me.getTaskList(),
        projectId = taskList.getProjectId(),
        projectList = me.getProjectList();

        tasksStore.clearFilter();

        tasksStore.each(function (item,index) {
            console.log('BOGDAN in acceptRemoveProject  tasksStore.each record.projectId = ' + item.get('projectId'));
            if(item.get('projectId') == projectId) {
                tasksStore.removeAt(index);
                console.log('BOGDAN in acceptRemoveProject  tasksStore.each remove record = true');
            } else {
                console.log('BOGDAN in acceptRemoveProject  tasksStore.each remove record = false');
            }
        });
        var projectRecord = projectStore.findRecord('id', projectId);
        console.log('BOGDAN in acceptRemoveProject  projectRecord = ' + JSON.stringify(projectRecord.getData()) );
        projectStore.remove(projectRecord);
        console.log('BOGDAN in acceptRemoveProject  after projectStore.remove(projectRecord) ');
        Td.nav.pop();
        console.log('BOGDAN in acceptRemoveProject  after Td.nav.pop() ');
        projectList.refresh();
        console.log('BOGDAN in acceptRemoveProject  projectList.refresh()');
    }
});
