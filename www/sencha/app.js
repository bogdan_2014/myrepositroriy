/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.application({
    name: 'Drl',

    requires: [
        'Ext.data.proxy.LocalStorage',
        'Ext.data.identifier.Uuid',
        'Drl.Utils',

        'Drl.locale.en',
        'Drl.locale.ru',
        'Drl.mixin.Messageable',
        'Drl.mixin.Maskable',
    ],

    controllers: [
        'Main',
        'Store',
        'Dream',
        'Camera',
        'Settings'
    ],

    views: [
        'TabPanel'
    ],

    isIconPrecomposed: true,

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();
        Ext.app.Application.mixin('messageable', Drl.mixin.Messageable);
        Ext.app.Application.mixin('maskable', Drl.mixin.Maskable);
        if (Ext.os.is.Desktop) {
             // use 'ctrl+e' combination to reload styles
            window.addEventListener('keydown', function(event) {
                if (event.ctrlKey && event.keyCode === 69) {
                    Ext.DomQuery.select('link')[0].href = 'resources/css/app.css?' + Math.ceil(Math.random() * 100000000);
                }
            }, false);
        }


        document.addEventListener("resume", onResume, false);
        function onResume() {
            debugger;
            setTimeout(function() {
              Drl.app.getController('Settings').checkUserStatus();
            }, 300);
        }
        if(!localStorage.getItem('drl_reminder_counter')) {
            localStorage.setItem('drl_reminder_counter', 0);
        }
        if( (localStorage.getItem('drl_reminder_counter') > 5) && !localStorage.getItem('drl_reminder')) {
            console.log('in launch reminder')
            setTimeout(function() {
                Drl.app.getController('Main').checkReminder();
            }, 1000 * 60 * 5);  //second * in minutes * minutes
        }
        console.log('drl_reminder_counter ===' + localStorage.getItem('drl_reminder_counter'));
        localStorage.setItem('drl_reminder_counter', parseInt(localStorage.getItem('drl_reminder_counter')) + 1);
        this.preventDoubleClicks();
        this.disableListMultiSelections();

        if (Ext.os.is.Android) {
            document.addEventListener('backbutton', this.onBackButtonPress, false);
        }

        //to claer all notification on first launch
        Ext.defer(function(){
            if(!localStorage.getItem('drl_cancelled')) {
                window.plugin.notification.local.cancelAll();
                localStorage.setItem('drl_cancelled', true);
                console.log("cancelled all succes");
            } else {
                console.log("cancelled all failure");
            }
        }, 1000);

        Ext.defer(function(){
            window.plugin.notification.local.getScheduledIds(function (scheduledIds) {
                console.log('Scheduled IDs: ' + scheduledIds.join(' ,'));
            });
        }, 1500);
        if (Ext.os.is.Android) {
            Ext.Msg.defaultAllowedConfig.showAnimation = false;
            Ext.Msg.defaultAllowedConfig.hideAnimation = false;
        }
            //cordova plugin add https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin.git

        Drl.nav = Ext.create('Drl.view.TabPanel');
        Ext.Viewport.add(Drl.nav);
        Drl.app.getController('Main').showFavourite();
        Ext.defer(function () {
            Drl.app.getController('Camera').requestFileSystem();
        }, 1000);
        Drl.app.getController('Settings').checkUserStatus();
    },

    /**
     *  Adds extra code to default onTap handler to prevent double clicks.
     */
    preventDoubleClicks: function() {
        var global = Ext.global,
            prototype = Ext.Button.prototype;

        prototype.onTap = function(e) {
            if (this.getDisabled()) {
                return false;
            }

            //<patch>
            if (global.passPress && global.passRelease) {
                //TODO ???

            } else {
                return false;
            }

            if (global.passTap) {
                global.passTap = false;
                return false;
            }

            if (global.processing) {
                return false;
            }

            global.processing = true;
            Ext.defer(function() {
                global.passPress = false;
                global.passRelease = false;
                global.passTap = false;
                global.processing = false;
            }, 300);
            //</patch>

            this.fireAction('tap', [this, e], 'doTap');
        };

        prototype.onPress = function() {
            var me = this,
                element = me.element,
                pressedDelay = me.getPressedDelay(),
                pressedCls = me.getPressedCls();

            //<patch>
            global.passPress = true;
            //</patch>

            if (!me.getDisabled()) {
                if (pressedDelay > 0) {
                    me.pressedTimeout = setTimeout(function() {
                        delete me.pressedTimeout;
                        if (element) {
                            element.addCls(pressedCls);
                        }
                    }, pressedDelay);
                }
                else {
                    element.addCls(pressedCls);
                }
            }
        };

        prototype.doRelease = function(me, e) {
            //<patch>
            global.passRelease = true;
            //</patch>

            if (!me.getDisabled()) {
                if (me.hasOwnProperty('pressedTimeout')) {
                    clearTimeout(me.pressedTimeout);
                    delete me.pressedTimeout;
                }
                else {
                    me.element.removeCls(me.getPressedCls());
                }
            }
        };
    },

    disableListMultiSelections: function() {
        var prototype = Ext.dataview.DataView.prototype;

        prototype.doItemTouchStart = function(me, index, target, record) {
            if (me.previousRecord) {
                var item = me.getItemAt(me.getStore().indexOf(me.previousRecord));
                if (item && item.renderElement) {
                    item.renderElement.removeCls(me.getPressedCls());
                }
            }

            if (record) {
                me.doAddPressedCls(record);
                me.previousRecord = record;
            }
        };

        prototype.onItemTouchEnd = function(container, target, index, e) {
            var me = this,
                store = me.getStore(),
                record = store && store.getAt(index);

            if (me.previousRecord) {
                var item = me.getItemAt(me.getStore().indexOf(me.previousRecord));
                if (item && item.renderElement) {
                    item.renderElement.removeCls(me.getPressedCls());
                }
            }

            if (record && target) {
                if (target.isComponent) {
                    if (target && target.renderElement) {
                        target.renderElement.removeCls(me.getPressedCls());
                    }
                } else {
                    target.removeCls(me.getPressedCls());
                }
            }

            me.fireEvent('itemtouchend', me, index, target, record, e);
        };
    },

    onBackButtonPress: function() {
        var nav = Ext.getCmp('main'),
            classNames = ['Ext.Panel', 'Ext.picker.Date', 'Ext.picker.Picker'],
            cl = null,
            itemToHide = null;

        Ext.each(Ext.Viewport.getItems().items, function(item) {
            cl = Ext.getClassName(item);
            if (classNames.indexOf(cl) !== -1 && item.isFloating() && !item.isHidden()) {
                itemToHide = item;
            }
        });

        if (itemToHide) {
            itemToHide.hide();
        } else if (Drl.app.menuActionSheet && !Drl.app.menuActionSheet.isHidden()) {
            Drl.app.menuActionSheet.hide();
        } else if (nav.getInnerItems().length > 1) {
            nav.pop();
        } else {
            if (this.doExitApp) {
                navigator.app.exitApp();
            } else {
                toast.showShort('Press Back again to quit');
                this.doExitApp = true;
                Ext.defer(function() {
                    this.doExitApp = false;
                }, 2500, this);
            }
        }
    }
});
