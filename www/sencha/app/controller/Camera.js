Ext.define('Drl.controller.Camera', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            /*editDreamForm: '#editDreamForm'*/
        },
        control: {
            "#editDreamForm #cameraBtn": {
                /*tap: 'onCameraBtn'*/
                tap: 'tempGetImage'
            },
            "#editDreamForm #librariBtn": {
                /*tap: 'onLibrariBtn'*/
                tap: 'tempGetImage'
            }
        },
        imageURI: '',
        fileSystem: null
    },
    requestFileSystem: function(){
        var me = this;
        console.log('in requestFileSystem');
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            var cont = Drl.app.getController('Camera');
            console.log(fileSystem);
            cont.fileSystem = fileSystem;
            console.log('in func gotFs');
        }, function (e) {
            var msg = '';
            console.log(e.code);
            switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'QUOTA_EXCEEDED_ERR';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'NOT_FOUND_ERR';
                break;
            case FileError.SECURITY_ERR:
                msg = 'SECURITY_ERR';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'INVALID_MODIFICATION_ERR';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'INVALID_STATE_ERR';
                break;
            default:
                msg = e.code;
            break;
            }
            console.log('Error: ' + msg);
        });

    },

    onCameraBtn: function () {
        var me = this;
         console.log('MAXIM in getPictureBtn');
//http://selinosblog.com/2014/01/27/saving-and-deleting-images-via-phonegaps-file-api/
        var pictureSource;   // picture source
        var destinationType; // sets the format of returned value
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;


        navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
            destinationType: destinationType.FILE_URI,
            saveToPhotoAlbum: false,
            sourceType: pictureSource.CAMERA,
            allowEdit: true });
        function onPhotoURISuccess(imageURI) {
            me.updateCameraImages(imageURI);
            /*Drl.app.dispatch({
                controller: 'Dream',
                action: 'changeImg',
                args: [imageURI]
            }, false);*/
        }

        // Called if something bad happens.
        function onFail(message) {
            console.log('MAXIM in onFail');
            console.log('Failed because: ' + message);
        }
    },
    onLibrariBtn: function () {
        var me = this;
        console.log('MAXIM in getPictureBtn');

        var pictureSource;   // picture source
        var destinationType; // sets the format of returned value
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;


        navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
            destinationType: destinationType.FILE_URI,
            saveToPhotoAlbum: false,
            sourceType: pictureSource.PHOTOLIBRARY,
            allowEdit: true });
        function onPhotoURISuccess(imageURI) {
            me.updateCameraImages(imageURI);
            /*Drl.app.dispatch({
                controller: 'Dream',
                action: 'changeImg',
                args: [imageURI]
            }, false);*/
        }
        // Called if something bad happens.
        function onFail(message) {
            console.log('MAXIM in onFail');
            console.log('Failed because: ' + message);
        }
    },
    tempGetImage: function () {
        var url = 'http://www.usiter.com/uploads/20120324/iva+derevo+plakuchaya+iva+iva+letom+44581660762.jpg';
        Drl.app.dispatch({
            controller: 'Dream',
            action: 'changeImg',
            args: [url]
        }, false);
    },






    gotFS : function(fileSystem) {
        var me = Drl.app.getController('Camera');

        me.setFileSystem(fileSystem);
        console.log('in gotFS');
        console.log(me.getFileSystem());
    },
    updateCameraImages : function(imageURI) {
        var me = this;

        me.imageURI = imageURI;
        console.log('in updateCameraImages');
        console.log(me.imageURI);
        window.resolveLocalFileSystemURL(imageURI, me.gotImageURI, me.errorHandler);
    },

    // pickup the file entry, rename it, and move the file to the app's root directory.
    // on success run the movedImageSuccess() method
     gotImageURI : function(fileEntry) {
        var cont = Drl.app.getController('Camera'),
            newName = "thumbnail_" + new Date().getTime() + ".jpg";
        console.log('in gotImageURI');
        console.log(newName);
        fileEntry.moveTo(cont.fileSystem.root, newName, cont.movedImageSuccess, function (e) {
            var msg = '';
            console.log(e.code);
            switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'QUOTA_EXCEEDED_ERR';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'NOT_FOUND_ERR';
                break;
            case FileError.SECURITY_ERR:
                msg = 'SECURITY_ERR';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'INVALID_MODIFICATION_ERR';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'INVALID_STATE_ERR';
                break;
            default:
                msg = e.code;
            break;
            }
            console.log('Error: ' + msg);
        });
     },

    // send the full URI of the moved image to the updateImageSrc() method which does some DOM manipulation
    movedImageSuccess : function(fileEntry) {
        console.log('movedImageSuccess');
        var me = Drl.app.getController('Camera');
        console.log(fileEntry);
        console.log(fileEntry.nativeURL);
        me.updateImageSrc(fileEntry.nativeURL);
    },
    updateImageSrc: function(imageURI){
        console.log('movedImageSuccess');
        console.log(imageURI);
        Drl.app.dispatch({
            controller: 'Dream',
            action: 'changeImg',
            args: [imageURI]
        }, false);
    },

    //TODO remove image when remove record
    // get a new file entry for the moved image when the user hits the delete button
    // pass the file entry to removeFile()
    removeDeletedImage : function(imageURI){
        var me = this;
        console.log('in removeDeletedImage imageURI = ' + imageURI);
        window.resolveLocalFileSystemURL(imageURI, function (fileEntry) {
            fileEntry.remove();
            console.log('in success removeDeletedImage');
        }, function (e) {
            var msg = '';
            console.log(e.code);
            switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'QUOTA_EXCEEDED_ERR';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'NOT_FOUND_ERR';
                break;
            case FileError.SECURITY_ERR:
                msg = 'SECURITY_ERR';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'INVALID_MODIFICATION_ERR';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'INVALID_STATE_ERR';
                break;
            default:
                msg = e.code;
            break;
            }
            console.log('Error: ' + msg);
        });
    },

    // simple error handler
    errorHandler : function(e) {
        var msg = '';
        console.log(e.code);
        switch (e.code) {
        case FileError.QUOTA_EXCEEDED_ERR:
            msg = 'QUOTA_EXCEEDED_ERR';
            break;
        case FileError.NOT_FOUND_ERR:
            msg = 'NOT_FOUND_ERR';
            break;
        case FileError.SECURITY_ERR:
            msg = 'SECURITY_ERR';
            break;
        case FileError.INVALID_MODIFICATION_ERR:
            msg = 'INVALID_MODIFICATION_ERR';
            break;
        case FileError.INVALID_STATE_ERR:
            msg = 'INVALID_STATE_ERR';
            break;
        default:
            msg = e.code;
        break;
        }
        console.log('Error: ' + msg);
    }
});
