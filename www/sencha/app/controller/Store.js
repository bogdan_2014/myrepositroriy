Ext.define('Drl.controller.Store', {
    extend: 'Ext.app.Controller',

    requires: [
        'Drl.store.Dreams',
        'Drl.store.RepeatMember',
        'Drl.view.DreamsList'
    ],
    config: {
        refs: {
            editDreamForm: '#editDreamForm'
        },
        control: {
            "main #addImgToStoreBtn": {
                tap: 'beforeAddImgToStore'
            },
            '#editDreamForm #bigCancel': {
                tap: 'onBigCancel'
            },
            '#editDreamForm #bigSave': {
                tap: 'onBigSave'
            }
        }
    },
    onBigCancel: function () {
        if(!Drl.editview.getRecord() && Drl.editview.getType() !== 'idea') {
            Drl.nav.setActiveItem(Drl.nav.oldActiveItem);
        }
        if(Ext.os.is.Android) {
            Drl.editview.setZIndex(0);
            Drl.editview.hide(null);
        } else {
            Drl.editview.setZIndex(0);
            Drl.editview.hide();
        }
    },
    onBigSave: function () {
        var me = this,
            editDreamForm = me.getEditDreamForm(),
            values = editDreamForm.getValues(),
            store,
            repeatStore;


        if(Ext.getStore('repeatStore')) {
            repeatStore = Ext.getStore('repeatStore');
        } else {
            repeatStore = Ext.create('Drl.store.RepeatMember');
        }
        repeatStore.load();

        repeatStore.clearFilter();
        repeatStore.filterBy(function(item){
            if(item.get('status') == true) {
                return item;
            }
        });
        var repeatCount = repeatStore.getCount();

        if(Ext.getStore('dreamsStore')) {
            store = Ext.getStore('dreamsStore');
        } else {
            store = Ext.create('Drl.store.Dreams');
        }
        if(values.url) {
            if(values.id) {
                var record = editDreamForm.getRecord();
                me.onEditDreamOrIdea(store, record, values, repeatStore, repeatCount);
            } else {
                if(values.repeatMember != 0) {
                    if(repeatCount >= 3) { //TOD FIXME 64
                        Drl.app.showMessage({
                            message: 'Превышен лимит напоминаний, напоминание будет отключено',
                            fn: function () {
                                Ext.defer(function(){
                                    me.addDreamOrIdea(store, values, false);
                                }, 300);
                            }
                        });
                    } else {
                        me.addDreamOrIdea(store, values, true);
                    }
                } else {
                    me.addDreamOrIdea(store, values, false);
                }
            }

        } else {
            Drl.app.showMessage({
                title: L('title.error'),
                message:L('addDream.error.image')
            });
        }
    },
    onEditDreamOrIdea: function (store, record, values, repeatStore, repeatCount) {
        var flag,
            dreamId,
            repeatRecord;
        repeatStore.clearFilter();
        if(record.get('repeatMember') == 0 && values.repeatMember == 0) {
            flag = 1; //just replace record
            record.set(values);
            store.sync();
            Drl.app.showMessage({
                title: L('addDream.error.titleSuccess'),
                message: values.type == "idea" ? L('addIdea.error.successUpdate') : L('addDream.error.successUpdate'),
                fn: function () {
                    Drl.nav.getActiveItem().down('list').refresh();
                    if(Ext.os.is.Android) {
                        Drl.editview.setZIndex(0);
                        Drl.editview.hide(null);
                    } else {
                        Drl.editview.setZIndex(0);
                        Drl.editview.hide();
                    }
                }
            });
        }
        if(record.get('repeatMember') != 0 && values.repeatMember == 0) {
            flag = 2; // turn off notification
            values.remember = null;
            record.set(values);
            store.sync();
            dreamId = record.get('id');
            repeatRecord = repeatStore.findRecord('dreamId', dreamId);
            repeatRecord.set('status', false);
            repeatStore.sync();
            Drl.app.getController('Main').cancelLocalNotification(repeatRecord.get('repeatId'));
            Drl.app.showMessage({
                title: L('addDream.error.titleSuccess'),
                message: values.type == "idea" ? L('addIdea.error.successUpdate') : L('addDream.error.successUpdate'),
                fn: function () {
                    Drl.nav.getActiveItem().down('list').refresh();
                    if(Ext.os.is.Android) {
                        Drl.editview.setZIndex(0);
                        Drl.editview.hide(null);
                    } else {
                        Drl.editview.setZIndex(0);
                        Drl.editview.hide();
                    }
                }
            });
        }
        if(record.get('repeatMember') != 0 && values.repeatMember != 0) {
            flag = 3; // replace notification
            record.set(values);
            store.sync();
            dreamId = record.get('id');
            repeatRecord = repeatStore.findRecord('dreamId', dreamId);
            repeatRecord.set('status', false);
            repeatStore.sync();
            Drl.app.getController('Main').cancelLocalNotification(repeatRecord.get('repeatId'));
            Ext.defer(function(){
                Drl.app.getController('Main').launchLocalNotification(record, repeatRecord);
            }, 300);
            Drl.app.showMessage({
                title: L('addDream.error.titleSuccess'),
                message: values.type == "idea" ? L('addIdea.error.successUpdate') : L('addDream.error.successUpdate'),
                fn: function () {
                    Drl.nav.getActiveItem().down('list').refresh();
                    if(Ext.os.is.Android) {
                        Drl.editview.setZIndex(0);
                        Drl.editview.hide(null);
                    } else {
                        Drl.editview.setZIndex(0);
                        Drl.editview.hide();
                    }
                }
            });
        }
        if(record.get('repeatMember') == 0 && values.repeatMember != 0) {
            flag = 4; // replace off notification
            if(repeatCount >= 3) { //TOD FIXME 64
                Drl.app.showMessage({
                    message: 'Превышен лимит напоминаний, напоминание будет отключено',
                    fn: function () {
                        values.remember = null;
                        values.repeatMember = 0;
                        record.set(values);
                        store.sync();
                        Ext.defer(function(){
                            Drl.app.showMessage({
                                title: L('addDream.error.titleSuccess'),
                                message: values.type == "idea" ? L('addIdea.error.successUpdate') : L('addDream.error.successUpdate'),
                                fn: function () {
                                    Drl.nav.getActiveItem().down('list').refresh();
                                    if(Ext.os.is.Android) {
                                        Drl.editview.setZIndex(0);
                                        Drl.editview.hide(null);
                                    } else {
                                        Drl.editview.setZIndex(0);
                                        Drl.editview.hide();
                                    }
                                }
                            });
                        }, 300);
                    }
                });
            } else {
                record.set(values);
                store.sync();
                dreamId = record.get('id');
                repeatRecord = repeatStore.findRecord('dreamId', dreamId);
                if(repeatRecord) {
                    repeatRecord.set('status', true);
                } else {
                    var repeatId = repeatStore.getTotalCount() + 1;

                    repeatRecord = repeatStore.add({
                        dreamId: dreamId,
                        repeatId: repeatId,
                        status: true
                    });
                    repeatRecord = repeatRecord[0];
                }
                repeatStore.sync();
                Drl.app.getController('Main').launchLocalNotification(record, repeatRecord);
                Drl.app.showMessage({
                    title: L('addDream.error.titleSuccess'),
                    message: values.type == "idea" ? L('addIdea.error.successUpdate') : L('addDream.error.successUpdate'),
                    fn: function () {
                        Drl.nav.getActiveItem().down('list').refresh();
                        if(Ext.os.is.Android) {
                            Drl.editview.setZIndex(0);
                            Drl.editview.hide(null);
                        } else {
                            Drl.editview.setZIndex(0);
                            Drl.editview.hide();
                        }
                    }
                });
            }
        }
        console.log("In onEditDreamOrIdea flag: " + flag);
    },

    removeRecord: function(record, store) {
        if(record.get('repeatMember') != 0) {
            var repeatStore;

            if(Ext.getStore('repeatStore')) {
                repeatStore = Ext.getStore('repeatStore');
            } else {
                repeatStore = Ext.create('Drl.store.RepeatMember');
            }
            repeatStore.load();
            repeatStore.clearFilter();

            var dreamId = record.get('id');
            var repeatRecord = repeatStore.findRecord('dreamId', dreamId);
            if(repeatRecord) {
                repeatRecord.set('status', false);
                repeatStore.sync();
                Drl.app.getController('Main').cancelLocalNotification(repeatRecord.get('repeatId'));
            }
        }
        Drl.app.getController('Camera').removeDeletedImage(record.get('url'));
        store.remove(record);
        store.sync();
    },

    onDreamFinished: function(record, store, carousel) {
        if(record.get('repeatMember') != 0 && record.get('finished') == false) {
            var repeatStore;

            if(Ext.getStore('repeatStore')) {
                repeatStore = Ext.getStore('repeatStore');
            } else {
                repeatStore = Ext.create('Drl.store.RepeatMember');
            }
            repeatStore.load();
            repeatStore.clearFilter();

            var dreamId = record.get('id');
            var repeatRecord = repeatStore.findRecord('dreamId', dreamId);
            if(repeatRecord) {
                repeatRecord.set('status', false);
                repeatStore.sync();
                Drl.app.getController('Main').cancelLocalNotification(repeatRecord.get('repeatId'));
            }
            record.set('repeatMember', 0);
            record.set('remember', null);
        }
        record.set('finished', !record.data.finished);
        if(record.data.finished) {
            record.set('dateFin', new Date());
            console.log(new Date());
        } else {
            record.set('dateFin', null);
        }
        store.sync();
        if(carousel) {
            carousel.checkForUpdate(record);
        }
    },

    addDreamOrIdea: function (store, values, addRepeat) {
        console.log("In addDreamOrIdea addRepeat: " + addRepeat);
        if(!addRepeat) {
            values.remember = null;
            values.repeatMember = 0;
        }
        var record = store.add(values);
        store.sync();
        if(values.type == "idea") {
            Drl.nav.setActiveItem(4);
        } else {
            Drl.nav.setActiveItem(1);
        }
        if(addRepeat) {
            var repeatStore;
            if(Ext.getStore('repeatStore')) {
                repeatStore = Ext.getStore('repeatStore');
            } else {
                repeatStore = Ext.create('Drl.store.RepeatMember');
            }
            var dreamId = record[0].get('id'),
                repeatId = repeatStore.getTotalCount() + 1;

            var repeatRecord = repeatStore.add({
                dreamId: dreamId,
                repeatId: repeatId,
                status: true
            });
            repeatStore.sync();
            Drl.app.getController('Main').launchLocalNotification(record[0], repeatRecord[0]);
        }
        Drl.app.showMessage({
            title: L('addDream.error.titleSuccess'),
            message: values.type == "idea" ? L('addIdea.error.success') : L('addDream.error.success'),
            fn: function () {
                Drl.nav.getActiveItem().down('list').refresh();
                if(Ext.os.is.Android) {
                    Drl.editview.setZIndex(0);
                    Drl.editview.hide(null);
                } else {
                    Drl.editview.setZIndex(0);
                    Drl.editview.hide();
                }
            }
        });
    },

    showAllDreamsList: function () {
        var me = this,
            store;
        var view = Ext.create('Drl.view.DreamsList', {
            type:'allDreams'
        });
        var pop = Drl.nav.getActiveItem().getInnerItems().length;
        if(pop !== 0) {
            Drl.nav.removePreviousViews();
        }
        Drl.nav.push(view);
        Ext.defer(function () {
            if(Ext.getStore('dreamsStore')) {
                store = Ext.getStore('dreamsStore');
            } else {
                store = Ext.create('Drl.store.Dreams');
            }
            store.load();
            store.clearFilter();
            store.filterBy(function(item){
                if(item.get('type') == 'dream') {
                    return item;
                }
            });
            store.sort('date', 'DESC');
            view.setStore(store);
        }, 300);
    }
});
