Ext.define('Drl.controller.Main', {
    extend: 'Ext.app.Controller',

    requires: [
        'Drl.view.DifferentDreamsList',
        'Drl.view.DreamsList',
        'Drl.store.Dreams',
        'Drl.view.Settings',
        'Drl.view.Main'
    ],
    config: {
        control: {
            "main #show-settings-btn": {
                tap: 'showSettings'
            },
            "main #settings-hide-btn": {
                tap: 'hideSettings'
            }
        }
    },


    showFavourite: function(){
        Drl.nav.setActiveItem(0);
        var view = Ext.create('Drl.view.DifferentDreamsList', {
            type:'favDreams'
        });
        var pop = Drl.nav.getActiveItem().getInnerItems().length;
        if(pop !== 0) {
            Drl.nav.removePreviousViews();
        }
        Drl.nav.push(view);
        var store;
        Ext.defer(function () {
            if(Ext.getStore('dreamsStore')) {
                store = Ext.getStore('dreamsStore');
            } else {
                store = Ext.create('Drl.store.Dreams');
            }
            store.load();
            store.clearFilter();
            store.filterBy(function(item){
                if(item.get('type') == 'dream' && item.get('favourite')) {
                    return item;
                }
            });
            store.sort('dateEdit', 'DESC');
            view.setStore(store);

        }, 300);
    },

    showIdeas: function () {
        var view = Ext.create('Drl.view.DreamsList', {
            store:store,
            type:'ideas'
        });
        var pop = Drl.nav.getActiveItem().getInnerItems().length;
        if(pop !== 0) {
            Drl.nav.removePreviousViews();
        }
        Drl.nav.push(view);

        var store;
        Ext.defer(function () {
            if(Ext.getStore('dreamsStore')) {
                store = Ext.getStore('dreamsStore');
            } else {
                store = Ext.create('Drl.store.Dreams');
            }
            store.load();
            store.clearFilter();
            store.filterBy(function(item){
                if(item.get('type') == 'idea') {
                    return item;
                }
            });
            store.sort('date', 'DESC');
            view.setStore(store);
        }, 300);
    },
    showFinished: function(){
        var view = Ext.create('Drl.view.DreamsList', {
            store:store,
            type:'finDreams'
        });
        var pop = Drl.nav.getActiveItem().getInnerItems().length;
        if(pop !== 0) {
            Drl.nav.removePreviousViews();
        }
        Drl.nav.push(view);

        var store;
        Ext.defer(function () {
            if(Ext.getStore('dreamsStore')) {
                store = Ext.getStore('dreamsStore');
            } else {
                store = Ext.create('Drl.store.Dreams');
            }
            store.load();
            store.clearFilter();
            store.filterBy(function(item){
                if(item.get('type') == 'dream' && item.get('finished')) {
                    return item;
                }
            });
            store.sort('dateFin', 'DESC');
            view.setStore(store);
        }, 300);
    },
    activeItemChange: function (activeItem) {
        var itemId = activeItem.getItemId();
        switch (itemId) {
        case 'btnZvezda' :
            this.showFavourite();
            break;
        case 'btnListDreams' :
            Drl.app.getController('Store').showAllDreamsList();
            break;
        case 'btnAddDream' :
            Drl.app.getController('Dream').showAddDream('dream');
            break;
        case 'btnFinished' :
            this.showFinished();
            break;
        case 'btnIdea' :
            this.showIdeas();
            break;
        }
    },

    launchLocalNotification: function (record, repeatRecord) {
        var me = this,
            payload = {
                id: repeatRecord.get('repeatId'),
                title: L('reminder.title'),
                message: 'Dream:' + record.get('description'),
                repeat: me.convertRepeat(record.get('repeatMember')),
                date: record.get('remember')
            };
        console.log("In launchLocalNotification: " + Ext.JSON.encode(payload));
        window.plugin.notification.local.add(payload);
    },
    cancelLocalNotification: function(id){
        console.log("In cancelLocalNotification: " + id);
        window.plugin.notification.local.cancel(id);
    },
    convertRepeat: function(value){
        var str;
        switch (value) {
        case 1 :
            str = 'secondly';
            break;
        case 2 :
            str = 'minutely';
            break;
        case 3 :
            str = 'hourly';
            break;
        case 4 :
            str = 'daily';
            break;
        case 5 :
            str = 'weekly';
            break;
        case 6 :
            str = 'monthly';
            break;
        case 7 :
            str = 'yearly';
            break;
        }
        return str;
    },
    showSettings: function() {
        var settingsView = Ext.create('Drl.view.Main', {
            height: '100%',
            width:'100%',
            left: 0,
            top: 0,
            bottom: 0,
            right: 0,
            scrollable: null
        });
        var panel = Ext.create('Drl.view.Settings');
        settingsView.push(panel);
        Drl.settingsView = settingsView;
        Drl.settingsView.on('hide', function(view){
            view.destroy();
        });
        Ext.Viewport.add(Drl.settingsView);
        if(Ext.os.is.Android) {
            Drl.settingsView.show(null);
        } else {
            Drl.settingsView.show();
        }
    },
    hideSettings: function(){
        if(Ext.os.is.Android) {
            Drl.settingsView.hide(null);
        } else {
            Drl.settingsView.hide();
        }
    },
    socialShare: function (record, type) {
        var description = record.get('description'),
            url = record.get('url'),
            title = L('social.header'),
            download = L('social.download'),
            link = encodeURI('https://itunes.apple.com/us/app/dreams-come-true/id888208511?ls=1&mt=8');
        switch (type) {
        case 'twitter':
            window.plugins.socialsharing.shareViaTwitter(
                description + '\n' + title,
                [url],
                link
            );
        break;
        case 'facebook':
            window.plugins.socialsharing.shareViaFacebook(
                description + '\n' + title,
                [url],
                link
            );
        break;
        case 'whatsapp':
            window.plugins.socialsharing.shareViaWhatsApp(
                title + description,
                null,
                null,
                function (e) {
                    console.log('in succes share via whatsapp ' + e);
                },
                function (error) {
                  console.log('in error share via whatsapp ' + e);
                }
            );
        break;
        case 'sms':
            window.plugins.socialsharing.shareViaSMS(
                title + '\n' + description + '\n' + link,
                null,
                function (e) {
                    console.log('in succes share via SMS ' + e);
                },
                function (error) {
                  console.log('in error share via SMS ' + e);
                }
            );
        break;
        case 'email':
            window.plugins.socialsharing.shareViaEmail(
                description + '  <br><a href="' + link + '">' + download + '</a>',
                title,
                null, // TO: must be null or an array
                null, // CC: must be null or an array
                null, // BCC: must be null or an array
                [url]
            );
        break;
        }
    },
    checkReminder: function(){
        var nav = Ext.getCmp('main'),
            classNames = ['Ext.Panel', 'Ext.picker.Date', 'Ext.picker.Picker', 'Ext.MessageBox'],
            cl = null,
            itemToHide = null;

        Ext.each(Ext.Viewport.getItems().items, function(item) {
            cl = Ext.getClassName(item);
            if (classNames.indexOf(cl) !== -1 && item.isFloating() && !item.isHidden()) {
                itemToHide = item;
            }
        });
        if(itemToHide || (Drl.editview && Drl.editview.isHidden()) || (Drl.message && !Drl.message.isHidden()) || (Drl.blockView && !Drl.blockView.isHidden()) || (Drl.actionSheet && !Drl.actionSheet.isHidden()) ) {
            setTimeout(function() {
                Drl.app.getController('Main').checkReminder();
            }, 3000);
        } else {
            var reminderPopUp = Ext.Viewport.add({
                xtype: 'panel',
                cls: 'reminder',
                modal: true,
                showAnimation: null,
                hideAnimation: null,
                /*showAnimation: {
                    type: 'popIn',
                    duration: 250,
                    easing: 'ease-out'
                },
                hideAnimation: {
                    type: 'popOut',
                    duration: 250,
                    easing: 'ease-out'
                },*/
                centered: true,
                width: '82%',
                items: [{
                    xtype: 'panel',
                    cls: 'rate-header',
                    html: L('settings.rateApp.title')
                }, {
                    xtype: 'panel',
                    html: L('settings.rateApp.text')
                }, {
                    xtype: 'button',
                    listeners: {
                        tap: function (btn) {
                            btn.up().hide();
                            Drl.app.getController('Settings').rateApp();

                        }
                    },
                    cls: 'rate-now',
                    text: L('settings.rateApp.buttons.0')
                },{
                    xtype: 'button',
                    listeners: {
                        tap: function (btn) {
                            btn.up().hide();
                            setTimeout(function() {
                                Drl.app.getController('Main').checkReminder();
                            }, 1000 * 60 * 5);
                        }
                    },
                    text: L('settings.rateApp.buttons.1')
                },{
                    xtype: 'button',
                    cls: 'cancel',
                    listeners: {
                        tap: function (btn) {
                            btn.up().hide();
                            localStorage.setItem('drl_reminder', true);
                        }
                    },
                    text: L('settings.rateApp.buttons.2')
                }]
            });
        }
    }
});
