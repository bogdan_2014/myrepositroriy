Ext.define('Drl.controller.Settings', {
    extend: 'Ext.app.Controller',

    requires: [
        'Drl.view.password.PasswordToogle',
        'Drl.view.password.SetPassword',
        'Drl.view.password.ChangePassword',
        'Drl.view.Faq',
        'Drl.view.PasswordBlock'
    ],

    config: {
        refs: {
            passwordForm: '#passwordForm',
            setPasswordForm: '#setPasswordForm',
            changePasswordForm: '#changePasswordForm',
            passwordField: '#setPasswordForm #passwordField',
            passwordRepeatField: '#setPasswordForm #passwordRepeatField',
            passwordHelpField: '#setPasswordForm #passwordHelpField',
            changePasswordHelpField: '#changePasswordForm #passwordHelpField',
            changePasswordField: '#changePasswordForm #passwordField',
            changePasswordRepeatField: '#changePasswordForm #passwordRepeatField',
            changePasswordOldField: '#changePasswordForm #passwordOldField',


            passwordToogle: '#passwordForm #password-toogle',
            changePasswordBtn: '#passwordForm #change-password-btn',
            savePasswordBtn: '#setPasswordForm #save-password-btn',
            saveChangePasswordBtn: '#changePasswordForm #save-change-password-btn',


            passwordBlock: '#passwordBlock',
            blockedPanelPassword: '#blocked-panel-password',
            blockedPanelError: '#blocked-panel-error'

        },
        control: {
            "main #settings-password": {
                tap: 'showSettingsPassword'
            },
            "main #settings-faq": {
                tap: 'showSettingsFaq'
            },
            "main #settings-callback": {
                tap: 'showSettingsCallback'
            },
            "main #settings-rate-app": {
                tap: 'rateApp'
            },
            passwordToogle: {
                change: 'changePasswordToogle'
            },
            passwordField: {
                keyup: 'keyUpPassword'
            },
            passwordRepeatField: {
                keyup: 'keyUpPasswordRepeat'
            },
            changePasswordField: {
                keyup: 'keyUpPassword'
            },
            changePasswordRepeatField: {
                keyup: 'keyUpPasswordRepeat'
            },
            changePasswordOldField: {
                keyup: 'keyUpPasswordOld'
            },
            passwordHelpField: {
                focus: 'focusPasswordHelp'
            },
            changePasswordHelpField: {
                focus: 'focusPasswordHelp'
            },
            savePasswordBtn: {
                tap: 'onSavePasswordBtn'
            },
            changePasswordBtn: {
                tap: 'onChangePasswordBtn'
            },
            saveChangePasswordBtn: {
                tap: 'onSaveChangePasswordBtn'
            }
        }
    },


    showSettingsFaq: function () {
        var view = Ext.create('Drl.view.Faq');
        Drl.settingsView.push(view);
    },
    showSettingsPassword: function () {
        var view = Ext.create('Drl.view.password.PasswordToogle');
        view.on('show', function(){
            this.hasToogle = false;
        });
        Drl.settingsView.push(view);
        this.hasToogle = false;
    },
    hasToogle: false,
    changePasswordToogle: function(field, newValue, oldValue) {
        var me = this,
            hasToogle = me.hasToogle,
            changePasswordBtn = me.getChangePasswordBtn(),
            passwordForm = me.getPasswordForm();
            console.log(newValue);
        if (newValue == 1) {
            if(!hasToogle) {
                if(!localStorage.getItem('drl_old_password')) {
                    var view = Ext.create('Drl.view.password.SetPassword');
                    Drl.settingsView.push(view);
                    me.hasToogle = true;
                    passwordForm.setShowOnce(true);
                }
            }
        } else {
            me.hasToogle = false;
            field.setLabel(L('settings.password.enablePas'));
            changePasswordBtn.hide();
            localStorage.removeItem('drl_old_password');
            localStorage.removeItem('drl_old_help');
        }
        if(localStorage.getItem('drl_old_password')) {

            field.setLabel(L('settings.password.disablePas'));
            changePasswordBtn.show();
        }
    },
    setFocusPassword: function(){
        var me = this,
            setPasswordForm = me.getSetPasswordForm(),
            changePasswordForm = me.getChangePasswordForm(),
            form;
        if(setPasswordForm) {
            form = setPasswordForm;
        } else {
            form = changePasswordForm;
        }
        var field = form.getFields(),
            password = field.password,
            setPasswordError = form.down('#set-password-error');

        password.onClearIconTap();
        password.focus();
        password.element.dom.focus();
        me.repaitBlockedPanelSetPassword('');
        setPasswordError.setHtml(L('settings.password.passwordErrorNormal'));
    },
    setFocusPasswordRepeat: function(){
        var me = this,
            setPasswordForm = me.getSetPasswordForm(),
            changePasswordForm = me.getChangePasswordForm(),
            form;
        if(setPasswordForm) {
            form = setPasswordForm;
        } else {
            form = changePasswordForm;
        }
        var field = form.getFields(),
            passwordRepeat = field.passwordRepeat;
            setPasswordRepeatError =form.down('#set-passwordRepeat-error');

        passwordRepeat.onClearIconTap();
        passwordRepeat.focus();
        passwordRepeat.element.dom.focus();
        me.repaitBlockedPanelSetPasswordRepeat('');
        setPasswordRepeatError.setHtml(L('settings.password.passwordRepeatErrorNormal'));
    },
    setFocusOldPassword: function(){
        var me = this,
            form = me.getChangePasswordForm();
        var field = form.getFields(),
            passwordOld = field.passwordOld;
            setPasswordOldError =form.down('#set-passwordOld-error');

        passwordOld.onClearIconTap();
        passwordOld.focus();
        passwordOld.element.dom.focus();
        me.repaitBlockedPanelSetPasswordOld('');
        setPasswordOldError.setHtml(L('settings.password.passwordOldErrorNormal'));
    },
    keyUpPasswordOld: function(field){
        var me = this,
            value = field.getValue();
        if(value) {
            value = value.toString();
            if(value.length == 4) {
                me.onFullSetPasswordOld();
            }
        }
        me.repaitBlockedPanelSetPasswordOld(value);
    },
    keyUpPassword: function(field){
        var me = this,
            value = field.getValue();
        if(value) {
            value = value.toString();
            if(value.length == 4) {
                me.onFullSetPassword();
            }
        }
        me.repaitBlockedPanelSetPassword(value);
    },
    keyUpPasswordRepeat: function(field){
        var me = this,
            value = field.getValue();
        if(value) {
            value = value.toString();
            if(value.length == 4) {
                me.onFullSetPasswordRepeat();
            }
        }
        me.repaitBlockedPanelSetPasswordRepeat(value);
    },
    repaitBlockedPanelSetPasswordOld: function (value){
         var me = this,
            form = me.getChangePasswordForm();
        var blockedPanelSetPasswordOld = form.down('#blocked-panel-set-passwordOld'),
            items = blockedPanelSetPasswordOld.getInnerItems(),
            length = 0;
        Ext.Array.forEach(items, function(item) {
            if(item.getXTypes() == "component/container/panel") {
                if(item.getCls()[1] == 'active') {
                    item.removeCls('active');
                }
            }
        });
        if(value) {
            length = value.toString().length;
            for (i = 0; i < length; i++) {
                items[i+1].addCls('active');
            }
        }
    },
    repaitBlockedPanelSetPassword: function(value){
        var me = this,
            setPasswordForm = me.getSetPasswordForm(),
            changePasswordForm = me.getChangePasswordForm(),
            form;
        if(setPasswordForm) {
            form = setPasswordForm;
        } else {
            form = changePasswordForm;
        }
        var blockedPanelSetPassword = form.down('#blocked-panel-set-password'),
            items = blockedPanelSetPassword.getInnerItems(),
            length = 0;
        Ext.Array.forEach(items, function(item) {
            if(item.getXTypes() == "component/container/panel") {
                if(item.getCls()[1] == 'active') {
                    item.removeCls('active');
                }
            }
        });
        if(value) {
            length = value.toString().length;
            for (i = 0; i < length; i++) {
                items[i+1].addCls('active');
            }
        }

    },
    repaitBlockedPanelSetPasswordRepeat: function(value){
        var me = this,
            setPasswordForm = me.getSetPasswordForm(),
            changePasswordForm = me.getChangePasswordForm(),
            form;
        if(setPasswordForm) {
            form = setPasswordForm;
        } else {
            form = changePasswordForm;
        }
        var blockedPanelSetPasswordRepeat = form.down('#blocked-panel-set-passwordRepeat'),
            items = blockedPanelSetPasswordRepeat.getInnerItems(),
            length = 0;
        Ext.Array.forEach(items, function(item) {
            if(item.getXTypes() == "component/container/panel") {
                if(item.getCls()[1] == 'active') {
                    item.removeCls('active');
                }
            }
        });
        if(value) {
            length = value.toString().length;
            for (i = 0; i < length; i++) {
                items[i+1].addCls('active');
            }
        }
    },
    onFullSetPasswordOld: function(){
        var me = this,
            form = me.getChangePasswordForm(),
            field = form.getFields(),
            passwordOld = field.passwordOld;
        passwordOld.blur();
    },
    onFullSetPassword: function(){
        var me = this,
            setPasswordForm = me.getSetPasswordForm(),
            changePasswordForm = me.getChangePasswordForm(),
            form;
        if(setPasswordForm) {
            form = setPasswordForm;
        } else {
            form = changePasswordForm;
        }
        var field = form.getFields(),
            password = field.password;
        password.blur();
    },
    onFullSetPasswordRepeat: function(){
        var me = this,
            setPasswordForm = me.getSetPasswordForm(),
            changePasswordForm = me.getChangePasswordForm(),
            form;
        if(setPasswordForm) {
            form = setPasswordForm;
        } else {
            form = changePasswordForm;
        }
        var field = form.getFields(),
            passwordHelp = field.passwordHelp,
            passwordRepeat = field.passwordRepeat;
        passwordRepeat.blur();
        passwordHelp.focus();
    },
    onSaveChangePasswordBtn: function(){
        var me = this,
            form = me.getChangePasswordForm(),
            field = form.getFields(),
            password = field.password.getValue(),
            passwordRepeat = field.passwordRepeat.getValue(),
            passwordOld = field.passwordOld.getValue(),
            passwordHelp = field.passwordHelp.getValue(),
            setPasswordError = form.down('#set-password-error'),
            setPasswordRepeatError = form.down('#set-passwordRepeat-error'),
            setPasswordOldError = form.down('#set-passwordOld-error'),
            setPasswordHintError = form.down('#set-passwordHint-error');
        var error = false;
        if(password.length != 4) {
            error = true;
            setPasswordError.setHtml(L('settings.password.passwordError'));
        }
        if(passwordRepeat.length != 4) {
            error = true;
            setPasswordRepeatError.setHtml(L('settings.password.passwordRepeatError'));
        }
        if(passwordOld.length != 4) {
            error = true;
            setPasswordOldError.setHtml(L('settings.password.passwordOldError'));
        }
        if(passwordHelp.length < 1 || passwordHelp.length > 25 ) {
            error = true;
            setPasswordHintError.setHtml(L('settings.password.passwordHintError'));
        }
        if(!error) {
            if(passwordOld == localStorage.getItem('drl_old_password')) {
                if(password == passwordRepeat) {
                    localStorage.setItem('drl_old_password', password);
                    localStorage.setItem('drl_old_help', passwordHelp);
                    Drl.app.showMessage({
                        title: L('dreams.message.titleSuccess'),
                        message: L('settings.password.passwordChangeSucces'),
                        fn: function () {
                            Drl.settingsView.pop(2);
                        }
                    });
                } else {
                    setPasswordRepeatError.setHtml(L('settings.password.passwordBothError'));
                }
            } else {
                setPasswordOldError.setHtml(L('settings.password.passwordOldWrong'));
            }
        }
    },
    onSavePasswordBtn: function(){
        var me = this,
            form = me.getSetPasswordForm(),
            field = form.getFields(),
            password = field.password.getValue(),
            passwordRepeat = field.passwordRepeat.getValue(),
            passwordHelp = field.passwordHelp.getValue(),
            setPasswordError = form.down('#set-password-error'),
            setPasswordRepeatError = form.down('#set-passwordRepeat-error'),
            setPasswordHintError = form.down('#set-passwordHint-error');
        var error = false;
        if(password.length != 4) {
            error = true;
            setPasswordError.setHtml(L('settings.password.passwordError'));
        }
        if(passwordRepeat.length != 4) {
            error = true;
            setPasswordRepeatError.setHtml(L('settings.password.passwordRepeatError'));
        }
        if(passwordHelp.length < 1 || passwordHelp.length > 25 ) {
            error = true;
            setPasswordHintError.setHtml(L('settings.password.passwordHintError'));
        }
        if(!error) {
            if(password == passwordRepeat) {
                localStorage.setItem('drl_old_password', password);
                localStorage.setItem('drl_old_help', passwordHelp);
                Drl.app.showMessage({
                    title: L('dreams.message.titleSuccess'),
                    message: L('settings.password.passwordSaveSucces'),
                    fn: function () {
                        Drl.settingsView.pop(2);
                    }
                });
            } else {
                setPasswordRepeatError.setHtml(L('settings.password.passwordBothError'));
            }
        }
    },

    onChangePasswordBtn: function(){
        var view = Ext.create('Drl.view.password.ChangePassword');
        Drl.settingsView.push(view);
    },
    checkUserStatus: function(){
        if(localStorage.getItem('drl_old_password')) {
            if(Drl.blockView && Drl.blockView.isHidden()) {
                if(Ext.os.is.Android) {
                    Drl.blockView.setZIndex(1000);
                    Drl.blockView.show(null);
                    
                } else {
                    Drl.blockView.setZIndex(1000);
                    Drl.blockView.show();                    
                }
                Drl.blockView.resetPassword();

            } else {
                if(Drl.blockView && !Drl.blockView.isHidden()) {
                    Drl.blockView.resetPassword();
                } else {
                    Drl.blockView = Ext.create('Drl.view.PasswordBlock');
                    console.log('ZINDEX = ' + Drl.blockView.getZIndex());
                    Drl.blockView.setZIndex(1000);
                    Ext.Viewport.add(Drl.blockView);
                    if(Ext.os.is.Android) {                        
                        Drl.blockView.show(null);
                    } else {
                        Drl.blockView.show();
                    }
                }
            }
            console.log('ZINDEX = ' + Drl.blockView.getZIndex());
        }
    },
    onBlockedHelp: function(){
         var me = this,
            blockedPanelError = me.getBlockedPanelError();
        blockedPanelError.setHtml(L('settings.password.passwordHintShort') + ' : ' + localStorage.getItem('drl_old_help'));
    },
    onBlockedButton: function(btn) {
        var me = this,
            passwordBlock = me.getPasswordBlock(),
            password = passwordBlock.getPassword(),
            text = btn.getText(),
            blockedPanelError = me.getBlockedPanelError();

        blockedPanelError.setHtml(L('settings.password.passwordErrorBlock'));

        password = password + text;
        passwordBlock.setPassword(password);

        if(password.length < 4) {
            me.repaitBlockedPanelPassword(password);
        } else {
            me.onFullPassword(password);
        }
    },
    onBlockedBackSpace: function(){
        var me = this,
            passwordBlock = me.getPasswordBlock(),
            password = passwordBlock.getPassword();
        password = password.substring(0, password.length - 1);
        passwordBlock.setPassword(password);

        me.repaitBlockedPanelPassword(password);
    },
    repaitBlockedPanelPassword: function(password) {
        var me = this,
            blockedPanelPassword = me.getBlockedPanelPassword(),
            items = blockedPanelPassword.getInnerItems(),
            length = password.length;
        Ext.Array.forEach(items, function(item) {
            if(item.getXTypes() == "component/container/panel") {
                if(item.getCls()[1] == 'active') {
                    item.removeCls('active');
                }
            }
        });
        for (i = 0; i < length; i++) {
            items[i+1].addCls('active');
        }
    },
    onFullPassword: function(password){
        var me = this,
            blockedPanelError = me.getBlockedPanelError(),
            passwordBlock = me.getPasswordBlock();
        if(localStorage.getItem('drl_old_password') == password) {
            if(Ext.os.is.Android) {
                Drl.blockView.hide(null);
                Drl.blockView.setZIndex(0);
            } else {
                Drl.blockView.hide();
                Drl.blockView.setZIndex(0);
            }
            console.log('ZINDEX = ' + Drl.blockView.getZIndex());
        } else {
            passwordBlock.setPassword('');
            blockedPanelError.setHtml(L('settings.password.passwordBlockError'));
            me.repaitBlockedPanelPassword('');
        }
    },

    focusPasswordHelp: function(){
        var me = this,
            setPasswordForm = me.getSetPasswordForm(),
            changePasswordForm = me.getChangePasswordForm(),
            form;
        if(setPasswordForm) {
            form = setPasswordForm;
        } else {
            form = changePasswordForm;
        }
        var setPasswordHintError = form.down('#set-passwordHint-error');
        setPasswordHintError.setHtml(L('settings.password.passwordHint'));
    },
    showSettingsCallback: function(){
        var to = ['dreamstopapp@gmail.com'],
            subject = 'Dreams Come True';
        console.log(to + '   ' + subject);

        window.plugins.socialsharing.shareViaEmail(
            null,
            subject,
            to, // TO: must be null or an array
            null, // CC: must be null or an array
            null, // BCC: must be null or an array
            null
        );
    },
    rateApp: function(){
        console.log('in rateApp');
        var ref = window.open(encodeURI('https://itunes.apple.com/us/app/dreams-come-true/id888208511?ls=1&mt=8'), '_system');
        localStorage.setItem('drl_reminder', true);
    }
});
