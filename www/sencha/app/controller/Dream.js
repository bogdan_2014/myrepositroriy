Ext.define('Drl.controller.Dream', {
    extend: 'Ext.app.Controller',

    requires: [
        'Drl.view.AddDream',
        'Drl.view.DreamsCarousel'
    ],

    config: {
        refs: {
            editDreamForm: '#editDreamForm',
            dreamCarousel: '#dreamCarousel',
            editPhotoPanel: '#editPhotoPanel',
            removeEditPhotoImg: '#removeEditPhotoImg'
        },
        control: {
            "#editDreamForm #hiddenEditPhotoBtn": {
                singletap: 'onHdnEditPhotoBtn',
                taphold: 'onTapHoldHdnEditPhotoBtn'
            },
            '#editDreamForm #removeEditPhotoImg': {
                tap: 'onRmvEditPhotoImg'
            },
            '#editDreamForm #selectRepeat': {
                change: 'cnahgeSelectRepeat'
            },
            'main #addIdea': {
                tap: 'onAddIdea'
            }
        }
    },

    showAddDream: function (type) {
        var view = Ext.create('Drl.view.AddDream', {
                type: type
            }),
            values = view.getValues();
        values.type = type;
        view.setValues(values);
        Drl.editview = view;
        Ext.Viewport.add(Drl.editview);

        if(Ext.os.is.Android) {
            Drl.editview.setZIndex(2);
            Drl.editview.show(null);
        } else {
            Drl.editview.setZIndex(2);
            Drl.editview.show();
        }
    },
    showEditDream: function (record) {
        var view = Ext.create('Drl.view.AddDream', {
            record: record
        });
        Drl.editview = view;
        Ext.Viewport.add(Drl.editview);
        if(Ext.os.is.Android) {
            Drl.editview.setZIndex(2);
            Drl.editview.show(null);
        } else {
            Drl.editview.setZIndex(2);
            Drl.editview.show();
        }
    },
    changeImg: function (url) {
        var me = this,
            editPhotoPanel = me.getEditPhotoPanel(),
            editDreamForm = me.getEditDreamForm(),
            str ='';
        str+= '<div style="background:rgba(255,255,255,1) url(' + url + ') no-repeat;';
        str+=';background-size: cover;background-position: 50%;" class="edit-photo"></div>';
        editPhotoPanel.getInnerAt(1).setHtml(str);
        editPhotoPanel.setActiveItem(1);
        editDreamForm.getFields().url.setValue(url);
    },
    onHdnEditPhotoBtn: function () {
        console.log('Tap');
    },
    onTapHoldHdnEditPhotoBtn: function () {
        var me = this,
            removeEditPhotoImg = me.getRemoveEditPhotoImg();
        removeEditPhotoImg.show();
        console.log('Taphold');
    },
    onRmvEditPhotoImg: function () {
        var me = this,
            editPhotoPanel = me.getEditPhotoPanel(),
            editDreamForm = me.getEditDreamForm(),
            removeEditPhotoImg = me.getRemoveEditPhotoImg();
        removeEditPhotoImg.hide();
        Drl.app.getController('Camera').removeDeletedImage(editDreamForm.getFields().url.getValue());
        editDreamForm.getFields().url.setValue(null);
        editPhotoPanel.setActiveItem(0);
    },
    cnahgeSelectRepeat: function (field, newValue, oldValue) {
        var me = this,
            editDreamForm = me.getEditDreamForm(),
            rememberField = editDreamForm.getFields().remember;
        if(newValue !== 0) {
            rememberField.show();
            rememberField.setValue(new Date());
        } else {
            rememberField.hide();
        }
    },
    showDreamGallery: function(store, index) {
        if(!Drl.carouselview) {
            var view = Ext.create('Drl.view.DreamsCarousel');
            Drl.carouselview = view;
            Ext.Viewport.add(Drl.carouselview);
        }
        Drl.carouselview.setStore(store);
        Drl.carouselview.setIndex(index);
        Drl.carouselview.removeAll();
        Drl.carouselview.launch();
        if(store.getCount() > 10) {
            Ext.defer(function () {
                console.log('mainSelect hideBusy');
                Drl.app.hideBusy();
                if(Ext.os.is.Android) {
                    Drl.carouselview.show(null);
                } else {
                    Drl.carouselview.show();
                }
            },300);
        } else {
            Drl.app.hideBusy();
            if(Ext.os.is.Android) {
                Drl.carouselview.show(null);
            } else {
                Drl.carouselview.show();
            }
        }
    },
    onAddIdea: function () {
        Drl.app.getController('Dream').showAddDream('idea');
    }
});
