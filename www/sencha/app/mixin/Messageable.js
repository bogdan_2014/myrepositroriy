/**
 * A Messageable mixin of the application.
 */
Ext.define('Drl.mixin.Messageable', {

    extend: 'Ext.mixin.Mixin',

    requires: ['Ext.MessageBox'],

    mixinId: 'messageable',


    showMessage: function(config) {
       /* if(Ext.os.is.Android) {*/
            Ext.Msg.defaultAllowedConfig.showAnimation = false;
            Ext.Msg.defaultAllowedConfig.hideAnimation = false;
        /*}*/
        var message = config.message || '',
            title = config.title || '',
            fn = config.fn || Ext.emptyFn;

        Drl.message = Ext.Msg.alert(title, message, fn);
    },

    showQuestion: function(config) {
        /*if(Ext.os.is.Android) {*/
            Ext.Msg.defaultAllowedConfig.showAnimation = false;
            Ext.Msg.defaultAllowedConfig.hideAnimation = false;
        /*}*/
        var message = config.message || '',
            title = config.title || '',
            fn = config.fn || Ext.emptyFn,
            buttons = config.buttons || Ext.MessageBox.OKCANCEL;
        Drl.message = Ext.Msg.confirm(title, message, fn, buttons);
    }
});
