Ext.define('Drl.carousel.CyclicCarousel', {
    extend: 'Ext.carousel.Carousel',

    // @private
    temporary: false,

    // @private
    animating: false,

    // @private

    initialize: function() {
        var me = this;

        me.callParent();

    },

    onItemAdd: function(item, index) {
        if (!this.temporary) {
            this.callParent(arguments);
            return;
        }

        var innerIndex = this.getInnerItems().indexOf(item);

        if (innerIndex <= this.getActiveIndex()) {
            this.refreshActiveIndex();
        }

        if (this.isIndexDirty(innerIndex) && !this.isItemsInitializing) {
            this.refreshActiveItem();
        }
    },

    onItemRemove: function(item, index) {
        if (!this.temporary) {
            this.callParent(arguments);
            return;
        }

        var innerIndex = this.getInnerItems().indexOf(item),
            indicator = this.getIndicator(),
            carouselItems = this.carouselItems,
            i, ln, carouselItem;

        if (innerIndex <= this.getActiveIndex()) {
            this.refreshActiveIndex();
        }

        if (this.isIndexDirty(innerIndex)) {
            for (i = 0, ln = carouselItems.length; i < ln; i++) {
                carouselItem = carouselItems[i];

                if (carouselItem.getComponent() === item) {
                    carouselItem.setComponent(null);
                }
            }

            this.refreshActiveItem();
        }
    },
    onDragEnd: function(e) {
        if (!this.isDragging) {
            return;
        }

        this.onDrag(e);

        this.isDragging = false;

        var now = Ext.Date.now(),
            itemLength = this.itemLength,
            threshold = itemLength / 2,
            offset = this.offset,
            activeIndex = this.getActiveIndex(),
            maxIndex = this.getMaxItemIndex(),
            animationDirection = 0,
            flickDistance = offset - this.flickStartOffset,
            flickDuration = now - this.flickStartTime,
            indicator = this.getIndicator(),
            velocity;

        if (flickDuration > 0 && Math.abs(flickDistance) >= 10) {
            velocity = flickDistance / flickDuration;

            if (Math.abs(velocity) >= 1) {
                if (velocity < 0 && activeIndex < maxIndex) {
                    animationDirection = -1;
                    if (this.activeIndex === this.getMaxItemIndex()-1) {
                        this.temporary = true;
                        this.addItem(this.getInnerItemAt(2).getRecord(), this.getMaxItemIndex()+1);
                    }

                }
                else if (velocity > 0 && activeIndex > 0) {
                    animationDirection = 1;
                    if (this.activeIndex === 1) {
                        this.temporary = true;
                        this.addItem(this.getInnerItemAt(this.getMaxItemIndex()-2).getRecord(), 0);
                    }
                }
            }
        }

        if (animationDirection === 0) {
            if (activeIndex < maxIndex && offset < -threshold) {
                animationDirection = -1;
                if (this.activeIndex === this.getMaxItemIndex()-1) {
                    this.temporary = true;
                    this.addItem(this.getInnerItemAt(2).getRecord(), this.getMaxItemIndex()+1);
                }
            }
            else if (activeIndex > 0 && offset > threshold) {
                animationDirection = 1;
                if (this.activeIndex === 1) {
                    this.temporary = true;
                    this.addItem(this.getInnerItemAt(this.getMaxItemIndex()-2).getRecord(), 0);
                }
            }
        }

        if (indicator) {
            indicator.setActiveIndex(activeIndex - animationDirection);
        }
        this.animationDirection = animationDirection;

        this.setOffsetAnimated(animationDirection * itemLength);
    },

    /**
     * @private
     * @return {Ext.carousel.Carousel} this
     * @chainable
     */
    setOffsetAnimated: function(offset) {
        var indicator = this.getIndicator();

        if (indicator) {
            if (this.temporary) {
                indicator.setActiveIndex(0);
            } else {
                indicator.setActiveIndex(this.getActiveIndex() - this.animationDirection);
            }
        }

        this.offset = offset;

        this.getTranslatable().translateAxis(this.currentAxis, offset + this.itemOffset, this.getAnimation());

        return this;
    },

    onAnimationEnd: function(translatable) {
        var currentActiveIndex = this.getActiveIndex(),
            animationDirection = this.animationDirection,
            axis = this.currentAxis,
            currentOffset = translatable[axis],
            itemLength = this.itemLength,
            offset;

        if (animationDirection === -1) {
            offset = itemLength + currentOffset;
        }
        else if (animationDirection === 1) {
            offset = currentOffset - itemLength;
        }
        else {
            offset = currentOffset;
        }

        offset -= this.itemOffset;
        this.offset = offset;
        this.setActiveItem(currentActiveIndex - animationDirection);

        if (this.temporary) {
            if(animationDirection === -1) {
                this.removeAt(0);
            } else if (animationDirection === 1) {
                this.removeAt(this.getMaxItemIndex());
            }
            this.temporary = false;
        }

        this.animating = false;
    },

    /**
     * Switches to the next (or first) card.
     * @return {Ext.carousel.Carousel} this
     * @chainable
     */
    next: function() {
        if (!this.getInnerItems().length) {
            return;
        }

        this.setOffset(0);

        this.animating = true;

        if (this.activeIndex === this.getMaxItemIndex()-1) {
            this.temporary = true;
            this.addItem(this.getInnerItemAt(2).getRecord(), this.getMaxItemIndex()+1);
        }

        this.animationDirection = -1;
        this.setOffsetAnimated(-this.itemLength);
        return this;
    },
    previous: function() {
        this.setOffset(0);

        if (this.activeIndex === 1) {
            this.temporary = true;
            this.addItem(this.getInnerItemAt(this.getMaxItemIndex()-2).getRecord(), 0);
        }

        this.animationDirection = 1;
        this.setOffsetAnimated(this.itemLength);
        return this;
    },

    destroy: function() {
        this.callParent();
    }
});
