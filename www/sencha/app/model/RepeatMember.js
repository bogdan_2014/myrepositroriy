Ext.define('Drl.model.RepeatMember', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'dreamId',   type: 'string'},
            {name: 'repeatId',   type: 'int'},
            {name: 'status',   type: 'boolean'}
        ],
        identifier:'uuid', // needed to avoid console warnings!
        proxy: {
            type: 'localstorage',
            id  : 'drl_repeat'
        },
        autoLoad : true,
        autoSync : true
    }
});
