Ext.define('Drl.model.Dreams', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            'url',
            'description',
            'repeatMember',
            {
                name: 'date',
                type: 'date'
            },
            {
                name: 'dateFin',
                type: 'date'
            },
            {
                name: 'dateEdit',
                type: 'date'
            },
            {
                name: 'remember',
                type: 'date'
            },
            {
                name: 'finished',
                type: 'boolean'
            },
            {
                name: 'favourite',
                type: 'boolean'
            },
            'type'
        ],
        identifier:'uuid', // needed to avoid console warnings!
        proxy: {
            type: 'localstorage',
            id  : 'drl_dreams'
        },
        autoLoad : true,
        autoSync : true
    }
});
