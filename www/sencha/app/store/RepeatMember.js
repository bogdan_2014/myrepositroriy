Ext.define('Drl.store.RepeatMember', {
    extend:'Ext.data.Store',
    requires: [
        'Drl.model.RepeatMember'
    ],
    config:{
        model:'Drl.model.RepeatMember',
        storeId: 'repeatStore'
    }
});
