Ext.define('Drl.store.Dreams', {
    extend:'Ext.data.Store',
    requires: [
        'Drl.model.Dreams'
    ],
    config:{
        model:'Drl.model.Dreams',
        storeId: 'dreamsStore'
    }
});
