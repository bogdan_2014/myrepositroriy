Ext.define("Drl.view.password.PasswordToogle", {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Toggle'
    ],
    config: {
        itemId: 'passwordForm',
        title: L('settings.password.title'),
        cls: 'settings-view',
        height: '100%',
        showOnce: false,
        flex: 1,
        rightButton:  {
            xtype: 'button',
            cls: 'cancel',
            itemId: 'settings-hide-btn',
            align: 'right'
        },
        layout: 'vbox',
        scrollable: null,
        items: [{
            xtype: 'fieldset',
            layout: 'vbox',
            flex: 1,
            height: '100%',
            items: [{
                xtype: 'togglefield',
                name: 'tooglePassword',
                label: L('settings.password.enablePas'),
                itemId: 'password-toogle',
                labelWidth: '256px',
                value: 0
            }, {
                xtype: 'button',
                text: L('settings.password.passwordChangeBtn'),
                cls: 'btn-as-select',
                itemId: 'change-password-btn',
                hidden: true
            }]
        }]
    },
    initialize: function () {
        var me = this,
            toogleField = me.getFields().tooglePassword,
            toogleValue = toogleField.getValue();
        if(localStorage.getItem('drl_old_password') && toogleValue == 0) {
            toogleField.toggle();
        }
        me.on('show', function(){
            if(me.getShowOnce() && !localStorage.getItem('drl_old_password')) {
                if(!toogleValue) {
                    Drl.app.getController('Settings').hasToogle = false;
                    toogleField.toggle();
                }
            }
        });
    }
});
