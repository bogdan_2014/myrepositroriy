Ext.define("Drl.view.password.SetPassword", {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.form.FieldSet',
        'Drl.field.PasswordNumber'
    ],
    config: {
        title: L('settings.password.title'),
        cls: 'settings-view',
        height: '100%',
        flex: 1,
        rightButton:  {
            xtype: 'button',
            cls: 'cancel',
            itemId: 'settings-hide-btn',
            align: 'right'
        },
        layout: 'fit',
        itemId: 'setPasswordForm',
        scrollable: null,
        items: [{
            xtype: 'fieldset',
            layout: 'vbox',
            flex: 1,
            height: '100%',
            items: [{
                xtype: 'spacer'
            },{
                xtype: 'panel',
                listeners: {
                    initialize: function () {
                        this.element.on('tap', function(){
                            Drl.app.getController('Settings').setFocusPassword();
                        }, this);
                    }
                },
                items: [{
                    xtype: 'panel',
                    layout: 'hbox',
                    itemId: 'blocked-panel-set-password',
                    items: [{
                        xtype: 'spacer'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'spacer'
                    }]
                }, {
                    xtype: 'panel',
                    itemId: 'set-password-error',
                    html: L('settings.password.passwordErrorNormal'),
                    cls: 'error-panel'
                }]
            }, {
                xtype: 'pasnumfield',
                name: 'password',
                itemId: 'passwordField',
                cls: 'hidden-field'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'panel',
                listeners: {
                    initialize: function () {
                        this.element.on('tap', function(){
                            Drl.app.getController('Settings').setFocusPasswordRepeat();
                        }, this);
                    }
                },
                items: [{
                    xtype: 'panel',
                    layout: 'hbox',
                    itemId: 'blocked-panel-set-passwordRepeat',
                    items: [{
                        xtype: 'spacer'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'spacer'
                    }]
                }, {
                    xtype: 'panel',
                    itemId: 'set-passwordRepeat-error',
                    cls: 'error-panel',
                    html: L('settings.password.passwordRepeatErrorNormal')
                }]
            }, {
                xtype: 'pasnumfield',
                name: 'passwordRepeat',
                itemId: 'passwordRepeatField',
                cls: 'hidden-field'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'textfield',
                name: 'passwordHelp',
                cls: 'passwordHelpField',
                itemId: 'passwordHelpField',
                listeners: {
                    initialize: function(textField) {
                        var input = textField.element.down('input');
                        input.dom.onkeypress = function(event) {
                            if(textField.getValue().length >=25) {
                                return false;
                            }
                        };
                    }
                }
            }, {
                xtype: 'panel',
                itemId: 'set-passwordHint-error',
                html: L('settings.password.passwordHint'),
                cls: 'error-panel'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                docked: 'bottom',
                text: L('settings.password.passwordSaveBtn'),
                cls: 'save-btn',
                itemId: 'save-password-btn'
            }]
        }]
    }
});
