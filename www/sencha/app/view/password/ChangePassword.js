Ext.define("Drl.view.password.ChangePassword", {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.form.FieldSet',
        'Drl.field.PasswordNumber'
    ],
    config: {
        title: L('settings.password.changeTitle'),
        cls: 'settings-view',
        height: '100%',
        flex: 1,
        rightButton:  {
            xtype: 'button',
            cls: 'cancel',
            itemId: 'settings-hide-btn',
            align: 'right'
        },
        scrollable: null,
        layout: 'fit',
        itemId: 'changePasswordForm',
        items: [{
            xtype: 'fieldset',
            layout: 'vbox',
            flex: 1,
            height: '100%',
            items: [{
                xtype: 'spacer'
            },{
                xtype: 'panel',
                listeners: {
                    initialize: function () {
                        this.element.on('tap', function(){
                            Drl.app.getController('Settings').setFocusOldPassword();
                        }, this);
                    }
                },
                items: [{
                    xtype: 'panel',
                    layout: 'hbox',
                    itemId: 'blocked-panel-set-passwordOld',
                    items: [{
                        xtype: 'spacer'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'spacer'
                    }]
                }, {
                    xtype: 'panel',
                    itemId: 'set-passwordOld-error',
                    html: L('settings.password.passwordOldErrorNormal'),
                    cls: 'error-panel'
                }]
            }, {
                xtype: 'pasnumfield',
                name: 'passwordOld',
                itemId: 'passwordOldField',
                cls: 'hidden-field'
            }, {
                xtype: 'spacer'
            },{
                xtype: 'panel',
                listeners: {
                    initialize: function () {
                        this.element.on('tap', function(){
                            Drl.app.getController('Settings').setFocusPassword();
                        }, this);
                    }
                },
                items: [{
                    xtype: 'panel',
                    layout: 'hbox',
                    itemId: 'blocked-panel-set-password',
                    items: [{
                        xtype: 'spacer'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'spacer'
                    }]
                }, {
                    xtype: 'panel',
                    itemId: 'set-password-error',
                    html: L('settings.password.passwordErrorNormal'),
                    cls: 'error-panel'
                }]
            }, {
                xtype: 'pasnumfield',
                name: 'password',
                itemId: 'passwordField',
                cls: 'hidden-field'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'panel',
                listeners: {
                    initialize: function () {
                        this.element.on('tap', function(){
                            Drl.app.getController('Settings').setFocusPasswordRepeat();
                        }, this);
                    }
                },
                items: [{
                    xtype: 'panel',
                    layout: 'hbox',
                    itemId: 'blocked-panel-set-passwordRepeat',
                    items: [{
                        xtype: 'spacer'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'panel',
                        cls: 'rounded-panel'
                    }, {
                        xtype: 'spacer'
                    }]
                }, {
                    xtype: 'panel',
                    itemId: 'set-passwordRepeat-error',
                    cls: 'error-panel',
                    html: L('settings.password.passwordRepeatErrorNormal')
                }]
            }, {
                xtype: 'pasnumfield',
                name: 'passwordRepeat',
                itemId: 'passwordRepeatField',
                cls: 'hidden-field'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'textfield',
                name: 'passwordHelp',
                cls: 'passwordHelpField',
                itemId: 'passwordHelpField',
                listeners: {
                    initialize: function(textField) {
                        var input = textField.element.down('input');
                        input.dom.onkeypress = function(event) {
                            if(textField.getValue().length >=25) {
                                return false;
                            }
                        };
                    }
                }
            }, {
                xtype: 'panel',
                itemId: 'set-passwordHint-error',
                html: L('settings.password.passwordHint'),
                cls: 'error-panel'
            }, {
                xtype: 'button',
                docked: 'bottom',
                text: L('settings.password.passwordSaveChangeBtn'),
                cls: 'save-btn',
                itemId: 'save-change-password-btn'
            }]
        }]
    }
});
