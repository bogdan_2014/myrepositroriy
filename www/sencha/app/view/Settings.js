Ext.define("Drl.view.Settings", {
    extend: 'Ext.Panel',
    config: {
        title:  L('settings.title'),
        layout: 'vbox',
        rightButton: {
            xtype: 'button',
            cls: 'cancel',
            itemId: 'settings-hide-btn',
            align: 'right'
        },
        cls: 'settings-view',
        items: [{
            xtype: 'button',
            text: L('settings.info.title'),
            itemId: 'settings-faq',
            cls: 'btn-as-select'
        },{
            xtype: 'button',
            text: L('settings.password.title'),
            itemId: 'settings-password',
            cls: 'btn-as-select'
        }, {
            xtype: 'button',
            text: L('settings.callback.title'),
            itemId: 'settings-callback',
            cls: 'btn-as-select'
        }, {
            xtype: 'button',
            text: L('settings.rateApp.title'),
            itemId: 'settings-rate-app',
            cls: 'btn-as-select'
        }, {
            xtype: 'panel',
            layout: 'hbox',
            cls: 'panel-as-select',
            items: [{
                xtype: 'panel',
                html: L('settings.appVersion.title')
            },{
                xtype: 'spacer'
            }, {
                xtype: 'panel',
                cls: 'version',
                html: L('settings.appVersion.version')
            }]
        }]
    }
});

