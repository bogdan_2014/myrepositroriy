Ext.define("Drl.view.Faq", {
    extend: 'Ext.Panel',

    config: {
        title: L('settings.info.title'),
        rightButton:  {
            xtype: 'button',
            cls: 'cancel',
            itemId: 'settings-hide-btn',
            align: 'right'
        },
        cls: 'infopage',
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },
        items: [{
            xtype: 'panel',
            style: 'text-align:justify',
            html: L('settings.info.text')
        }]
    }
});
