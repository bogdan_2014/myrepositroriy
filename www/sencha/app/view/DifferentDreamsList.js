Ext.define('Drl.view.DifferentDreamsList', {
    extend: 'Ext.List',
    requires: [
        'Ext.ActionSheet'
    ],
    config: {
        rightButton:  {
            xtype: 'button',
            cls: 'settings',
            itemId: 'show-settings-btn',
            align: 'right'
        },
        store: null,
        type: null,
        infinite: true,
        variableHeights: true,
        scrollToTopOnRefresh: false,
        title: L('dreams.title'),
        cls: 'dream-diff-list',
        itemTpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    if(values.type == 'dream') {
                    var str = '<div class="dream-diff-list-item">';
                            str+= '<div style="background:url(' + values.url + ') no-repeat;';
                            str+=';background-size: cover;background-position: 50%; height:100%" class="img"></div>';
                            str+= '<div class="div-text">';
                                str+= '<div class="left text">';
                                    str+= '<h1>' + Ext.Date.format(values.date, 'd.m.Y');
                                    if(values.dateFin) {
                                        str += ' - ' +  Ext.Date.format(values.dateFin, 'd.m.Y');
                                    }
                                    str += '</h1>';
                                    str+= '<p>' + values.description +'</p>';
                                str+= '</div>';
                                str+= '<div class="right edit">';
                                str+= '</div>';
                            str+= '<div class="clear"></div>';
                            str+= '</div>';
                        str+= '<div class="clear"></div>';
                        str+= '</div>';
                        return str;
                    }
                }
            }
        ),
        deferEmptyText : true,
        emptyText : L('dreams.empty')
    },
    initialize: function () {
        var list = this,
            listElement = list.element;

        listElement.on({
            tap: list.changeVafourite,
            delegate: ['.dream-list-item .favourite'],
            scope: list
        });
        listElement.on({
            tap: list.mainSelect,
            delegate: ['.dream-diff-list-item .img', '.dream-diff-list-item .text' ],
            scope: list
        });
        listElement.on({
            tap: list.editSelect,
            delegate: ['.dream-diff-list-item .edit'],
            scope: list
        });

        list.callParent(arguments);
    },
    changeVafourite: function (event, element) {
        var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord(),
            store = me.getStore();

        record.set('favourite', !record.data.favourite );
        record.set('dateEdit', new Date());
        store.sync();

        if(event.target.className == 'favourite disabled') {
            event.target.className = 'favourite active';
        } else {
            event.target.className = 'favourite disabled';
        }
        console.log('favourite select');
    },
    mainSelect: function (event, element) {
            console.log('mainSelect showBusy');
            Drl.app.showBusy();
        var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord(),
            store = me.getStore(),
            index,
            type = me.getType();
        index = store.find('id', record.data.id);

        if(me.getStore().getCount() > 10) {
            Ext.defer(function () {
                Drl.app.getController('Dream').showDreamGallery(store, index);
            },300);
        } else {
            Drl.app.getController('Dream').showDreamGallery(store, index);
        }
    },
    editSelect: function (event, element) {
        var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord(),
            actionSheet;
        actionSheet = Ext.create('Ext.ActionSheet', {
            modal: true,
            record: null,
            cls: 'dream-list-actionsheet',
            hideOnMaskTap: true,
            listeners: {
                initialize: function () {
                    if(Ext.os.is.Android) {
                        this.config.showAnimation = null;
                        this.config.hideAnimation = null;
                    }
                }
            },
            items: [{
                text: L('actionsheet.0'),
                cls: 'actionSheet-remove',
                listeners: {
                    tap: me.onRecordRemove,
                    scope: me
                }
            }, {
                text: L('actionsheet.1'),
                cls: 'actionSheet-edit',
                listeners: {
                    tap: me.onRecordEdit,
                    scope: me
                }
            }, {
                text: L('actionsheet.2'),
                cls: 'actionSheet-send',
                listeners: {
                    tap: me.onRecordSend,
                    scope: me
                }
            }, {
                text: L('actionsheet.3'),
                cls: 'actionSheet-cancel',
                listeners: {
                    tap: me.onActionSheetCancel,
                    scope: me
                }
            }]
        });
        Drl.actionSheet = actionSheet;
        Ext.Viewport.add(Drl.actionSheet);
        Drl.actionSheet.on('hide', function (actionsheet) {
            actionsheet.destroy();
        });
        Drl.actionSheet.setRecord(record);
        if(Ext.os.is.Android) {
            Drl.actionSheet.show(null);
        } else {
            Drl.actionSheet.show();
        }
    },
    onRecordRemove: function (btn) {
        Drl.actionSheet.hide();
        var me = this,
            store = me.getStore(),
            record = btn.up().getRecord();
        var index = store.find('id', record.data.id);
        Drl.app.showQuestion({
            title: L('dreams.message.titleDelete'),
            message: L('dreams.message.deleteText'),
            fn: function (answer) {
                if(answer == 'yes') {
                    Drl.app.getController('Store').removeRecord(record, store);
                }
            },
            buttons: [{
                text: L('actionsheet.3'),
                itemId: 'actionSheet-q-cancel'
            }, {
                text: L('actionsheet.0'),
                itemId: 'actionSheet-q-remove',
                ui : 'action'
            }]
        });
    },
    onRecordEdit: function (btn) {
        Drl.actionSheet.hide();
        var record = btn.up().getRecord();
        Drl.app.getController('Dream').showEditDream(record);
    },
    onRecordSend: function (btn) {
        var me = this,
            record = btn.up().getRecord();

        window.plugins.socialsharing.available(function(isAvailable) {
            // the boolean is only false on iOS < 6
            if (isAvailable) {
                if(!me.social) {
                    me.social = Ext.Viewport.add({
                        xtype: 'panel',
                        cls: 'share',
                        modal: true,
                        record: null,
                        /*showAnimation: {
                            type: 'popIn',
                            duration: 250,
                            easing: 'ease-out'
                        },
                        hideAnimation: {
                            type: 'popOut',
                            duration: 250,
                            easing: 'ease-out'
                        },*/
                        centered: true,
                        width: '84%',
                        items: [{
                            docked: 'bottom',
                            xtype: 'button',
                            cls: 'cancel',
                            listeners: {
                                tap: function (btn) {
                                    btn.up().hide();
                                }
                            },
                            text: L('actionsheet.3')
                        }]
                    });
                var panel = Ext.create('Ext.Panel', {
                    layout: 'hbox',
                    items: [{
                        xtype: 'spacer'
                    }, {
                        xtype: 'button',
                        text: 'Twitter',
                        listeners: {
                            tap: function (btn) {
                                var record = btn.up().up().getRecord();
                                Drl.app.getController('Main').socialShare(record, 'twitter');
                            }
                        },
                        cls: 'twitter'
                    },{
                        xtype: 'spacer'
                    }, {
                        xtype: 'button',
                        text: 'Facebook',
                        listeners: {
                            tap: function (btn) {
                                var social = btn.up().up(),
                                    record = social.getRecord();
                                social.hide();
                                Drl.app.getController('Main').socialShare(record, 'facebook');
                            }
                        },
                        cls: 'facebook'
                    },{
                        xtype: 'spacer'
                    }]
                });
                console.log('before canShareVia whatsapp');
               /* window.plugins.socialsharing.canShareVia(
                    'whatsapp',
                    'msg',
                    null,
                    null,
                    null,
                    function(e){
                        console.log('in canShareVia whatsapp ' + e);
                        panel.add(Ext.create('Ext.Button', {
                            xtype: 'button',
                            text: 'WhatsApp',
                            listeners: {
                                tap: function (btn) {
                                    var social = btn.up().up(),
                                        record = social.getRecord();
                                    social.hide();
                                    Drl.app.getController('Main').socialShare(record, 'whatsapp');
                                }
                            },
                            cls: 'whatsapp'
                        }));
                        panel.add({
                            xtype: 'spacer'
                        });
                    }
                    );*/
                console.log('before canShareVia sms');
                window.plugins.socialsharing.canShareVia(
                    'sms',
                    'msg',
                    null,
                    null,
                    null,
                    function(e){
                        console.log('in canShareVia sms ' + e);
                        panel.add(Ext.create('Ext.Button', {
                            text: 'SMS',
                            listeners: {
                                tap: function (btn) {
                                    var social = btn.up().up(),
                                        record = social.getRecord();
                                    social.hide();
                                    Drl.app.getController('Main').socialShare(record, 'sms');
                                }
                            },
                            cls: 'sms'
                        }));
                        panel.add({
                            xtype: 'spacer'
                        });
                    }
                    );
                    panel.add(Ext.create('Ext.Button', {
                        text: 'E-mail',
                        listeners: {
                            tap: function (btn) {
                                var social = btn.up().up(),
                                    record = social.getRecord();
                                social.hide();
                                Drl.app.getController('Main').socialShare(record, 'email');
                            }
                        },
                        cls: 'email'
                    }));
                    panel.add({
                        xtype: 'spacer'
                    });
                    me.social.add(panel);
                }
                me.social.setRecord(record);
                me.social.show();
            } else {
                Drl.app.showMessage({
                    title: L('social.title'),
                    message: L('social.error')
                });
            }
        });

        Drl.actionSheet.hide();
    },
    onActionSheetCancel: function(){
        Drl.actionSheet.hide();
    }
});
