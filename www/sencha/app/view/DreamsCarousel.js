Ext.define("Drl.view.DreamsCarousel", {
    extend: 'Drl.carousel.CyclicCarousel',
    config: {
        direction: 'horizontal',
        cls: 'drl-carousel',
        itemId: 'dreamCarousel',
        height: '100%',
        width:'100%',
        store: null,
        index: null,
        scrollable: null,
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
        items: [{
            xtype: 'titlebar',
            title: L('carousel.title'),
            docked: 'top',
            cls: 'drl-viewpotr-titlebar',
            items: [{
                cls: 'cancel',
                align: 'right',
                listeners: {
                    tap: function (btn) {
                        if(Ext.os.is.Android) {
                            Drl.carouselview.hide(null);
                        } else {
                            console.log('2 in hide carousel');
                            Drl.carouselview.hide();
                        }
                    }
                }
            }]
        }]
    },
    launch: function () {
        var me = this,
            store = me.getStore();
        store.each(function(item, index) {
            me.addItem(item, index);
        });
        me.addLastFirs();
        if(me.getIndex()) {
            me.setActiveItem(me.getIndex() + 1);
        }
    },
    addItem: function(record, index){
        var me = this,
            data = record.getData();
        var str= '<div style="background: url(' + data.url + ') no-repeat;';
        str+=';background-size: cover;background-position: 50%;" class="view-photo"></div>';
        var panel = Ext.create('Ext.Panel', {
            layout: 'vbox',
            record: record,
            cls: 'drl-carousel-inner-item',
            items: [{
                html: str,
                cls: 'drl-carousel-img'
            }, {
                tpl: new Ext.XTemplate(
                    '<div class="text"><p>{description}</p></div>'
                ),
                data: data,
                flex: 1,
                scrollable: {
                    direction: 'vertical',
                    directionLock: true
                },
                cls: 'drl-carousel-desc'
            }, {
                xtype: 'panel',
                docked: 'bottom',
                margin: '0',
                layout: {
                    type: 'hbox',
                    align: 'center',
                    pack: 'center'
                },
                items: [{
                    xtype: 'button',
                    cls: 'arrow_left',
                    listeners: {
                        tap: function (btn) {
                            btn.up('#dreamCarousel').previous();
                        }
                    }
                }, {
                    xtype: 'spacer',
                    flex: 1
                }, {
                    xtype: 'button',
                    cls: 'zvezda' + (data.favourite ? ' x-button-active': ''),
                    listeners: {
                        tap: function(btn){
                            var carousel = btn.up('#dreamCarousel'),
                                activeItm = carousel.getActiveItem(),
                                record = activeItm.getRecord(),
                                store = carousel.getStore();

                            record.set('favourite', !record.data.favourite );
                            record.set('dateEdit', new Date());
                            store.sync();
                            if(btn.getCls()[0] === 'zvezda') {
                                btn.removeCls('zvezda');
                                btn.addCls('zvezda x-button-active');
                            } else {
                                btn.removeCls('zvezda x-button-active');
                                btn.addCls('zvezda');
                            }
                            carousel.checkForUpdate(record);
                        }
                    },
                    hidden: data.type == 'dream' ? false : true
                }, {
                    xtype: 'spacer',
                    width: '1em',
                    hidden: data.type == 'dream' ? false : true
                }, {
                    xtype: 'button',
                    cls: 'finished' + (data.finished ? ' x-button-active': ''),
                    listeners: {
                        tap: function(btn){
                            var carousel = btn.up('#dreamCarousel'),
                                activeItm = carousel.getActiveItem(),
                                record = activeItm.getRecord(),
                                store = carousel.getStore();
                            Drl.app.getController('Store').onDreamFinished(record, store, carousel);
                            if(btn.getCls()[0] === 'finished') {
                                btn.removeCls('finished');
                                btn.addCls('finished x-button-active');
                                Drl.app.showMessage({
                                    title: L('dreams.message.titleSuccess'),
                                    message: (record.get('type') == "dream" ? L('dreams.message.succesFinished') : L('ideas.message.succesFinished'))
                                });
                            } else {
                                btn.removeCls('finished x-button-active');
                                btn.addCls('finished');
                                Drl.app.showMessage({
                                    title: L('dreams.message.titleFailure'),
                                    message: (record.get('type') == "dream" ? L('dreams.message.failureFinished') : L('ideas.message.failureFinished'))
                                });
                            }
                        }
                    }
                }, {
                    xtype: 'spacer',
                    flex: 1
                }, {
                    xtype: 'button',
                    cls: 'arrow_right',
                    listeners: {
                        tap: function (btn) {
                            btn.up('#dreamCarousel').next();
                        }
                    }
                }]
            }]
        });
        me.insert(index, panel);
    },
    addLastFirs: function(){
        var me = this,
            store = me.getStore();

        me.addItem(store.first(), store.getCount());
        me.addItem(store.last(), 0);
        me.setActiveItem(1);
    },
    checkForUpdate: function(record) {
        var me = this;
        if (me.activeIndex === me.getMaxItemIndex()-1) {
            me.removeAt(0);
            me.addItem(record, 0);
        }
        if (me.activeIndex === 1) {
            me.removeAt(me.getMaxItemIndex());
            me.addItem(record, me.getMaxItemIndex()+1);
        }
    }
});
