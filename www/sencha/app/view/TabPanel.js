Ext.define('Drl.view.TabPanel', {
    extend: 'Ext.TabPanel',
    alias: 'widget.main-tab-panel',
    requires: [
        'Drl.view.Main'
    ],
    config: {
        fullscreen: true,
        cls: 'mainTabBottom',
        tabBar: {
            docked: 'bottom',
            layout: {
                pack: 'center',
                type: 'hbox'
            }
        },
        layout: {
            type: 'card',
            showAnimation: false,
            hideAnimation: false,
            animation: false
        },
        items: [{
            iconCls: 'zvezda',
            itemId:'btnZvezda',
            xtype: 'main'
        }, {
            iconCls: 'list-dreams',
            itemId:'btnListDreams',
            xtype: 'main'
        },{
            iconCls: 'add-dream',
            cls: 'add-dream-tab',
            itemId:'btnAddDream',
            xtype: 'main'
        }, {
            iconCls: 'finished',
            itemId:'btnFinished',
            xtype: 'main'
        }, {
            iconCls: 'idea',
            itemId:'btnIdea',
            xtype: 'main'
        }]
    },
    push:function (view) {
        var me = this;
        me.getActiveItem().push(view);
    },
    pop:function (pop) {
        var me = this;
        me.getActiveItem().pop(pop);
    },
    removePreviousViews: function() {
        var me = this.getActiveItem();
        me.removePreviousViews();
    },
    oldActiveItem: null,

    /**
     * Updates this container with the new active item.
     * @param {Object} tabBar
     * @param {Object} newTab
     * @return {Boolean}
     */
    doTabChange: function(tabBar, newTab) {
        var oldActiveItem = this.getActiveItem(),
            newActiveItem;

        this.setActiveItem(tabBar.indexOf(newTab));
        newActiveItem = this.getActiveItem();
        return this.forcedChange || oldActiveItem !== newActiveItem;
    },
      /**
     * Updates this container with the new active item.
     * @param {Object} tabBar
     * @param {Object} newTab
     * @return {Boolean}
     */
    doSetActiveItem: function(newActiveItem, oldActiveItem) {
        if (newActiveItem) {
            var items = this.getInnerItems(),
                oldIndex = items.indexOf(oldActiveItem),
                newIndex = items.indexOf(newActiveItem),
                reverse = oldIndex > newIndex,
                animation = this.getLayout().getAnimation(),
                tabBar = this.getTabBar(),
                oldTab = tabBar.parseActiveTab(oldIndex),
                newTab = tabBar.parseActiveTab(newIndex);

            if (animation && animation.setReverse) {
                animation.setReverse(reverse);
            }

            this.callParent(arguments);

            if (newIndex != -1) {
                this.forcedChange = true;
                tabBar.setActiveTab(newIndex);
                this.forcedChange = false;

                if (oldTab) {
                    oldTab.setActive(false);
                }

                if (newTab) {
                    Drl.app.getController('Main').activeItemChange(newActiveItem);
                    this.oldActiveItem = oldActiveItem;
                    newTab.setActive(true);
                }
            }
        }
    }
});
