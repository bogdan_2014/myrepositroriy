Ext.define('Drl.view.DreamsList', {
    extend: 'Ext.List',
    requires: [
        'Ext.ActionSheet'
    ],
    config: {
        rightButton: null,
        title: null,
        store: null,
        type: null,

        cls: 'dream-list',
        itemTpl: new Ext.XTemplate(
            '{[this.setTpl(values)]}',
            {
                setTpl: function(values) {
                    var str = '<div class="dream-list-item">';
                        str+= '<div class="img-date-selectors">';
                            str+='<div class="img left" style="background:url(' + values.url + ') no-repeat;';
                            str+=';background-size: cover;background-position: 50%;"></div>';
                            str+='<div class="date-selectors right">';
                                str+='<div class="top">';
                                    str+='<div class="date left">';
                                        str+= Ext.Date.format(values.date, 'd.m.Y');
                                        if(values.dateFin) {
                                            str += ' - ' +  Ext.Date.format(values.dateFin, 'd.m.Y');
                                        }
                                    str+='</div>';
                                    str+= '<div class="edit right"></div>';
                                str+='</div>';
                                str+= '<div class="clear"></div>';
                                str+='<div class="selectors">';
                                    str+= '<div class="open disabled"></div>';
                                    if(values.type !== 'idea') {
                                        str+= '<div class="favourite ' + (values.favourite ? 'active' : 'disabled' ) + '">';
                                        str+= '</div>';
                                    }
                                    str+= '<div class="finished ' + (values.finished ? 'active' : 'disabled' ) + '">';
                                    str+= '</div>';
                                    str+= '<div class="tresh disabled"></div>';
                                str+='</div>';
                            str+='</div>';
                        str+='</div>';
                        str+= '<div class="clear"></div>';
                        str+= '<div class="text">';
                        if(values.description.length !== 0) {
                            str+= '<p>' + values.description +'</p>';
                        }
                        str+= '</div>';
                        str+= '<div class="clear"></div>';
                    str+= '</div>';
                    return str;
                }
            }
        ),
        deferEmptyText : true
    },
    initialize: function () {
        var list = this,
            listElement = list.element,
            type = list.getType(),
            emptyText;
        list.setRightButton({
            xtype:'button',
            cls: 'hidden-btn-nav',
            align: 'right'
        });
        var title = '';
        switch (type) {
        case 'finDreams' :
            emptyText = L('finished.empty');
            title = L('finished.title');
            break;
        case 'ideas' :
            emptyText = L('ideas.empty');
            title = L('ideas.title');
            list.setRightButton({
                xtype: 'button',
                cls: 'add-idea',
                itemId: 'addIdea',
                align: 'right'
            });
            break;
        default:
            emptyText = L('allDreams.empty');
            title = L('allDreams.title');
        }
        list.setEmptyText(emptyText);
        list.setTitle(title);

        listElement.on({
            tap: list.changeVafourite,
            touchstart: list.onSelectorsPress,
            touchend: list.onSelectorsRelease,
            delegate: ['.dream-list-item .favourite'],
            scope: list
        });
        listElement.on({
            tap: list.changeFinished,
            touchstart: list.onSelectorsPress,
            touchend: list.onSelectorsRelease,
            delegate: ['.dream-list-item .finished'],
            scope: list
        });
        listElement.on({
            tap: list.editItem,
            touchstart: list.onSelectorsPress,
            touchend: list.onSelectorsRelease,
            delegate: ['.dream-list-item .open'],
            scope: list
        });
        listElement.on({
            tap: list.removeItem,
            touchstart: list.onSelectorsPress,
            touchend: list.onSelectorsRelease,
            delegate: ['.dream-list-item .tresh'],
            scope: list
        });
        listElement.on({
            tap: list.editSelect,
            delegate: ['.dream-list-item .edit'],
            scope: list
        });
        listElement.on({
            tap: list.mainSelect,
            delegate: ['.dream-list-item .date', '.dream-list-item .img','.dream-list-item .text'],
            scope: list
        });

        list.callParent(arguments);
    },
    changeVafourite: function (event, element) {
        var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord(),
            store = me.getStore();

        record.set('favourite', !record.data.favourite );
        record.set('dateEdit', new Date());
        store.sync();

        if(event.target.className == 'favourite disabled') {
            event.target.className = 'favourite active';
        } else {
            event.target.className = 'favourite disabled';
        }
    },
    onSelectorsPress: function(event, div) {
        if(Ext.get(div).hasCls('active')) {
            Ext.get(div).removeCls('active');
            Ext.get(div).addCls('disabled');
        } else {
            Ext.get(div).removeCls('disabled');
            Ext.get(div).addCls('active');
        }
    },
    onSelectorsRelease: function(event, div) {
        if(Ext.get(div).hasCls('active')) {
            Ext.get(div).removeCls('active');
            Ext.get(div).addCls('disabled');
        } else {
            Ext.get(div).removeCls('disabled');
            Ext.get(div).addCls('active');
        }
    },
    changeFinished: function (event, element) {
        var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord(),
            store = me.getStore();

        if(event.target.className == 'finished disabled') {
            event.target.className = 'finished active';
            Drl.app.showMessage({
                title: L('dreams.message.titleSuccess'),
                message:(record.get('type') == "dream" ? L('dreams.message.succesFinished') : L('ideas.message.succesFinished')),
                fn: function(){
                    Drl.app.getController('Store').onDreamFinished(record, store);
                }
            });
        } else {
            event.target.className = 'finished disabled';
            Drl.app.showMessage({
                title: L('dreams.message.titleFailure'),
                message: (record.get('type') == "dream" ? L('dreams.message.failureFinished') : L('ideas.message.failureFinished')),
                fn: function(){
                    Drl.app.getController('Store').onDreamFinished(record, store);
                }
            });
        }
    },
    mainSelect: function (event, element) {
            console.log('mainSelect showBusy');
            Drl.app.showBusy();
        var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord(),
            store = me.getStore(),
            index,
            type = me.getType();

        index = store.find('id', record.data.id);

        if(me.getStore().getCount() > 10) {
            Ext.defer(function () {
                Drl.app.getController('Dream').showDreamGallery(store, index);
            },300);
        } else {
            Drl.app.getController('Dream').showDreamGallery(store, index);
        }
    },
    editSelect: function (event, element) {
        var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord(),
            actionSheet;
        actionSheet = Ext.create('Ext.ActionSheet', {
            modal: true,
            record: null,
            cls: 'dream-list-actionsheet short',
            hideOnMaskTap: true,
            listeners: {
                initialize: function () {
                    if(Ext.os.is.Android) {
                        this.config.showAnimation = null;
                        this.config.hideAnimation = null;
                    }
                }
            },
            items: [{
                text: L('actionsheet.2'),
                cls: 'actionSheet-send',
                listeners: {
                    tap: me.onRecordSend,
                    scope: me
                }
            }, {
                text: L('actionsheet.3'),
                cls: 'actionSheet-cancel',
                listeners: {
                    tap: me.onActionSheetCancel,
                    scope: me
                }
            }]
        });
        Drl.actionSheet = actionSheet;
        Ext.Viewport.add(Drl.actionSheet);
        Drl.actionSheet.on('hide', function (actionsheet) {
            actionsheet.destroy();
        });
        Drl.actionSheet.setRecord(record);
        if(Ext.os.is.Android) {
            Drl.actionSheet.show(null);
        } else {
            Drl.actionSheet.show();
        }
    },
    removeItem: function (event, element) {
         var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord(),
            store = me.getStore();
        Drl.app.showQuestion({
            title: L('dreams.message.titleDelete'),
            message: (record.get('type') == 'dream' ? L('dreams.message.deleteText') : L('ideas.message.deleteText')),
            fn: function (answer) {
                if(answer == 'yes') {
                    Drl.app.getController('Store').removeRecord(record, store);
                }
            },
            buttons: [{
                text: L('actionsheet.3'),
                itemId: 'actionSheet-q-cancel'
            }, {
                text: L('actionsheet.0'),
                itemId: 'actionSheet-q-remove',
                ui : 'action'
            }]
        });
    },
    editItem: function (event, element) {
        var me = this,
            record = Ext.getCmp(Ext.get(element).parent('.x-list-item').getId()).getRecord();
        Drl.app.getController('Dream').showEditDream(record);
    },
    onRecordSend: function (btn) {
        var me = this,
            record = btn.up().getRecord();

        window.plugins.socialsharing.available(function(isAvailable) {
            // the boolean is only false on iOS < 6
            if (isAvailable) {
                if(!Drl.social) {
                    Drl.social = Ext.Viewport.add({
                        xtype: 'panel',
                        cls: 'share',
                        modal: true,
                        record: null,
                        showAnimation:null,
                        hideAnimation: null,
                        /*showAnimation: {
                            type: 'popIn',
                            duration: 250,
                            easing: 'ease-out'
                        },
                        hideAnimation: {
                            type: 'popOut',
                            duration: 250,
                            easing: 'ease-out'
                        },*/
                        centered: true,
                        width: '84%',
                        items: [{
                            docked: 'bottom',
                            xtype: 'button',
                            cls: 'cancel',
                            listeners: {
                                tap: function (btn) {
                                    btn.up().hide();
                                }
                            },
                            text: L('actionsheet.3')
                        }]
                    });
                var panel = Ext.create('Ext.Panel', {
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [{
                        xtype: 'button',
                        text: 'Twitter',
                        listeners: {
                            tap: function (btn) {
                                var record = btn.up().up().getRecord();
                                Drl.app.getController('Main').socialShare(record, 'twitter');
                            }
                        },
                        cls: 'twitter'
                    }, {
                        xtype: 'button',
                        text: 'Facebook',
                        listeners: {
                            tap: function (btn) {
                                var social = btn.up().up(),
                                    record = social.getRecord();
                                social.hide();
                                Drl.app.getController('Main').socialShare(record, 'facebook');
                            }
                        },
                        cls: 'facebook'
                    }]
                });
                console.log('before canShareVia whatsapp');
               /* window.plugins.socialsharing.canShareVia(
                    'whatsapp',
                    'msg',
                    null,
                    null,
                    null,
                    function(e){
                        console.log('in canShareVia whatsapp ' + e);
                        panel.add(Ext.create('Ext.Button', {
                            xtype: 'button',
                            text: 'WhatsApp',
                            listeners: {
                                tap: function (btn) {
                                    var social = btn.up().up(),
                                        record = social.getRecord();
                                    social.hide();
                                    Drl.app.getController('Main').socialShare(record, 'whatsapp');
                                }
                            },
                            cls: 'whatsapp'
                        }));
                        panel.add({
                            xtype: 'spacer'
                        });
                    }
                    );*/
                console.log('before canShareVia sms');
                window.plugins.socialsharing.canShareVia(
                    'sms',
                    'msg',
                    null,
                    null,
                    null,
                    function(e){
                        console.log('in canShareVia sms ' + e);
                        panel.add(Ext.create('Ext.Button', {
                            text: 'SMS',
                            listeners: {
                                tap: function (btn) {
                                    var social = btn.up().up(),
                                        record = social.getRecord();
                                    social.hide();
                                    Drl.app.getController('Main').socialShare(record, 'sms');
                                }
                            },
                            cls: 'sms'
                        }));
                    }
                    );
                    panel.add(Ext.create('Ext.Button', {
                        text: 'E-mail',
                        listeners: {
                            tap: function (btn) {
                                var social = btn.up().up(),
                                    record = social.getRecord();
                                social.hide();
                                Drl.app.getController('Main').socialShare(record, 'email');
                            }
                        },
                        cls: 'email'
                    }));
                    Drl.social.add(panel);
                }
                Drl.social.setRecord(record);
                Drl.social.show();
            } else {
                Drl.app.showMessage({
                    title: L('social.title'),
                    message: L('social.error')
                });
            }
        });

        Drl.actionSheet.hide();
    },
    onRecordSendFacebook: function (btn) {
        Drl.actionSheet.hide();
        var record = btn.up().getRecord();
        console.log('url = ' + record.get('url'));
        window.plugins.socialsharing.share(
          'Optional message',
          'Optional title',
          [record.get('url')],
          null);
        //window.plugins.socialsharing.share('Share message', 'Share title', record.get('url'), function() {console.log('share ok')}, function(errormsg){console.log(errormsg)});
    },
    onActionSheetCancel: function(){
        Drl.actionSheet.hide();
    }
});
