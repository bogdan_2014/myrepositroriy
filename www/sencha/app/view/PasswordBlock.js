Ext.define("Drl.view.PasswordBlock", {
    extend: 'Ext.Panel',
    config: {
        cls: 'blocked-panel',
        itemId: 'passwordBlock',
        height: '100%',
        width:'100%',
        zIndex:0,
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
        scrollable: null,
        password: '',
        layout: 'vbox',
        items: [{
            xtype: 'panel',
            cls: 'logotip'
        }, {
            xtype: 'spacer'
        }, {
            xtype: 'panel',
            layout: 'hbox',
            itemId: 'blocked-panel-password',
            items: [{
                xtype: 'spacer'
            }, {
                xtype: 'panel',
                cls: 'rounded-panel'
            }, {
                xtype: 'panel',
                cls: 'rounded-panel'
            }, {
                xtype: 'panel',
                cls: 'rounded-panel'
            }, {
                xtype: 'panel',
                cls: 'rounded-panel'
            }, {
                xtype: 'spacer'
            }]
        }, {
            xtype: 'spacer'
        }, {
            xtype: 'panel',
            itemId: 'blocked-panel-error',
            cls: 'error-panel',
            html: L('settings.password.passwordErrorBlock')
        }, {
            xtype: 'spacer'
        }, {
            xtype: 'panel',
            layout: 'hbox',
            items: [{
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '1',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '2',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '3',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }]
        }, {
            xtype: 'panel',
            layout: 'hbox',
            items: [{
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '4',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '5',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '6',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }]
        }, {
            xtype: 'panel',
            layout: 'hbox',
            items: [{
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '7',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '8',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '9',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }]
        }, {
            xtype: 'panel',
            layout: 'hbox',
            style: 'margin-bottom:10px',
            items: [{
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '?',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedHelp(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: '0',
                listeners: {
                    tap: function(btn){
                        Drl.app.getController('Settings').onBlockedButton(btn);
                    }
                },
                cls: 'rounded-button'
            }, {
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: ' ',
                listeners: {
                    tap: function(){
                        Drl.app.getController('Settings').onBlockedBackSpace();
                    }
                },
                cls: 'button-backspace'
            }, {
                xtype: 'spacer'
            }]
        }]
    },
    resetPassword: function(){
        var me = this,
            count = me.getPassword().length;
        for (var i = 0; i < count; i++) {
            Drl.app.getController('Settings').onBlockedBackSpace();
        }
    }
});
