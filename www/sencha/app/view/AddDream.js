Ext.define("Drl.view.AddDream", {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.form.FieldSet',
        'Ext.field.DatePicker',
        'Drl.picker.field.DateTimePicker',
        'Ext.field.Hidden'
    ],
    config: {
        cls: 'drl-grey-form',
        itemId: 'editDreamForm',
        title: null,
        height: '100%',
        width:'100%',
        type: null,
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
        scrollable: null,
        fullscreen: true,
        layout: 'vbox',
        items: [{
            xtype: 'titlebar',
            docked: 'top',
            cls: 'drl-viewpotr-titlebar',
            title: null
        }, {
            xtype: 'fieldset',
            layout: 'vbox',
            scrollable: null,
            flex:1,
            items: [{
                xtype: 'panel',
                layout: 'card',
                itemId: 'editPhotoPanel',
                cls: 'drl-white-panel',
                flex: 1,
                items: [{
                    xtype: 'panel',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [{
                        xtype: 'spacer',
                        flex: 1
                    }, {
                        xtype: 'button',
                        cls: 'get-image',
                        itemId: 'cameraBtn'
                    }, {
                        xtype: 'spacer',
                        width: '1em'
                    }, {
                        xtype: 'button',
                        cls: 'get-library',
                        itemId: 'librariBtn'
                    }, {
                        xtype: 'spacer',
                        flex: 1
                    }]
                }, {
                    xtype: 'panel',
                    height: '100%',
                    cls: 'editPhotoPanel',
                    items: [{
                        xtype: 'button',
                        cls: 'hidden-btn',
                        itemId: 'hiddenEditPhotoBtn',
                        listeners: {
                            initialize: function (me) {
                                me.element.on('taphold', function(e, t) {
                                    Drl.app.dispatch({
                                        controller: 'Dream',
                                        action: 'onTapHoldHdnEditPhotoBtn'
                                    }, false);
                                }, me);
                                me.element.on('singletap', function(e, t) {
                                    Drl.app.dispatch({
                                        controller: 'Dream',
                                        action: 'onHdnEditPhotoBtn'
                                    }, false);
                                }, me);
                            }
                        }
                    }, {
                        xtype: 'button',
                        iconCls: 'cancel-image',
                        cls: 'rmvEditPhotoImg',
                        itemId: 'removeEditPhotoImg',
                        hidden: true
                    }]
                }]
            }, {
                xtype: 'hiddenfield',
                name: 'url'
            }, {
                xtype: 'hiddenfield',
                name: 'dateEdit',
                value: new Date()
            }, {
                xtype: 'hiddenfield',
                name: 'id'
            }, {
                xtype: 'hiddenfield',
                name: 'type',
                value: 'dream'
            }, {
                xtype: 'hiddenfield',
                name: 'favourite',

                value: false
            }, {
                xtype: 'hiddenfield',
                name: 'finished',
                value: false
            }, {
                xtype: 'textareafield',
                autoCapitalize : 'on',
                placeHolder: L('addDream.placeHolder.description'),
                maxRows: 3,
                disabled: true,
                cls: 'add-dream-textarea',
                name: 'description',
                listeners: {
                    initialize: function(textField) {
                        var input = textField.element.down('textarea');
                        input.dom.onkeypress = function(event) {
                            if(textField.getValue().length >=250) {
                                return false;
                            }
                        };
                    }
                }
            }, {
                name: 'date',
                label: L('addDream.label.date'),
                xtype: 'datepickerfield',
                dateFormat: 'd.m.Y',
                labelWidth: '50%',
                cls: 'input-align-right',
                picker: {
                    xtype: 'datepicker',
                    yearFrom: new Date().getFullYear()-10,
                    yearTo: new Date().getFullYear()+10,
                    listeners: {
                        initialize: function () {
                            if(Ext.os.is.Android) {
                                this.setShowAnimation(null);
                                this.setHideAnimation(null);
                            }
                        }
                    },
                    useTitles: true
                }
            }, {
                xtype: 'selectfield',
                name: 'repeatMember',
                itemId: 'selectRepeat',
                listeners: {
                    initialize: function () {
                        if(Ext.os.is.Android) {
                            this.config.showAnimation = null;
                            this.config.hideAnimation = null;
                            this.setDefaultPhonePickerConfig({
                                showAnimation:null,
                                hideAnimation:null
                            });
                        }
                    }
                },
                labelWidth: '50%',
                cls: 'input-align-right',
                label: L('addDream.label.repeat'),
                options: [{
                    text: L('addDream.options.0'),
                    value: 0
                }, {
                    text: L('addDream.options.2'),
                    value: 2
                }, {
                    text: L('addDream.options.3'),
                    value: 3
                }, {
                    text: L('addDream.options.4'),
                    value: 4
                }, {
                    text: L('addDream.options.5'),
                    value: 5
                }, {
                    text: L('addDream.options.6'),
                    value: 6
                }, {
                    text: L('addDream.options.7'),
                    value: 7
                }]
            }, {
                xtype: 'datetimepickerfield',
                label: L('addDream.label.remember'),
                name: 'remember',
                hidden: true,
                dateTimeFormat: 'H:i',
                picker: {
                    slotOrder: ['hour','minute'],
                    useTitles: true,
                    minuteInterval : 1,
                    listeners: {
                        initialize: function () {
                            if(Ext.os.is.Android) {
                                this.setShowAnimation(null);
                                this.setHideAnimation(null);
                            }
                        }
                    },
                    ampm: false
                },
                labelWidth: '50%',
                cls: 'input-align-right'
            }, {
                xtype: 'panel',
                docked: 'bottom',
                margin: '.5em 0 0 0',
                layout: {
                    type: 'hbox',
                    align: 'center',
                    pack: 'center'
                },
                items: [{
                    xtype: 'button',
                    cls: 'big-cancel',
                    itemId: 'bigCancel'
                }, {
                    xtype: 'spacer',
                    flex: 1
                }, {
                    xtype: 'button',
                    cls: 'big-save',
                    itemId: 'bigSave'
                }]
            }]
        }]
    },
    initialize: function () {
        var me = this,
            record = me.getRecord(),
            title = '';
        if(!record) {
            if(me.getType() == 'dream') {
                title = L('addDream.title.add');
            } else {
                title = L('addIdea.title.add');
            }
            me.getFields().date.setValue(new Date());
            me.getInnerAt(0).getInnerAt(0).setActiveItem(0);
        } else {
            title = L('addDream.title.edit');
            Drl.app.dispatch({
                controller: 'Dream',
                action: 'changeImg',
                args: [record.data.url]
            }, false);
        }
        me.down('titlebar').setTitle(title);
        me.on('show', function (view) {
            Ext.defer(function () {
                console.log(' Drl.editview on show');
                Drl.editview.getFieldsArray()[6].setDisabled(false);
            }, 400);
        });
        me.on('hide', function (view) {
            view.destroy(true);
            Drl.editview = null;
        });

        me.callParent(arguments);
    }
});
