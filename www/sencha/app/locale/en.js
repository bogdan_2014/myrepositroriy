/**
 *  Resource file for english language.
 */
Ext.define('Drl.locale.en', {
    singleton: true,
    requires: [
        'Ext.MessageBox',
        'Ext.picker.Picker',
        'Ext.picker.Date',
        'Ext.IndexBar',
        'Ext.LoadMask',
        'Ext.dataview.DataView',
        // 'Ext.NestedList',
        'Ext.plugin.PullRefresh',
        // 'Ext.plugin.ListPaging',
        'Ext.util.Format',
        'Drl.mixin.Messageable'
    ],
    translation: {
        backBtn: {
            text: 'Back'
        },
        title: {
            error: 'Error'
        },
        social: {
            title: 'Sorry',
            header: 'Shared via Dreams Come True application!',
            download: 'Download Dreams Come True application!',
            error: 'Requires iOs 6.0 or later'
        },
        settings: {
            title: 'Settings',
            info: {
                title: 'Information',
                text: "<p><b>Dear users!</b><br><br>We are pleased to introduce you a new and unique «Dreams» application that will help you to get closer to your dreams, and you will learn how to easily control and realize them!<br><br><b>What is a dream?</b><br><br>Dream is the first step to our purpose!<br><br>Dream – it is our thoughts, desires that seem inaccessible and impossible to us, but it’s only at first sight.<br><br>Dream is a power, meaning of our life, an interest in our life and our motivator! Without dreams we have no purposes, no problems, and no achievements.<br><br><b>What does it mean to dream?</b><br><br>To play with fancy, fall into a play of thoughts, to imagine, to think, to imagine what we have not in the present. Somebody like imagining before bedtime, but others dream always. And there are those who forbid themselves to play with fancy. Each person has its own dream. Someone wishes a peace without wars, someone dreams of a great feat, someone wants to travel, someone dreams of business, love, children, a house, a car, a yacht, and someone wishes a plate of hot soup. And every dream has the right to its existence. Because the dream comes to us in order that we could realize it. And, namely, to realize it during the right period of time. Because at every age we have our own dreams and purposes in our life.<br><br>Psychologists have argued that people are divided into different categories. Those who have dreams and purposes and ideas, we will not tell about. Because such people often realize their dreams, desires, purposes and ideas into their life! Let's talk about those who have no dreams, no goals and no purposes. There are people who dream a lot and often, but do nothing realize their imaginary thoughts. Others make themselves stop dreaming. They have forbidden themselves it just because their life (in their opinion) didn’t give them what they had wished. When we do not allow ourselves to wish or stop striving for the subject of our desire, we begin to feel that we have no dreams. But the dream is always exist and moreover - it has everyone! Perhaps it has not been visualized, or defined, or outlined. Sometimes a man is afraid to put it out. Why he or she is afraid of it? A man is afraid of this because it does not realize - whether it its dream, and whether or not he wants to realize it, and the most importantly – it is afraid that the dream will have to strive for. Fear of beginning of the acts will always stop a man! Fear of error. There is a lot of such fears. One of them discover a lot of reasons why they cannot realize their dreams, the others discover a lot of arguments of why nothing will never happen. For someone it is difficult to step over their beliefs. But in spite of everything, just the dream allows us to live and strive for our dream every day (sometimes unconsciously). The strongest motivation for any action - it is the same dream! If it's yours and if it does not allow you to sleep peacefully, if that dream always reminds you about itself, so it really is your dream! And only you can realize it. Will it be difficult? Yes, of course! Will there be mistakes? Yes! All will be! But if your dream shines like the moon during the night, you can be sure, if you do not give up, to realize it!<br><br><b>Importance of working with the dreams</b><br><br>When a dream comes true it makes your life more interesting and exciting!<br><br>Simple process of imagination waken your dream and add an importance and meaning into our life!<br><br>When you stop dreaming, you are dying. While you wishing, you are living.<br><br>We always dream and our dream to create our future. But if our fantasies are unconscious and uncontrollable, we can create for a real nightmare for our future. And when we manage our fantasies through the visualization process, we create our future and a dream come true.<br><br>Dreams make the life spice, reviving even the dullest days!<br><br>If we do not dream of our future, how we will know where we are going to?<br><br>When we build our dreams, our dreams build us!  The more we dream, the more we can do.<br><br><b>How to identify your dream, how to find it?</b><br><br>Firstly, it is desirable to write down all your desires. The least, the most reasonable. It may be buying of something else, and also the desires associated with a rest or a work (career). All what you wish for today. And you should start with these small desires. If your dream is hidden too deep, it appears in front of you when you're ready to accept it. But now devote yourself to your small desires. Start to realize them one by one. One small step in a day is better than nothing at all! When you understand that it is not difficult to realize a small desire, it may be born more. After realizing a greater desire and a real dream will come to you! And the most importantly – always ask yourself the question: «What has been done today? What kind of steps have been taken by me to achieve my dreams?». Will you have a dream in your life or if you realize it - everything depends only on you!<br><br>So, let’s highlight the main:<br><br>Dream will become a reality if you strive for it.<br><br>Dreams should be written.<br><br>You should define your dreams (those that will motivate you).<br><br>Start to make your dreams come true beginning with easier in order to believe (if there is no faith) that the dream can become a reality.<br><br>Periodically check yourself if you follow the right direction.<br><br>Dream! And if you wish to make your dreams a reality, then do all in your power to realize them!<br><br><b>How application «Dreams» does work?</b><br><br>Let's suppose that you have already found your dream and believe that you can realize it. Then much hard work is followed to your dream come true. How is it possible to achieve the dream be always at the forefront and never would leave in the shade? Maybe something what it take your breath away and does not go out of your head, might be forgotten tomorrow. You have no the right to forget about your dream! The secret of success is that it is necessary to cherish your hope and work its realization. We are always busy with some small problems only to get rid of them, but understand that small problems will always be much more. We need to clearly understand that firstly we should think about our dreams and purposes, and only then about anything else. For the present instance in our application «Dreams» some convenient reminders has been realized. You can set a reminder for any of your dreams and dream as often as you wish, and at any time convenient for you.<br><br>The secret of working at a dream is firstly laid in concentration maintenance. Create a habit at least from time to time to think about your dream to feel yourself reached it. You will immediately notice for a moment how enthusiasm, vitality and good humor will be grown. Experience the feeling as if your dreams one by one have been come true, feel yourself as reached your dream.<br><br>Specialists point out two main effective methods that really help to realize your dreams and give you the strength and faith for every day. «Dreams» application is based on these methods!<br><br><b>Visualization</b><br><br>Our imagination is a very powerful and it has stronger influence on us than the reasonable thoughts or willpower. It might be our best ally or worst enemy, so we need to control our imagination. When we control our imagination through the visualization process, we create our future and any dream come true.<br><br>Willful control our imagination visualization is called. We can also name it as a fantasy. But there are two forms of such fantasies: conscious and unconscious. Unconscious fantasies are unintentional, they are like a daydream and we are lost in them undesignedly. This is the most widespread form of visual fantasies. They could be undesirable so long as we can imagine negative pictures or repeatedly worry about negative experiences. And our subconscious interprets this imaginary experience as the present, increasing our negative beliefs and habits. Thus, it is obvious that we should be aware of own imagination and use them constructively.<br><br>Since the success of setting our purposes depends on our subconscious beliefs, we should pay more attention to our visual imagination. This involves practice of visualization to change our beliefs.<br><br>With the help of application «Dreams», you can easily help yourself to visualize your dream, it is sufficient to take a photo or upload your existing photos for that, as well as make detailed description what you wish to get. It is important that your photo and description describe your dream as much as possible.<br><br>Since visualization is essential for achieving our goals, let’s consider it in details, how to implement this practice.<br><br><b>How to use visualization?</b><br><br>Find and choose the time and place where you can stay alone for ten minutes and nobody could disturb you.<br><br>Choose one purpose or a dream to work at.<br><br>Look through the picture, read a description of your dreams and be inspired with it.<br><br>Relax, quit breathing, and close your eyes.<br><br>Imagine yourself with your mind's eye as if you have already reached your purpose.<br><br>To help your imagination you can ask yourself following questions: «What the situation will be when I achieve my dream? What will be changed? What will I see, hear, taste, feel and how it will be smelled? Why I'll be glad to this?».<br><br>After 5-6 minutes of imagination of your success, then ask yourself: «How can I make it better?». And during the last four or five minutes, try to imagine still more success.<br><br>Do this every day (optimally, if you do it at the same time, at least 2 times a day during 5-10 minutes), until you succeed.<br><br>It is important to accompany all this process with emotions. Get maximum details and emotions. Visualization should accompany you every day on the way to your purpose. You mustn’t miss a day. You have no right to forget your dream! The farther you will go, the easier it will be to convince your mind that you have already achieved the goal. And after convincing it at 100%, only then your dream will be realized. The essence of visualization – make your mind to see a new reality, to convince that it is real true.<br><br>After a few weeks of practice the unexpected changes will break into your daily experience. This means that without planning you start to make any changes that will make you closer to your dream. And this will be a sign for you, because your subconscious mind has take your visualization sessions for the real experience and led your beliefs into harmony with your goals.<br><br>Application «Dreams» certainly will help you with making wishes come true!<br><br><b>Thanks</b><br><br>What do you usually say when someone give you a good service, help you with something? You are right - «Thank you». And here thank the universe for realizing your dream. Yes, exactly, thank for the fact that your dream has already come true, although in fact it is not really so. Remember that your task is to convince the mind that you have already realized your dream. After believing you, the realization of the dream does not take much time from you. You see, all the way to the realization of the dream is that we work at our faith. Visualization is good in a quiet environment where you can fully concentrate. Thanks a lot easier to use. You can thank anywhere at any time. Just remember that you should feel what you thank for. And do not be afraid to deceive your sub consciousness, thanks for what you have not yet.<br><br><b>Achievement</b><br><br>This step consists in the achievement of a dream. When you give yourself to your dream and in spite of the existing problems decide to realize it you will feel that the work at the realization of your dreams is not as difficult as you have thought. You will find strength, which you haven’t there before. Each day will be filled with meaning. You’ll give up many pointless occupations. Try to find at least 1 hour per day for the realization of your dream. It can be as simple reading of positive literature and certain actions. And above all, note that all activities related to the achievement of the dreams, shall be made with joy. You should feel the joy. If you feel strong fear, or any other negative emotions are stronger than the positive, you should know that you on the wrong way. Something screaming inside of you - do not go there, it's not that way. The main thing during the whole process is feeling a joy. This applies both to the submission process, visualization, and the action itself. Do not focus yourself on any particular method of achieving, just open the application «Dreams», choose one purpose or a dream which you will work at, and dream, imagine, visualize, feel its performance and the universe will show you the best way to realize it.<br><br><b>Remember</b><br><br>When you desire something very much, the universe will help you to achieve that!<br><br>Do not be afraid of changes and do not miss your chance to become happier!<br><br>Exactly so, the Universe will help You!<br><br><b>Earnest request</b><br><br>Dear Users, if you like our idea, please support us! Rate the application «Dreams» and write down your comment, please! Help other people to learn about this application and realize their dreams with the help of application «Dreams»!<br><br><b>We, sincerely wish you that every your dream come true!</b></p>"
            },
            password: {
                title: 'Password',
                changeTitle: 'Edit password',
                enablePas: 'Turn password on',
                disablePas: 'Turn password off',
                passwordSaveSucces: 'Password successfully saved',
                passwordChangeSucces: 'Password successfully changed',
                passwordSaveBtn: 'Save password',
                passwordSaveChangeBtn: 'Change password',
                passwordChangeBtn: 'Change password',
                passwordErrorNormal: 'Enter your new password',
                passwordErrorBlock: 'Enter your password',
                passwordOldErrorNormal: 'Enter your old password',
                passwordOldError: '<span style="font-size:75%">Old password consists of 4 digits</span>',
                passwordOldWrong: '<span style="font-size:75%">Incorrect old password</span>',
                passwordError: '<span style="font-size:75%">Password consists of 4 digits</span>',
                passwordRepeatErrorNormal: 'Re-enter your new password',
                passwordRepeatError: '<span style="font-size:75%">Password consists of 4 digits</span>',
                passwordBothError: '<span style="font-size:75%">Passwords did not match</span>',

                passwordHint: 'Password hint',
                passwordHintShort: 'Hint',
                passwordHintError: '<span style="font-size:75%">Enter password hint</span>',
                passwordBlockError: 'Incorrect password'
            },
            callback: {
                title: 'Feedback'
            },
            rateApp: {
                title: 'Rate Dreams',
                buttons: {
                    0: 'Rate it now',
                    1: 'Remind me later',
                    2: 'No, thanks'
                },
                text: 'If You enjoy using Rate Dreams, <br>would You mind taking a moment<br>to rate it?<br>It won’t take more than a minute.<br>Thanks for your support!'
            },
            appVersion: {
                title: 'Version',
                version : '1.1'
            }
        },
        addDream: {
            title: {
                add: 'Add dream',
                edit: 'Edit'
            },
            placeHolder: {
                description: 'Add description'
            },
            label: {
                date: 'Date',
                repeat: 'Repeat',
                remember: 'Remind'
            },
            options: {
                0: 'Off',
                2: 'Once a minute',
                3: 'Once an hour',
                4: 'Once a day',
                5: 'Once a week',
                6: 'Once a month',
                7: 'Once a year'
            },
            error: {
                image: 'Image not set',
                titleSuccess: 'Excellent',
                success: 'Dream is added',
                successUpdate: 'Dream is edited'
            }
        },
        carousel: {
            title: 'Visualization'
        },
        dreams: {
            title: 'Goals',
            empty: 'No goals, add goal',
            message: {
                titleSuccess: 'Congratulations',
                succesFinished: 'Your dream has just came true',
                titleFailure: 'Don`t worry',
                failureFinished: 'Your dream will surely come true',
                titleDelete: 'Are you sure',
                deleteText: 'Delete dream?'
            }
        },
        allDreams: {
            title: 'Dreams',
            empty: 'No dreams, add dream'
        },
        finished: {
            title: 'Achievements',
            empty: 'Your dream will soon come true'
        },
        ideas: {
            title: 'Ideas',
            empty: 'No ideas, add idea',
            message: {
                succesFinished: 'Your idea has just fulfilled',
                failureFinished: 'Your idea will surely come true',
                deleteText: 'Delete idea?'
            }
        },
        addIdea: {
            title: {
                add: 'Add idea'
            },
            error: {
                success: 'Idea is added',
                successUpdate: 'Idea is edited'
            }
        },
        actionsheet: {
            0: 'Delete',
            1: 'Edit',
            2: 'Share',
            3: 'Cancel'
        },
        datetime: {
            hour: 'Hour',
            minute: 'Minute'
        },
        reminder: {
            title: "It's high time to dream"
        },
        loadingText: 'Loading...'
    },
    constructor: function() {
        if (this.$className.substr(-2) === lang) {
            this.applyLocale();
        }
    },
    applyLocale: function() {
        console.log('en');


        //
        // Default Sencha wordings
        //
        Ext.Date.dayNames = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];

        Ext.Date.monthNames = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        Ext.Date.monthNumbers = {
            'Jan': 0,
            'Feb': 1,
            'Mar': 2,
            'Apr': 3,
            'May': 4,
            'Jun': 5,
            'Jul': 6,
            'Aug': 7,
            'Sep': 8,
            'Oct': 9,
            'Nov': 10,
            'Dec': 11
        };

        Ext.Date.getShortDayName = function(day) {
            return Ext.Date.dayNames[day].substring(0, 3);
        };

        Ext.Date.getMonthNumber = function(name) {
            return Ext.Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
        };

        Ext.Date.parseCodes.S.s = '(?:st|nd|rd|th)';

        Ext.Date.getSuffix = function(date) {
            switch (date.getDate()) {
                case 1:
                case 21:
                case 31:
                    return 'st';
                case 2:
                case 22:
                    return 'nd';
                case 3:
                case 23:
                    return 'rd';
                default:
                    return 'th';
            }
        };

        if (Ext.picker && Ext.picker.Picker) {
            obj = Ext.picker.Picker.prototype.config;
            obj.doneButton = {
                text: 'Done'
            };
            obj.cancelButton = {
                text: 'Cancel'
            };
        }

        if (Ext.picker && Ext.picker.Date) {
            obj = Ext.picker.Date.prototype.config;
            obj.doneButton = {
                text: 'Done'
            };
            obj.cancelButton = {
                text: 'Cancel'
            };
            Ext.apply(obj, {
                'dayText': 'Day',
                'monthText': 'Month',
                'yearText': 'Year',
                'hourText': 'Hour',
                'minuteText': 'Minute',
                'slotOrder': ['day', 'month', 'year']
            });
        }

        if (Ext.IndexBar) {
            obj = Ext.IndexBar.prototype.config;
            Ext.apply(obj, {
                letters: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
            });
        }

        if (Ext.LoadMask) {
            obj = Ext.LoadMask.prototype.config;
            Ext.apply(obj, {
                message: 'Loading...'
            });
        }

        if (Ext.dataview && Ext.dataview.DataView) {
            obj = Ext.dataview.DataView.prototype.config;
            Ext.apply(obj, {
                loadingText: 'Loading...'
            });
        }

        // if (Ext.NestedList) {
        //     obj = Ext.NestedList.prototype.config;
        //     Ext.apply(obj, {
        //         backText: 'Back',
        //         loadingText: 'Loading...',
        //         emptyText: 'No items available.'
        //     });
        // }

        if (Ext.plugin && Ext.plugin.PullRefresh) {
            obj = Ext.plugin.PullRefresh.prototype.config;
            Ext.apply(obj, {
                loadingText: 'Loading...'
            });
        }

        // if (Ext.plugin && Ext.plugin.ListPaging) {
        //     obj = Ext.plugin.ListPaging.prototype.config;
        //     Ext.apply(obj, {
        //         loadMoreText: 'Load More...',
        //         noMoreRecordsText: 'No More Records'
        //     });
        // }

        if (Ext.util.Format) {
            obj = Ext.util.Format;
            Ext.apply(obj, {
                defaultDateFormat: 'm/d/Y'
            });
        }

        if (Ext.MessageBox) {
            Ext.MessageBox.OK.text = 'OK';
            Ext.MessageBox.CANCEL.text = 'Cancel';
            Ext.MessageBox.YES.text = 'Yes';
            Ext.MessageBox.NO.text = 'No';

            Ext.MessageBox.OKCANCEL[0].text = 'Cancel';
            Ext.MessageBox.OKCANCEL[1].text = 'OK';

            Ext.MessageBox.YESNOCANCEL[0].text = 'Cancel';
            Ext.MessageBox.YESNOCANCEL[1].text = 'No';
            Ext.MessageBox.YESNOCANCEL[2].text = 'Yes';

            Ext.MessageBox.YESNO[0].text = 'No';
            Ext.MessageBox.YESNO[1].text = 'Yes';
        }
    }
});
