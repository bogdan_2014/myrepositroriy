/**
 * Utilities
 */
Ext.define('Drl.Utils', {

    config: {
    },

    humaneDate: function(date_str) {
        var time_formats = [
            [1, '1 second'],
            [60, 'seconds', 1],
            [90, '1 Minute'], // 60*1.5
            [3600, 'Minutes', 60], // 60*60, 60
            [5400, '1 Hour'], // 60*60*1.5
            [86400, 'Hours', 3600], // 60*60*24, 60*60
            [129600, '1 Day'], // 60*60*24*1.5
            [604800, 'Days', 86400], // 60*60*24*7, 60*60*24
            [907200, '1 Week'], // 60*60*24*7*1.5
            [2628000, 'Weeks', 604800], // 60*60*24*(365/12), 60*60*24*7
            [3942000, '1 Month'], // 60*60*24*(365/12)*1.5
            [31536000, 'Months', 2628000], // 60*60*24*365, 60*60*24*(365/12)
            [47304000, '1 Year'], // 60*60*24*365*1.5
            [3153600000, 'Years', 31536000], // 60*60*24*365*100, 60*60*24*365
            [4730400000, '1 Century'] // 60*60*24*365*100*1.5
        ];

        var time = ('' + date_str).replace(/-/g, "/").replace(/[TZ]/g, " "),
            dt = new Date,
            seconds = (date_str instanceof Date ? (dt - date_str) : (dt - new Date(time) + (dt.getTimezoneOffset() * 60000))) / 1000,
            token = ' Ago',
            i = 0,
            format;

        if (seconds < 0) {
            seconds = Math.abs(seconds);
            token = ' from now';
        }

        while (format = time_formats[i++]) {
            if (seconds < format[0]) {
                if (format.length == 2) {
                    return format[1] + token;
                } else {
                    return Math.round(seconds / format[2]) + ' ' + format[1] + token;
                }
            }
        }

        // overflow for centuries
        if (seconds > 4730400000)
            return Math.round(seconds / 4730400000) + ' Centuries' + token;

        return date_str;
    },

    shadeColor: function(color, percent) {
        var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, B = (num >> 8 & 0x00FF) + amt, G = (num & 0x0000FF) + amt;
        return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
    },

    constructor: function(config) {
        this.initConfig(config);
    }
}, function(Utils) {

    Ext.onSetup(function() {
        // create global instance to access utils
        Drl.utils = new Utils();
    });

});

