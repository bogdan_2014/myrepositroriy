Ext.define('Drl.field.PasswordNumber', {
    extend : 'Ext.field.Password',
    xtype  : 'pasnumfield',

    initialize: function() {
        var me = this,
            input = me.element.down('input');
        input.set({
            pattern : '[0-9]*'
        });
        input.dom.onkeypress = function(event) {
            if(me.getValue().length >=4) {
                return false;
            }
        };

        me.callParent(arguments);
    }
});
