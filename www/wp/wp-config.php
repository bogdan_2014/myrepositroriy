<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wordpress');

/** Имя пользователя MySQL */
define('DB_USER', 'Bogdan');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'TKdDvZEnpvvJvfAh');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|NKe >j6:~9LVCV+acYa~dj+7.kVRZ[(37r;i2L<o7}zL=+9AP+8ydt6K^][;Ked');
define('SECURE_AUTH_KEY',  '7:d5~KZ:oshj jtP0FhvO:i$v*Vyo25kqNDuH,Ns2KcoH-W&eyggCexg&xqMKugE');
define('LOGGED_IN_KEY',    '$[P%2wska6R+KP/hls[forA)rf;I{d^F3AD`c7lkubl]^*bG]-sg<e,r^_SEr?Mz');
define('NONCE_KEY',        '[=en;|@u4ko5w9;qlL!NGu)y)sD%Imx#**2`s|iuMArwque_UMwo29hueOV$Jk(b');
define('AUTH_SALT',        ' hm]<*];/m2zOI?tK[IKmo`pRc?2I;AYIK6Y@1#RMYO)AqAKq<zZ#Mz),^/fEC[t');
define('SECURE_AUTH_SALT', 'J!`z@eu8cHlZ{]B[ShKRvi!PegRguVa#9-@EHe[$%zRL2I*[<+As`J+9>|r |qGr');
define('LOGGED_IN_SALT',   'n4U##gd*cZ_g|nNW[r16!F]74#?<g%0R$aN0GssQ.W_u P|y2xH4~w?9XvLhKiLw');
define('NONCE_SALT',       '-%I3Ziv)b&PbAH{OmJ#t@ES-?.Vu_9|Knh5JuU|6iBE#2i,6]]Qlh_gFa9HqEuax');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Язык локализации WordPress, по умолчанию английский.
 *
 * Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
 * для выбранного языка должен быть установлен в wp-content/languages. Например,
 * чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
 * и присвойте WPLANG значение 'ru_RU'.
 */
define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
