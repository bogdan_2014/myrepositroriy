# Native Packaging for Mobile Devices

This guide describes how to package a Sencha Touch app using
[Sencha Cmd](http://www.sencha.com/products/sencha-cmd/) to run 
natively on mobile devices using the Sencha Touch Native Packager tool. 
This guide describes packaging and publishing only for iOS and Android. 


**Important** Touch 2.3 and later provides support for creating
Cordova and PhoneGap apps. If you use Cordova or PhoneGap, refer
to [Cordova and PhoneGap Apps](#!/guide/cordova)
for information on packaging and emulating. These two products provide an extensive API,
the ability of Cordova to build for multiple platforms simultaneously, and PhoneGap's 
remote building capability. In addition, Cordova/PhoneGap lets you develop for Android, iOS,
BlackBerry, and Windows Phone.

## Prerequisites

The following guides are recommended reading before proceeding further:

  - [Introduction to Sencha Cmd](#!/guide/command).
  - [Using Sencha Cmd](#!/guide/command_app).

## Native App Packaging General Procedures

The app packaging process is similar whether you target iOS or Android devices.
Each platform differs by how you prepare and the configuration file you create
with platform-specific parameters.

Basic steps for app packaging:

<ol>
<li><a href="#provis">Provisioning</a>:
<ol>
<li><b>iOS:</b>
<ol>
<li>Complete iOS provisioning on the 
<a href="https://developer.apple.com/account/ios/profile/profileLanding.action">Apple iOS 
provisioning profiles portal</a> (requires an Apple ID, password, and a purchased developer
license). Use this site to obtain a certificate, identify devices, and get an App ID.</li>
<li>Download and install the free 
<a href="https://itunes.apple.com/us/app/xcode/id497799835">Xcode</a>
software. You can use the Xcode simulator to debug your iOS app before installing 
your app on a device. Xcode only works on a Mac with the Lion, Mountain Lion, or 
Mavericks OS X versions.</li>
</ol></li>
<li><b>Android:</b> - Obtain an Android ready 
certificate (debug or release) to sign your application. You need the 
<a href="http://developer.android.com/sdk/index.html">Android SDK Manager</a>, 
which provides a simulator and tools for connecting
a mobile device to your computer for downloading apps to the device.</li>
<li><b>BlackBerry:</b> - Use the 
<a href="https://build.phonegap.com">Adobe PhoneGap Build</a> 
site to package an app for use on the device. 
For Sencha Touch 2.3 and later, refer to 
[Cordova and PhoneGap Apps](#!/guide/cordova) and
[BlackBerry 10 Support](#!/guide/blackberry).</li>
<li><b>Windows Phone and Surface:</b> - Use the 
<a href="https://build.phonegap.com">Adobe PhoneGap Build</a> 
site to package an app for use on the device.</li>
</ol></li>
<li><a href="#cfg">Create config file</a> - Create a packaging configuration file 
for use with Sencha Cmd.</li>
<li><a #pkg">href="Package your app</a> - Run Sencha Cmd to create a 
packaged <tt>&lt;application&gt;.app</tt> file for iOS or an `.apk` file for Android.</li>
</ol>

**Note** The remainder of this guide only applies to packaging an iOS or Android app. 
If you are developing an app for BlackBerry, Windows Phone, or Surface, continue now to  
[Cordova and PhoneGap Apps](#!/guide/cordova) to package your app using Apache Cordova.


<a name="provis"></a>
## Step 1: Provisioning

Provisioning differs by platform:

**iOS:** Refer to the [Native iOS Provisioning](#!/guide/native_provisioning) 
and use the <a href="https://developer.apple.com/account/ios/profile/profileLanding.action">Apple iOS 
provisioning profiles portal</a> (requires an Apple ID, password, and a purchased developer
license) to get a development or distribution certificate and profiles. 
Create an App ID and provision your application. You need your App ID and App Name to package your app. 
Refer to the How-To section in the 
[Apple iOS Member Center](https://developer.apple.com/membercenter/index.action) for help.

**Android:** Use the Android SDK Keytool to create a certificate to 
sign your Android application. The following example Keytool command 
generates a private key:

<pre>
$ keytool -genkey -v -keystore my-release-key.keystore -alias alias_name
    -keyalg RSA -keysize 2048 -validity 10000
</pre>

For more information, see the Android 
[Signing Your Applications](http://developer.android.com/tools/publishing/app-signing.html).

<a name="cfg"></a>
## Step 2: Create a packaging configuration file

Create a configuration file template by running the following command at
the command line or Terminal:

    sencha app package generate <configTemplate.json>

<tt>&lt;configTemplate.json&gt;</tt> is the name of the configuration file. 
The file name cannot contain spaces.

The template contains the following elements: 

<table style="width: 80%" border="1">
<tr><th>Element</th><th>Platform</th><th>Requires Value?</th></tr>
<tr><td><a href="#AppName">applicationName</a></td><td>iOS and Android</td><td>Yes</td></tr>
<tr><td><a href="#AppID">applicationId</a></td><td>iOS and Android</td><td>No</td></tr>
<tr><td><a href="#bseed">bundleSeedId</a></td><td>iOS only</td><td>No</td></tr>
<tr><td><a href="#vstr">versionString</a></td><td>iOS and Android</td><td>Yes</td></tr>
<tr><td><a href="#vcode">versionCode</a></td><td>iOS and Android (also known as the build number)</td><td>Yes</td></tr>
<tr><td><a href="#icon">icon</a></td><td>iOS and Android</td><td>No</td></tr>
<tr><td><a href="#ipath">inputPath</a></td><td>all platforms (used by packager)</td><td>Yes</td></tr>
<tr><td><a href="#opath">outputPath</a></td><td>all platforms (used by packager)</td><td>Yes</td></tr>
<tr><td><a href="#cfgel">configuration</a></td><td>iOS and Android</td><td>Yes</td></tr>
<tr><td><a href="#plat">platform</a></td><td>iOS and Android</td><td>Yes</td></tr>
<tr><td><a href="#dtype">deviceType</a></td><td>iOS only</td><td>Yes</td></tr>
<tr><td><a href="#calias">certificateAlias</a></td><td>iOS (if packaging under Mac OS) only</td><td>No</td></tr>
<tr><td><a href="#cpath">certificatePath</a></td><td>iOS (if packaging under Windows OS) and Android</td><td>No</td></tr>
<tr><td><a href="#cpw">certificatePassword</a></td><td>iOS (if packaging under Windows OS) and Android</td><td>No</td></tr>
<tr><td><a href="#propo">provisionProfile</a></td><td>iOS only</td><td>No</td></tr>
<tr><td><a href="#us">URLScheme</a></td><td>iOS and Android</td><td>No</td></tr>
<tr><td><a href="#noco">notificationConfiguration</a></td><td>iOS only</td><td>No</td></tr>
<tr><td><a href="#spath">sdkPath</a></td><td>Android only</td><td>No</td></tr>
<tr><td><a href="#anlev">androidAPILevel</a></td><td>Android only (min 8, max 18)</td><td>No</td></tr>
<tr><td><a href="#perms">permissions</a></td><td>Android only</td><td>No</td></tr>
<tr><td><a href="#ors">orientations</a></td><td>iOS and Android</td><td>Yes</td></tr>
</table>

<a name="AppName"></a>
### `applicationName` (Required)

The name of your application, which a device displays to the user when the app installs. 

**iOS:** The application name needs to match the name provided in the 
[iOS Provisioning Portal](https://developer.apple.com/account/ios/profile/profileLanding.action) 
(requires an Apple ID and password) in the **Identifiers** > **iOS App IDs** 
section of the iOS developer portal.

This example iOS app ID shows both the name and the ID:

{@img idScreen.png App ID}

This example uses:

  - AppName: Sencha Touch 2 Packaging
  - AppID: com.Sencha.Touch2Package

**Note** The App ID is the same as the one you put in the Identifier field in Xcode.

**Android:** The output file has the name &lt;applicationName&gt;.apk.

<a name="AppID"></a>
### `applicationId` (Optional)
  
The ID for your app. Use a name space for your app such as `com.sencha.TouchPackage`, 
as shown in the [`applicationName`](#AppName) example. 
For iOS, the ID can also be found in **Identifiers** > **iOS App IDs** section of the provisioning portal.

<a name="bseed"></a>
### `bundleSeedId` (iOS only)
  
The ten-character string in front of the iOS application ID obtained from the 
[iOS Provisioning Portal](https://developer.apple.com/account/ios/profile/profileLanding.action) 
(requires an Apple ID and password). In the previous example 
for [`applicationName`](#AppName), the example `bundleSeedId` would be `H8A8ADYR7H`.

<a name="vstr"></a>
### `versionString` (Required)
  
Indicates the version number of your application. 
This is a string and can have a value such as `1.0-beta`.

<a name="vcode"></a>
### `versionCode` (Android only - Required for Android)

Indicates the build number of an Android app, also called the integer version code.

<a name="icon"></a>
### `icon` (Optional)

Indicates the icon that displays to a user along with your app name on the device's home screen.

**iOS:** 

 - Specify the icon file to be used for your application. 
 - Specify a Retina icon with `@2x` at the end of the icon name. 
 - A regular icon name looks like `icon.png`, while a Retina icon looks 
like `(regular) anicon@2x.png`. If a Retina icon with the `@2x.png` exists, 
the packager includes the Retina icon. 
- Refer to the 
Apple documentation about icon sizes at [Custom Icon and Image Creation Guidelines](https://developer.apple.com/library/ios/#documentation/userexperience/conceptual/mobilehig/IconsImages/IconsImages.html).
 - iOS uses 57, 72, 114, and 144 pixel icons. 
- Specify a target device for your app:

<pre>
"icon": {
    "57": "resources/icons/Icon.png",
    "72": "resources/icons/Icon~ipad.png",
    "114": "resources/icons/Icon@2x.png",
    "144": "resources/icons/Icon~ipad@2x.png"
}
</pre>

**Android:** 

 - Refer to the [Google Iconography guide](http://developer.android.com/design/style/iconography.html).
 - Android uses 48, 72, and 96 pixel icons. 
 - If you package for Android, you can omit iOS icons and vice versa.
 - Specify a target device for your app, Substitute the correct path
   for the resources/icon variable in the path. Icons reside in either
   the res/drawable or res/drawable-hdpi depending on whether the 
   device is hign density or not. 

<pre>
"icon": {
    "48":"resources/icons/Icon_Android48.png",
    "72":"resources/icons/Icon_Android72.png",
    "96":"resources/icons/Icon_Android96.png"
}
</pre>

Refer to Apple's  
[Custom Icon and Image Creation Guidelines](https://developer.apple.com/library/ios/#documentation/userexperience/conceptual/mobilehig/IconsImages/IconsImages.html) 
table for information about icon sizes. 
See the statement in the table for "App icon (required for all apps)" 
for the icon sizes for each iOS device.

**Android:** Specifies the launcher icon file to be used for your application. Refer to the 
[Google Iconography guide](http://developer.android.com/design/style/iconography.html) 
for more information.

<a name="ipath"></a>
### `inputPath` (Required)

Indicates the location of your Sencha Touch application, relative to the configuration file.

<a name="opath"></a>
### `outputPath` (Required)

Indicates the output location of the packaged application, where the built
application file is saved.

<a name="cfgel"></a>
### `configuration` (Required)

Indicates whether you are building the debug or release configuration of your
application. Use `Debug` unless you are submitting your app to an online store, 
in which case, use `Release` to submit to an online store.

<a name="plat"></a>
### `platform` (Required)

Indicates the platform on which your application runs.

- **iOS:** Options are `iOSSimulator` or `iOS`.
- **Android:** Options are `Android` or `AndroidEmulator`.

<a name="dtype"></a>
### `deviceType` (iOS only - Required for iOS)

Indicates the iOS device type on which your application runs. 

Available options are:

 - iPhone
 - iPad
 - Universal

<a name="cpath"></a>
### `certificatePath` (Optional)

Indicates the location of your certificate, which is required when you are 
developing for Android or Windows.

<a name="calias"></a>
### `certificateAlias` (Optional)

Indicates the name of your certificate. If this is not specified when developing on
Mac OS X, the packaging tool automatically tries to find the certificate using the
[applicationId](#AppID). 

Can be just a simple matcher. For example, if your certificate name is 
"iPhone Developer: Polly Hedra (ABCDEFGHIJ)", you can just enter `iPhone Developer`.

Not required when using a `certificatePath` on Windows.

<a name="cpw"></a>
### `certificatePassword` (Optional)

Use only if a password was specified when generating certificate for 
release build of Android (iOS or Windows), or any iOS build on Windows. 
Indicates that a password is set for the certificate. 
If a password is not set, leave blank, or delete this parameter.

<a name="propo"></a>
### `provisionProfile` (Optional)

Indicates a string for the path to the provision profile 
(APP_NAME.mobileprovision), which you can create 
and then download from Apple's provisioning portal.

<a name="us"></a>
### `URLScheme` (Optional)

Indicates a string for the URL scheme for communication with your application. 
Can be empty if you don't use a custom URL scheme in your project.

<a name="noco"></a>
### `notificationConfiguration` (iOS only - Optional)

Optional for apps that use push notifications. Use `Debug` unless you 
are submitting your app to an online store, in which case use `Release`. 
If your app doesn't use push notifications, leave blank or remove this parameter.

<a name="spath"></a>
### `sdkPath` (Android only - Optional)

Indicates the path to the Android SDK (string).

<a name="anlev"></a>
### `androidAPILevel` (Android only - Optional)

Indicates the Android API level, which is the version of the Android SDK 
to use. For more information, see 
[What is API Level?](http://developer.android.com/guide/appendix/api-levels.html) 
in the Android SDK documentation. Be sure to install the corresponding platform API 
in the Android SDK manager (*android_sdk/tools/android*). 

**Note** This parameter is optional, but the default is set to API level 8 (SDK 2.2 - Froyo). 

Android SDK Versions:

- API level 8 = Android SDK version 2.2 - Froyo
- API level 9 = Android SDK version 2.3 - 2.3.2 - Gingerbread
- API level 10 = Android SDK version 2.3.3 - 2.3.7 - Gingerbread
- API level 11 = Android SDK version 3.0 - Honeycomb
- API level 12 = Android SDK version 3.1 - Honeycomb
- API level 13 = Android SDK version 3.2 - Honeycomb
- API level 14 = Android SDK version 4.0 - 4.0.2 -which is Android SDK 2.2  Ice Cream Sandwich
- API level 15 = Android SDK version 4.0.3 - 4.0.4 - Ice Cream Sandwich
- API level 16 = Android SDK version 4.1 - Jelly Bean
- API level 17 = Android SDK version 4.2 - Jelly Bean
- API level 18 = Android SDK version 4.3 - Jelly Bean

<a name="perms"></a>
### `permissions` (Android only - Optional)

Array of permissions to use with services called from an Android app, 
including coarse location, fine location, information about networks, 
the camera, and so on. See the complete list of permissions in the Android
[Manifest.permission](http://developer.android.com/reference/android/Manifest.permission.html).

Default values are:

    "INTERNET",
    "ACCESS_NETWORK_STATE",
    "CAMERA",
    "VIBRATE",
    "ACCESS_FINE_LOCATION",
    "ACCESS_COARSE_LOCATION",
    "CALL_PHONE"

<a name="ors"></a>
### `orientations`

Indicates the device orientations in which the application can run. 

Options are (all are enabled by default):

- portrait
- landscapeLeft
- landscapeRight
- portraitUpsideDown

**Note** If omitted, the default orientations setting is all four orientations.

<a name="pkg"></a>
## Step 3: Run the packager to create the packaged application

After creating the config file, package the app using these procedures 
for packaging debug and release versions of an app for iOS or Android.

### iOS: Package a Debug Application 

The appropriate `platform` and `configuration` settings need to be made 
in the config file, for example:

    platform: iOSSimulator
    configuration: Debug

If `platform` and `configuration` are not set, the packaged app will 
not run correctly.

With these configs set properly, issue the following command in Terminal:

    sencha app package run <configFile.json>

In this example, the iOS Simulator in the `platform` config parameter
(in the JSON config file) upon successful completion of the `package` command, 
launches the iOS simulator with the application running natively. 
See [Step 2](#cfg) for a complete list of variables
you can specify in the JSON config file.

**Note** Set the `deviceType` identifier in the JSON config file 
to `iPhone` or `iPad` to trigger the appropriate simulator.

### iOS: Package a Release Application 

To package a signed application to run on the device, issue this 
command in the terminal:

    sencha app package build <configFile.json>

**Note** This command creates the <tt>&lt;AppName.app&gt;</tt> in the directory 
indicated by the outputPath variable in the *configFile.json*. 
See [Step 2](#cfg) for a complete list of variables you can specify in 
the JSON config file. Deploy this application to the iOS device.

### Android: Package a Debug App and Run it on the Android Emulator

The appropriate `platform` and `configuration` settings need to be made in the config file, for example:

    platform: AndroidEmulator
    configuration: Debug

If `platform` and `configuration` are not set, the packaged app won't run correctly.

With these configs set properly, start the Android Emulator and issue this command:
    
    sencha app package run <configFile.json>

In this example, which targets the Android Emulator in the `platform` 
config parameter, successful completion of the `package` command launches 
the app in the already running emulator. 

If `package` is successful, an `.apk` is available in the application 
outputPath location for you to test on an Android Emulator or a device.

More information about the Android Emulator can be found in 
[Android Developer Guide: Using the Android Emulator](http://developer.android.com/tools/devices/emulator.html).

### Android: Package an application for distribution

To package a signed application to run on the device, issue the following command:

    sencha app package build <configFile.json>

This command creates the <tt>&lt;AppName.apk&gt;</tt> application file that 
you can use to release for distribution. The APK file is stored in the 
directory indicated by the outputPath variable 
in the configFile.json file. See [Step 2](#cfg) for a complete list of variables
you can specify in the JSON config file.

You can also use this command to create your APK file:

    sencha app build native

## Additional Resources

### iOS Resources

  1. [Native iOS Provisioning](#!/guide/native_provisioning)
  2. <a href="https://developer.apple.com/account/ios/profile/profileLanding.action">Apple iOS 
provisioning profiles portal</a> (requires an Apple ID, password, and a purchased developer
license)

### Android Resources

  1. [Signing Your Applications](http://developer.android.com/tools/publishing/app-signing.html)
  2. [Installing the ADT Plugin for Eclipse](http://developer.android.com/tools/sdk/eclipse-adt.html)
  3. [Eclipse](http://www.eclipse.org/)
  4. [Managing Virtual Devices for Android Emulator](http://developer.android.com/tools/publishing/app-signing.html), [Setting up Virtual Devices](http://developer.android.com/tools/devices/index.html).
 
### BlackBerry Resources

 - [BlackBerry 10 Support](#!/guide/blackberry)

### Windows Phone and Surface Resources

 - [Adobe PhoneGap Build](https://build.phonegap.com)
  
