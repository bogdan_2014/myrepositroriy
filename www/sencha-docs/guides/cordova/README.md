# Cordova and PhoneGap Apps

<a href="http://cordova.apache.org/">Apache Cordova</a> provides APIs and packaging
tools for creating an app for an Android, iOS, BlackBerry, or Windows Phone device. 
Touch 2.3 introduces tools and an API for working with Cordova and PhoneGap. 

PhoneGap is built on top of Cordova and both have access to the Cordova API.
PhoneGap and Cordova differ by how their packaging tools are implemented. 
PhoneGap provides a remote building interface at 
<a href="https://build.phonegap.com/">Adobe PhoneGap Build</a>
that lets you package and emulate an app for a single platform in the cloud. 
Cordova packaging tools let you build simultaneously for 
multiple platforms on your computer. In addition, Cordova is constantly updated
by the open source community, whereas PhoneGap updates are coordinated by Adobe.

While Cordova and PhoneGap make it easy to package an app, you still need a 
certificate and AppID for each platform for the market in which you want to distribute
your app, such as for Google Play or the Apple App Store.

Sencha Cmd version 4.0 and later provides access to Cordova and PhoneGap. 
You can easily enable or disable access, package an app, and run an 
emulator or simulator. You can use the Sencha Cmd Cordova and PhoneGap 
commands to package and emulate a Touch app.

See the Apache Cordova <a href="http://cordova.apache.org/docs/en/3.0.0/guide_platforms_index.md.html#Platform%20Guides">Platform Guides</a> for information.

<b>Important</b>
<ul>
<li>While PhoneGap builds BlackBerry version&nbsp;10 locally, the 
BlackBerry version at the Adobe PhoneGap Build site is version&nbsp;6.</li>
<li>If you're used to packaging your app using the Sencha Touch procedure of 
configuring the packager.json file, the packager.json file is not used in 
Cordova and PhoneGap packaging. Instead, you specify the App ID when you
start Cordova or PhoneGap, and then configure the 
<a href="https://build.phonegap.com/docs/config-xml">config.xml</a> file
using information from the PhoneGap site. <br>See also: 
<a href="https://github.com/phonegap/phonegap-start/blob/master/www/config.xml">PhoneGap example config.xml file</a> 

</ul>

## Before Creating an App

Before developing an app:

<ol>
<li><a href="http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html">Install Java JRE</a>.</li>
<li><a href="http://nodejs.org/download/">Install Node.js</a></li>
<li>If you plan to package and emulate with Cordova, after Node.js is installed, use this command to install Cordova:<br>
<pre>npm install -g cordova</pre>
<b>Important</b> Whether or not you install Cordova to package and emulate your app, the
Cordova API is available to your apps. The Cordova packaging and emulating package does 
not affect the use of the Cordova APIs in a Cordova or PhoneGap app.</li>
<li>If you plan to package and emulate with PhoneGap, after Node.js is installed, use this command to install PhoneGap:<br>
<pre>npm install -g phonegap</pre>
<b>Notes</b><br>
&bull; You must register with the free 
<a href="https://build.phonegap.com/">Adobe PhoneGap Build</a> site
and obtain a username and password to access the PhoneGap remote building site.<br>
&bull; Sencha recommends that you install both Cordova and PhoneGap--both are
free (as is access to the Adobe PhoneGap Build site). Both software packages provide
features that help your app development.</li>
<li><a href="http://www.sencha.com/products/sencha-cmd/download">Install Sencha Cmd version 4.0 or later</a>.</li>
<li>Create your app's file structure with:<br>
<pre>
<b>sencha -sdk</b> <i>/path/to/SenchaTouch</i> <b>generate app</b> <i>MyApp /path/to/MyApp</i><br></pre>
For more information, see the 
<a href="http://docs.sencha.com/touch/#!/guide/getting_started">Getting Started</a>
guide.</li>
<li>If you are developing for Android, download the
<a href="http://developer.android.com/sdk/index.html">Android SDK Manager</a>.</li>
<li>If you are developing for iOS, complete iOS provisioning on the 
<a href="https://developer.apple.com/account/ios/profile/profileLanding.action">Apple 
iOS provisioning profiles portal</a> (requires an Apple ID, password, and a 
purchased developer license). Use this site to obtain a certificate, 
identify devices, and get an AppID. In addition, download and install the free 
<a href="https://itunes.apple.com/us/app/xcode/id497799835">Xcode</a>
software. You can use the Xcode simulator to debug your iOS app before installing 
your app on a device. Xcode only works on a Mac with the Lion, Mountain Lion, or 
Mavericks OS X versions.</li>
<li>If you are developing for BlackBerry, review the 
<a href="http://developer.blackberry.com/native/">BlackBerry Native SDK</a> and 
register to sign your apps with the
<a href="https://www.blackberry.com/SignedKeys/codesigning.html">BlackBerry Keys Order Form</a>.
<li>Create a <a href="https://build.phonegap.com/docs/config-xml">config.xml</a> using the information 
provided by PhoneGap. This file resides at the root level of your project where your index.html file resides.
If you are porting from <code>packager.json</code> to Cordova, the <code>config.xml</code> file is not similar, however,
after you register your app with the Apple Developer Program, and for Android in the AndroidManifest.xml file, 
most of the parameters in <code>packager.json</code> will be provided for. In addition, the default settings in 
<code>config.xml</code> handle most app conditions, so there will be fewer items to change.
<br>See also: 
<br><a href="https://github.com/phonegap/phonegap-start/blob/master/www/config.xml">PhoneGap example config.xml file</a>
<br><a href="http://docs.phonegap.com/en/3.0.0/config_ref_index.md.html#Configuration%20Reference">Configuration Reference</a>
</li>
</ol>

## Developing a Cordova or PhoneGap App

To develop an app:

<ol>
<li>Change directory to your project directory.</li>
<li>Enable support for Cordova - If for iOS, see 
<a href="#iOSNote">iOS Note</a> below:<br>
<pre>sencha cordova init [AppID]</pre>
And/or enable support for PhoneGap:<br>
<pre>sencha phonegap init [AppID]</pre>
<p>If you need to remove support for Cordova or PhoneGap, see the 
<a href="#RemoveNote">Remove Note</a> below.
</li>
<li>If using Cordova, configure the <code>cordova.local.properties</code> file 
to indicate which 
platforms you're building for. This file contains a single statement and 
lists the build targets separated by a space - this example lists
all the target platforms:<br>
<pre>cordova.platforms=ios android blackberry wp8</pre></li>
<li>If using PhoneGap, configure the <code>phonegap.local.properties</code> file 
to indicate which 
platforms you're building for. This file contains information you 
need to pass to the Adobe PhoneGap Build site to create your 
app - PhoneGap only lets you 
specify one platform at a time:<br>
<pre>
phonegap.platform=ios
phonegap.build.remote=true
phonegap.build.remote.username=&lt;Your_PhoneGap_Username&gt;
phonegap.build.remote.password=&lt;Your_PhoneGap_Password&gt;
</pre>
<b>Note</b> If you set <code>phonegap.build.remote=false</code>, PhoneGap builds
locally on your computer.</li>
<li>Use the <a href="http://cordova.apache.org/docs/en/3.0.0/">Cordova API</a> 
to develop your app. Touch 2.3 provides
an extensive set of device APIs in the <code>Ext.device.&lt;API&gt;.Cordova</code> 
and <code>Ext.device.&lt;API&gt;.PhoneGap</code> namespaces. 
Alternatively, you can access the Cordova functions directly in your app by preceding the 
API with <code>navigator</code>, for example for the 
<a href="http://docs.phonegap.com/en/3.0.0/cordova_compass_compass.md.html#Compass">Compass</a> API, use:<br>
<code>navigator.compass.getCurrentHeading(compassSuccess, compassError, compassOptions);</code>
</li>
<li>Create your app using Sencha Touch or Sencha Architect.</li>
<li>Emulate your app with: <br>
<pre>sencha app build -run native</pre></li>
<li>Package your app with:<br>
<pre>sencha app build native</pre></li>
</ol>

<a name="iOSNote"></a>
<b>iOS Note</b><br>

If your app runs on iOS, supply a unique AppID that you get from the 
<a href="https://developer.apple.com/account/ios/identifiers/bundle/bundleLanding.action">Apple developer site</a>. 
Access to the Apple developer site requires that you specify your Apple Developer username and password.  

<a name="RemoveNote"></a>
<b>Remove Note</b><br>

You can remove support for Cordova or PhoneGap with:<br>

<pre>sencha cordova remove</pre>

Or for PhoneGap with:<br>

<pre>sencha phonegap remove</pre>

The remove command also deletes these files in your project directory:
<ul>
<li>If Cordova, the <code>cordova.local.properties</code> file.</li>
<li>If PhoneGap, the <code>phonegap.local.properties</code> file.</li>
<li>The <a href="https://build.phonegap.com/docs/config-xml">config.xml</a> file.</li>
</ul>

## Sencha Cmd Ant Commands

Starting with Sencha Cmd version 4.0, the Cordova/PhoneGap implementation 
provides extra functionality that you can access using the Ant component 
in Sencha Cmd.

<table style="width: 90%" border="1">
<tr><th style="width: 40%">Command</th><th style="width: 60%">Description</th></tr>
<tr><td>sencha ant cordova-build</td>
<td>Builds all platforms without building the Sencha app.</td></tr>
<tr><td>sencha ant cordova-clean</td>
<td>Deletes all files in the general <code>{project}/cordova/www</code> folder.</td></tr>
<tr><td>sencha ant cordova-emulate</td>
<td>Emulates all platforms without building the Sencha app.</td></tr>
<tr><td>sencha ant cordova-prepare</td>
<td>Prepares all platforms without building the Sencha app.</td></tr>
<tr><td>sencha ant cordova-sencha-build</td>
<td>Builds into the Cordova general <code>{project}/cordova/www</code> folder. 
When you specify the <code>sencha cordova init</code> command,
Cordova creates this folder for you. <b>Important</b> This folder is overwritten on each
build. Never put files of your own in the <code>www</code> folder.</td></tr>
<tr><td>sencha ant cordova-sencha-emulate</td>
<td>Builds then emulates in all platforms.</td></tr>
<tr><td>sencha ant cordova-sencha-prepare</td>
<td>Builds then Emulates in all platforms.</td></tr>
<tr><td>sencha ant phonegap-build</td>
<td>Builds then Emulates in all platforms.</td></tr>
<tr><td>sencha ant phonegap-clean</td>
<td>Deletes all the files in the <code>{project}/phonegap/www</code> folder.</td></tr>
<tr><td>sencha ant phonegap-run</td>
<td>Runs the native app without building the Sencha app.</td></tr>
<tr><td>sencha ant phonegap-sencha-build</td>
<td>Builds the Sencha app and the PhoneGap native platform.</td></tr>
<tr><td>sencha ant phonegap-sencha-run</td>
<td>Builds the Sencha app and the PhoneGap native app then runs the native app.</td></tr>
</table>

## More Information

<ul>
<li><a href="http://cordova.apache.org/">Apache Cordova</a></li>
<li><a href="http://cordova.apache.org/docs/en/3.0.0/">Cordova Documentation</a></li>
<li><a href="http://docs.phonegap.com/en/3.0.0/index.html">PhoneGap Documentation</a></li>
<li><a href="https://build.phonegap.com/docs/config-xml">config.xml Information</a></li>
<li><a href="https://build.phonegap.com/">Adobe PhoneGap Build</a></li>
<li><a href="http://docs.sencha.com">Sencha Docs</a></li>
<li><a href="http://docs.sencha.com/cmd/#!/guide/">Sencha Cmd Docs</a></li>
<li><a href="http://docs.sencha.com/touch/#!/api">Sencha Touch API</a></li>
<li><a href="http://cordova.apache.org/docs/en/edge/guide_cli_index.md.html#The%20Command-line%20Interface">Cordova command line interface and the local.properties file</a>.</li>
</ul>
