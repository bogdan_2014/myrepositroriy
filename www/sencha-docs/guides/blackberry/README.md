# Developing BlackBerry 10 Apps 

Sencha Touch makes it easy to develop applications designed for 
the BlackBerry 10 OS. 

This guide explains how to create BlackBerry 10 applications using Sencha Touch.

Sencha Touch now supports the BlackBerry 10 theme with improved optimization 
and performance. In addition, in Sencha Touch 2.3, new support is provided for
BlackBerry 10 icons, which you can use to ensure your application has the  
BlackBerry look and feel. For more information, see 
[BlackBerry 10 Icons](#BB10Icons) in this guide.

If you are new to BlackBerry 10 development, see the 
[BlackBerry Getting Started](http://developer.blackberry.com/html5/documentation/getting_started_with_webworks.html) 
guide.

## Action Bar

An action bar is an area along the bottom of the screen to display actions 
and tabs that apply to the screen. Inside the action bar are tabs, which 
are just buttons in Sencha Touch. If you have too many tabs to display, 
you need a tab menu (which has the special docked left button on the left). 
Add the tab menu button component and dock it to the left.

After creating the tab menu and other items that go into the action bar, 
refer to [Adding Menus to a Toolbar](#AddMenus) to see how to add other 
components to your action bar. 

Touch provides the {@link Ext.ux.TabMenuButton} component, which lets 
you create an action bar tab menu. 
	
    @example preview portrait theme bb10
    // Tab menu button
    var tabMenuButton = Ext.create('Ext.ux.TabMenuButton', {
        text: 'All',
        docked: 'left',
        iconCls: 'view_grid',
        menuItems: [{
            text: 'All',
            iconCls: 'view_grid'
        }, {
            text: 'Favorites',
            iconCls: 'done'
        }, {
            text: 'Messenger',
            iconCls: 'bbm'
        }]
    });
    // Add it to the action bar
    Ext.Viewport.add({
        xtype: 'toolbar',
        docked: 'bottom',
        items: [tabMenuButton]
    });

## Action Menus

Action menus are a vertical list of actions with captions that slide in 
from the sides and contain actions used less frequently than those 
in the action bar. Users open an action menu by tapping an action 
button on the right side of the action bar.

    @example preview portrait theme bb10
    // Action menu
    var actionMenuButton = Ext.create('Ext.ux.ActionOverFlowMenuButton', {
        docked: 'right',
        iconCls: 'overflow_tab',
        menuItems: [{
            text: 'All',
            iconCls: 'view_grid'
        }, {
            text: 'Favorites',
            iconCls: 'done'
        }, {
            text: 'Messenger',
            iconCls: 'bbm'
        }]
    });
    // Add it to the action bar
    Ext.Viewport.add({
        xtype: 'toolbar',
        docked: 'bottom',
        items: [actionMenuButton]
    });

## Context Menus

Context menus are a vertical list of icons without captions that slide in from 
the sides and provide actions in context with an app. Context menus replace 
pop-up or graphical context menus. A suitable place for Context menus to appear is when you {@link Ext.List#itemtaphold} on an {@link Ext.List}.

    @example preview portrait theme bb10
    Ext.Viewport.add({
        xtype: 'list',
        data: [{
            text: 'Tap and hold on this item!'
        }],
        listeners: {
            itemtaphold: function() {
                Ext.Viewport.setMenu(Ext.create('Ext.ux.ContextMenu', {
                    width: 55,
                    items: [{
                        iconCls: 'copy'
                    }, {
                        iconCls: 'select_more'
                    }]
                }), {
                    side: 'right'
                });
                Ext.Viewport.showMenu('right');
            }
        }
    });


## Application Menus

Application menus provide important actions independent of context, for example, 
"Settings", "Log Out", and "About". Users open application menus by swiping down
from the top of the screen, and dismiss the menus by tapping outside the menu.

    @example preview portrait theme bb10
    // Application menu
    var applicationMenu = Ext.create('Ext.ux.ApplicationMenu', {
        items: [{
            text: 'Settings',
            iconCls: 'settings'
        }]
    });
    Ext.Viewport.setMenu(applicationMenu, {
        side: 'top'
    });

    Ext.Viewport.setHtml('Swipe down form the top of the screen to show the application menu.');

<a name="AddMenus"></a>
## Adding Menus to a Toolbar (Action Bar)

The following example shows how to add a menu to an action bar:

    @example preview portrait theme bb10
    // Tab menu button
    var tabMenuButton = Ext.create('Ext.ux.TabMenuButton', {
        text: 'All',
        docked: 'left',
        iconCls: 'view_grid',
        menuItems: [{
            text: 'All',
            iconCls: 'view_grid'
        }, {
            text: 'Favorites',
            iconCls: 'done'
        }, {
            text: 'Messenger',
            iconCls: 'bbm'
        }]
    });
    // Add it to the action bar
    Ext.Viewport.add({
        xtype: 'toolbar',
        docked: 'bottom',
        items: [tabMenuButton]
    });

<a name="BB10Icons"></a>
## BlackBerry 10 Icons

Sencha Touch 2.3 includes over 50 BlackBerry 10 icons, which you can use in your 
applications to give the BlackBerry 10 native look and feel to it.  You can find the icons in
the Sencha Touch  `/resources/themes/images/bb10/icons` directory
after you download and unzip the Sencha Touch software installation.
Compile these icons into your `app.scss` CSS before using them in your application. 
The SCSS file resides in the `resources/sass` directory in the Sencha Touch installation
directory. Add the images that you want to use to the `app.scss` file,  
and use Compass to compile the file and generate the `app.css` file. 

Compass is provided for 
free with [Sencha Cmd](http://www.sencha.com/products/sencha-cmd/download).
 
You can use the image in your application with this config statement: 

    iconCls: 'overflow_tab'

## Adding the BlackBerry Theme to app.json

The following example shows how to specify the BlackBerry theme in your Sencha Touch app.json file:

    "css": [
      {
        "path": "touch/resources/css/bb10.css",
        "platform": ["chrome", "safari", "ios", "android", "blackberry", "firefox", "ie10"],
        "theme": "Blackberry",
        "update": "delta"
      }
    ]
 
## Using the BlackBerry loading indicator

To show the Blackberry 10 loading indicator when your app launches, please insert the following `style` block into your `index.html` file:

    <style type="text/css">
        html, body {
            height: 100%;
            background-color: #fff
        }

        @keyframes spin {
            0% {
                transform:rotate(0deg);
            }
            
            100% {
                transform:rotate(360deg);
            }
        }

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform:rotate(0deg);
            }
            
            100% {
                -webkit-transform:rotate(360deg);
            }
        }

        #SpinnerContainer .SpinnerShadow,
        #SpinnerContainer .SpinnerRing,
        #SpinnerContainer .SpinnerCenter,
        #SpinnerContainer .Spinner {
            -webkit-border-radius:50%;
            -moz-border-radius:50%;
            -o-border-radius:50%;
            border-radius:50%;
        }

        #SpinnerContainer {
            position:absolute;
            top: 50%;
            left: 50%;
            width:168px;
            height:168px;
            margin-left: -84px;
            margin-top: -84px;
        }

        #SpinnerContainer .SpinnerShadow {
            position:absolute;
            top:0;
            left:0;
            width:100%;
            height:100%;
            box-shadow:0 -2px 2px rgba(0, 0, 0, 0.03), inset 0 2px 6px rgba(0, 0, 0, 0.36), inset 0 -2px 1px rgba(0, 0, 0, 0.06);
        }

        #SpinnerContainer .SpinnerRing {
            position:relative;
            border:25px solid rgba(0, 0, 0, 0.13);
        }

        #SpinnerContainer .SpinnerCenter {
            width:118px;
            height:118px;
            box-shadow:0 -2px 2px rgba(0, 0, 0, 0.06), 0 2px 6px rgba(0, 0, 0, 0.36);
        }

        #SpinnerContainer .Spinner {
            position:absolute;
            top:-25px;
            left:-25px;
            width:168px;
            height:168px;
            /* base64 image blob */
            background-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKgAAACoCAYAAAB0S6W0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAmdSURBVHja7J1riCRXGYaf6pnZ8ZYQw65rSMxmEtpL3JgbCEEWXRQlIBmCGASDqKC/FEEESUKMq0h0iQYSWEFR1B/xgigjJGoIqIg/Vo2KoGaZ6MSNZo1xN9e5T0/743zFlm1Vd11OXft94NDVlx1mup99z/mqzzkVDIdDhGgqPb0FQoIKkZPZsn5wf2mlC+9PAMwBe+y9Cm/DNmctsP/s4fEw8tgg0nasbQCbwJq1wbQKuLy4UI+gLe1N9kTavN32IrKFt+MeC4CZyP09kedHXx/e3wCeB54DngXOmMBK0CkXcn5EyiBGvrhGxsfHPd8DXgacA1wUEXcVOA38GzgFrEvQ6fh7w3ScGxGRlLdphZwk66R2LnAe0Lff8WngCeCkSStBO8JMJCFnPUjmMz2zyLwXeAVwjaXr34BHgackaPuLmzgR4o6T0rLM9MyTtOcCVwPXWrL+CXjExrIStOHjyrnIeDIYkXacpEGBLp0SJEzb9gKHgTcDy8DDwJMStHnd+GwkLZmQgqQUMo2oVJyeJPy7OeAg8AbgceC4DQEkaM2JOWuCJslSdnqWNWYt8vpLgEutmPo5cEKC1iNmL2XCFE3PLJKX2bVn/XkXAO+101QP2RCgdR9024qfuK6cAulJSll9yUOOYUBR0S8CPmhtvxK0vP9MMyk/zHEykjH9skheVWGUt70WeA3wK+DBNlT9vZak5kyK3zVLITMpPYsK52PcSUmSz1rVf6sVVBK0oJy9jB+oj/TMIlrRn1VXOw/4MPAhO68qQXPIGWR87SQxfaRn2YURFct8DfAp4EoJml3OoGB6ZqnYfZxWoiXpGffN1EeB9+G+5JCgE+QsIjQ5znf6Ss8qq/sy2mHgDuCVErScBJ0kVh3p2bb2KuCzuO/5JWiF6YnH9KSE9GySpC8GPg7clPMz6fR50KzdX9r0zPrVZPS5cJnGVqRtc3YJxwC3zGMQeV/D02PhVL8XAS+1dg7wcjtuapL2gHfhvo06Zn/vVAsa5Kjoi6Zn3PND4AXcDPY1u93J+LeEr9+ecDJ8HjgfN89zv4395muUMu59PWS/3532vkx1gqapzoumZ5ykWyZjKGZVmwVs4r4nPxUZcu0HLgYOmBhNSNPLgS8At+PWS02doEGG8WXR9Ix22y9Y22rI+7AbEfa4nUx/NfA63LzPKpMzrng6CtxGhfNNew1NT0pKzyFuuUS4tudMg+SM4xng18A3gW/jZs7v1JikF5ikF06ToGmELJqeA9xy3n/i1vC0cWnEE8ADVrD80oYkVU/hw4YdR6loVlSvBemZdmwad7xrKXTKBO3CBgnrJug9wE+sR6hKzvB4n0m6r+uCZu3O03brQ9wmCE/aGLOLO6Tt2Dj1buCnVnCVOfk5bo7pURsnd76LnzQxZNL4M3q8bt14V8UcZdsS9S7gNx7EyyLzJcAR3HqozhdJkG0ySNzJ9DPWle8yfawC3wfutSGNzxn94x6/GriFkr5x6jVExjznRqP310zObcRJ4EtWUO2WeFoqev8duHmlU1EkZUnPsAhalZf/w8DGpV/Erewsey0UwPtxa/M7IaiP9NwyOXfk49g0vdOKqbLHoz3cVL2Lu1wkpU3PtSkqgoqyCXwduC9Dlw/ZJ9cEuEkwd+EmxrRW0CLpOcTto7kh7zLzM5NnjXLOjYb3L8NN1etEgmZJz3CWkbr0/JwAPhMZl/o8bxp9/ibgLW0UNG96Diw5B3KsMP/Cnbs86Tk9R587gpvg0onTTOPSc9eqdI03/fEcblnHX8k/sXvS8+fjpue1RtC06Rl9reQsj1WT9FHP3X30ubcB17f9NFPSH7lrA3rJWR7rJuljntMz+vitVt03WtCsmzAMJWelSXqHjU2LbCeZ9Nw+4GNtLJKS2pBql10INyb9tN3mXWoddxze3oxbGdBIQbOm5wbTOdmjbk4Bn7czJb63+JnBLRVpTZGU1DZ1KqlW/gh8pWDXTsLrD1lrlKBZ0nMHzUZqAj/C7cbs+6tQgE+QcVpe1UXSuHHnltxoDPfg1m/5vpbUVcBbmyJolvTcVFHUKDaAzxG/gjRveuZK0SqLpKS2raKokZwAvlUgPZMq/tdnSdGyE3QSuxp3Npr7cJdcLLKZWtxzH2lakZTUNO5sNju4LW+GKT/PSekZ3r6RlNs79mr+49W1N58/A/fjf/fADzSlSEr6xdW1t4djxG8QAfn2XwW4AbcFZSMTdEdVe6t4BvgG+b6fT3rdPPDuJiao0rOdfA+3tLvo3v/R45ubmKCSs51sAF/zlJ7h8eX9pZWrmpSgYfcu2skPObuBbdH0DI9vbFKCSs52swl8x1N6hseL/aWVoCkJKkG7MRbd8pSeAW6r82ubkKCq3LtT0f84RXqmkTS8/866BB1dOiy6wQ9SpGfaE/sAb6+riw/ZlaCd4ne4Jct4SM8AONhfWrmwzgSVnN3jAdJdanJSeoaPHa4zQSVo97i/QIEUJ+qb6qrih2hSSBf5O/CXnF18nKjX1VXFKz27yy9ydPFJovb7Syt760hQCdp9QdOk5yRRY1O07AQdStBO83vclphpZzklpWfIlVUnqE7Md5uBSZonPeOEvqLqBFV6dp+Hx6TnuCIpLkEPVp2gErT7/DYmPZlQJJFQ/V/aX1p5SZUJqtNL3ecR0s1amjT+DH1cqLpIEt3mNO7Sk1m/OUpqlQmq9JyuFC2aniEHJKjwzUlP6RleoLYSQdW9T5+gRdMTRq5BrwQVPnjcU3oGQF8JKnxzxlN6wshW4RJU+BS0aHqG11eSoMIrpz2lJ7grCkpQ4ZV1T+n5f9709N4KDww8pWcAzCpBhW9WcfvPD0j/vXvSrjNXKEFFGfwD+GqCkGnT89jy4sJjElSUxRHOTr9Lm54hx4FPjv5ACSp8sgW8B3dBsCxXtv4DcMPy4sKmBBVl8x/cTiFfJv4yNqN7dd0LHFpeXHgq1t7hsJxapr+0oo9KHMDtonw9cBmwF3fOdAW3v9N3R8eclQkqhA/UxQsJKoQEFRJUCAkqhAQVElQICSokqBASVAgJKiSoEBJUSFAhJKgQElRIUCEkqJCgQkhQISSokKBCSFAhQfUWCAkqhAQVElQICSqEBBUSVAgJKiSoEBJUCAkqJKgQElRIUCEkqBASVEhQISSokKBCSFAhJKiQoEJIUCFBhZCgQkhQIUGFkKBCSFAhQYWQoEKCCiFBhZCgQoIKIUGFBBVCggohQYUEFUKCCgkqhAQVQoIKCSqEBBUSVAgJKoQEFRJUCAkqhAQVElQICSokqBB18d8BAHRulGuZTtiIAAAAAElFTkSuQmCC');
            background-repeat:no-repeat;
            background-position:top left;
            
            -webkit-animation:spinner 1s infinite linear;
            animation:spin 1s infinite linear;
        }
    </style>

Then add the following code to your body:

    <div id="SpinnerContainer">
        <div class="SpinnerRing">
            <div class="SpinnerCenter"></div>
            <div class="Spinner"></div>
        </div>
        <div class="SpinnerShadow"></div>
    </div>

Please ensure you remove the default animation element and styling from your index.html, and when your app launches, hide the `#SpinnerContainer`.

## Packaging, Testing, and Deploying a BlackBerry 10 App

Now that you have developed an application for BlackBerry 10 using Sencha Touch, you can run the application 
on a BlackBerry 10 Simulator or on a BlackBerry 10 device in just a few steps. A complete 
[porting guide](http://developer.blackberry.com/html5/documentation/creating_blackberry_webworks_applications_with_sencha_touch.html) 
and [developer video](http://devblog.blackberry.com/2012/11/sencha-touch-blackberry-10/)
assist you with step-by-step instructions to bring your Sencha Touch application to BlackBerry 10.

## More Information

 - [BlackBerry Developer Screen Structure](http://developer.blackberry.com/design/bb10/screen_structure.html)
 - [BlackBerry Developer Menus](http://developer.blackberry.com/design/bb10/menus.html)
