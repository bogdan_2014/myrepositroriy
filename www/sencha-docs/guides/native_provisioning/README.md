# Packaging Native iOS Applications

This guide walks through the steps required to create and install a native application
on a device running iOS for development and testing. This guide does not discuss how to 
submit your finished app to the iTunes store.  For more information, see 
the <a href="https://developer.apple.com">Apple Development Portal</a>.

These steps are required within the Apple Developer Portal to run 
an application on an iOS device:

1. Create, download, and convert an iOS Certificate.
2. Create an Application ID.
3. Register a device.
4. Create and download a Provisioning Profile.  

After you complete the Apple Developer Portal steps, then:

5. Configure the application package Configuration.
6. Generate your app.
7. Copy and run the packaged application.

<a name="CreateCert"></a>
## iOS Certificate Creation, Download and Conversion

<b>Important</b> Because an iOS Certificate contains signed information about an 
author (person and company) who creates the application, perform this step normally 
once. You can use one certificate for multiple applications.

All iOS native applications need to be bundled with iOS Certificates that
contain information about the author of the application. These certificates 
are issued and digitally signed by Apple Inc. through the Apple Developer Portal.

To generate an iOS Certificate:

1. Generate a Certificate Signing Request (CSR).
2. Navigate to the Apple Development Portal, Certificates section, and Click <b>Add</b>.
3. Upload the CSR to the Development Portal and generate an iOS Certificate
4. Download the iOS Certificate.
5. Convert to iOS Certificate to P12 format.

When developing under Windows, the steps are the same, but the methods and 
commands differ slightly.  

{@img icon-mac.png}


### Mac OS Environment

All of the tools required for your iOS Certificate are included with Mac OS, 
so creating your certificate is relatively simple and facilated using the 
Keychain Access utility. This is located in your Applications/Utility folder.


#### Generate Certificate Signing Request

Launch the KeyChain Access utility. From the application menu, choose 
<b>Keychain access</b> &gt; <b>Certificate Assistant</b> &gt; <b>Request a Certificate 
from a Certificate Authority</b>.

In the Certificate Information window, enter or select the following information:

<ul>
<li>User Email Address field: Your email address</li>
<li>Common Name field: Your name</li>
<li>Request is group: Select the <b>Save to disk</b> option</li>
<li>Click <b>Continue</b></li>
</ul>

The Certificate Assistant saves a Certificate Signing Request (CSR) file 
to your desktop.
The public and private key pair generates when you create the Certificate Signing 
Request (CSR) if you use the Key Chain Assistant to create the CSR.

#### Upload CSR and Generate iOS Certificate

After you create your CSR file, navigate to the 
[Apple Development Portal](https://developer.apple.com), Certificates section. 
To create a Certificate, click the <b>Add</b> button and select a certificate type of 
<b>iOS App Development</b>. The portal then requests you to upload your CSR file. 
After the upload completes, you can generate and download your iOS Certificate.

#### Convert Certificate to P12 Format

After you download your certificate (the default name is <tt>ios_development.cer</tt>), 
convert the certificate to P12 format before you can bundle it into your application. 
For information on what a P12 certificate is, see the Wikipedia
<a href="http://en.wikipedia.org/wiki/PKCS_%E2%99%AF12">PKCS #12</a> article.

To convert the certificate to P12 format:

<ol>
<li>In the Mac Keychain Access utility, click <b>File</b> &gt; <b>Import</b> 
and navigate to the location of the downloaded <tt>ios_development.cer</tt> file.</li>
<li>After importing the certificate file, click <b>Keys Category</b>.</li>
<li>Click the private key associated with your Apple iOS Certificate. The private 
key has the following name pattern: iPhone Developer: 
<tt>&lt;First Name&gt; &lt;Last Name&gt;</tt> public key reference.</li>
<li>Click <b>File</b> &gt; <b>Export Items</b> and save the key in P12 format.</li>
<li>Specify this file in the Sencha packager config file in the <tt>certificatePath</tt>
variable.</li> 
</ol>

{@img icon-windows.png}
### Windows Environment

OpenSSL is required for you to generate a self-signed certificate for your application. 
OpenSSL for Windows is available at 
[Shining Light Productions](http://slproweb.com/products/Win32OpenSSL.html) 
where you can download and install:

<ul>
<li>Visual C++ 2008 Redistributables (required for OpenSSL)</li>
<li>Open SSL Light (32 or 64 bit version)</li>
</ul>

By default, OpenSSL installs in the directory \OpenSSL-Win<i>XX</i> where <i>XX</i> 
is 32 or 64 depending on the version you installed. Set environment variables first 
for OpenSSL to run properly before you proceed to generate your private key required 
for creating your digital certificate.

#### Create Private Key 

The following example generates a private key, which is required for signing a 
digital certificate:
<pre>
&gt; cd \OpenSSL_Win64
&gt; set OPENSSL_CONF=\OpenSSL-Win64\bin\openssl.cfg
&gt; set RANDFILE=\OpenSSL-Win64\.rnd
&gt; openssl genrsa -out myprivatekey.key 2048
Loading 'screen' into random state - done
Generating RSA private key, 2048 bit long modulus
................................................................+++
................................................................+++
e is 65537 (0x10001)
&gt;
</pre>

<b>Note</b>: Safely store the `myprivatekey.key` file. You need this to 
generate your P12 certificate.

<a name="GenerateCSR"</a>
#### Generate Certificate Signing Request 

A CSR is a message sent to a certificate authority to request an identity
certificate. For more information, see 
<a href="http://en.wikipedia.org/wiki/Certificate_signing_request">Certificate signing request</a>.

To create your CSR, type the following <b>openssl</b> command at a command line:
<pre>
&gt; openssl req -new -key myprivatekey.key -out mycert.csr 
Loading 'screen' into random state - done
You are about to be asked to enter information that will be incorporated into your certificate request.
What you are about to enter is called a Distinguished Name or a DN.
There are quite a few fields, but you can leave some blank.
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name &lt;2 letter code&gt; [AU]: US
State or Province Name &lt;full name&gt; [Some-State]: My State
Locality Name &lt;eg, city&gt; []: My City
Organizational Name &lt;eg, company [Internet Widgits Pty Ltd]: MyCompany, Inc
Organizational Unit Name &lt;eg, section&gt; []:
Common Name &lt;eg server FQDN or Your name&gt; []: com.mydomain
Email Address [] joedeveloper@mydomain.com

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:

&gt;
</pre>

#### Upload CSR and Generate iOS Certificate

After you create your CSR file, navigate to the 
<a href="https://developer.apple.com">Apple Development Portal</a>, Certificates section. 
To create a Certificate, click the <b>Add</b> button and select the certificate type 
of <tt>iOS App Development</tt>. You are requested to upload your CSR file. 
After the upload completes, you can generate and then download your iOS Certificate.

#### Convert Certificate to P12 Format

After you download your certificate (default name is <tt>ios_development.cer</tt>),
convert the certificate to P12 format so that you can bundle it into your application. 
Assuming you are in the directory where you downloaded your iOS Certificate, convert
the certificate to P12 by executing the following openssl commands:

<pre>
&gt; openssl x509 -in ios_development.cer -inform DER -out ios_development.pem -outform PEM
&gt; openssl pkcs12 -export -inkey <i>myprivatekey.key</i> -in ios_development.pem -out ios_development.p12

Loading 'screen' into random state - done
Enter Export Password:
Verifying - Enter Export Password:
</pre>

Where <i>myprivatekey.key</i> is the keyfile you specified when you
<a href="#GenerateCSR">generated your CSR</a>. 

<b>Note</b>: If you didn't enter a challenge password in your CSR, 
press the <tt>Enter</tt> key to leave this request empty.

The resulting <tt>.P12</tt> file is the name you specified in your 
packager configuration file for the <tt>certificatePath</tt> setting.

<a name="AppID"></a>
## App ID Creation

The App ID, also known as the Bundle ID, is a unique string used to identify 
your application. Obtain the App ID from the 
<a href="https://developer.apple.com">Apple Development Portal</a>.

For development purposes, all your apps may use the same ID, 
however for production, this ID must be unique for each application you submit 
to the iTunes store. Normally the format of the ID follows reverse domain name 
convention of <tt>tld.domain</tt> (for example, <tt>com.mydomain.appname</tt>).

The App ID is used during the provisioning process to create a relationship 
between the application you're developing, and iOS certificates and devices 
permitted to run the application during testing.

<a name="DevReg"></a>
## Device Registration

(Required once for each device) - Register your device 
in the Apple Developer portal if you have not already done so. You will need 
your device's unique ID (UDID), which you can get with iTunes from your device.

To view the UDID:

<ol>
<li>Open iTunes on your computer (Mac or PC).</li>
<li>Plug in your iOS device (iPhone, iPod, etc).</li>
<li>Click its name under the devices list.</li>
<li>Ensure you're on the Summary tab.</li>
<li>Click the serial number - This changes to your device's UDID.</li>
<li>Click <b>Copy</b> from the Edit menu.</li>
</ol>

{@img udid-itunes.png}

With your UDID in the clipboard ready for use, go to the Developer Portal 
and open up the `Devices` section. 

To add your device:

<ol>
<li>Select the Devices menu option.</li>
<li>Click the [+] in the upper right hand corner to open up the 
Add iOS Devices page.</li>
<li>Enter a name for your device and its UDID (paste from clipboard).</li>
<li>Click <b>Continue</b>.</li>
</ol>

## Provisioning Profile Creation and Download

At this point, you've <a href="#CreateCert">created an iOS Certificate</a> 
with your credentials (Step 1), 
<a href="#AppID">defined an App ID</a> (Step 2) and 
<a href="#DevReg">registered your iOS device</a> (Step 3). 
You only have one step left and that is to put all these together so that 
your iOS device knows that it can trust your application. 
That's what provisioning accomplishes.

A Provisioning Profile simply links an App ID with iOS Certificates 
and devices authorized to run the application. 

To create or update a Provisioning profile:

<ol>
<li>Give your profile a name.</li>
<li>Specify the App ID for this profile along with authorized iOS Certificates 
and devices.</li>
<li>Generate and download the provisioning file 
which defaults to the name <tt><i>profileName</i>.mobileprovision</tt> 
(where <i>profileName<i> is the name you specified for the profile).</li>
<li>Specify the downloaded provisioning file path and location in your 
packager configuration file with the <tt>provisionProfile</tt> setting:
<pre>
"provisionProfile": "profileName.mobileprovision"
</pre>
</li>
</ol>

When an application loads on an iOS device, iOS compares the iOS Certificate 
that is bundled into the application and the device's UDID against the 
authorized certificates and device UDIDs found in the provisioning file. 
If either the bundled iOS Certificate or device UDID are not found, 
the application is not permitted to load onto the device.

You can have mulitple provision files, one for each project you work on, 
or use just one provision file for all your apps. If you have just started 
a new app, or still are in the early stages of development, the simple thing 
to do is to use one provision file for all development. Eventually, though, 
you may find it prudent to create specific provision files for each app as 
the AppId added in the provisioning portal is compared to what is configured 
in the app, and only one AppId is permitted per app in the App Store.

For more information about provisioning files, see the 
[Apple iOS provisioning portal](https://developer.apple.com/ios/manage/overview/index.action)
(requires an Apple ID and password).


## Package Configuration

The next step is to edit the Sencha packager file with appropriate settings for 
your environment and application testing. 

<b>Note</b> If you are using the Touch Cordova or PhoneGap features, the packager
file is not used. Refer to 
<a href="http://docs.sencha.com/touch/#!/guide/cordova">Cordova and PhoneGap app</a>
for more information.

In the root directory of your application there should be a packager configuration 
file - `packager.json'. If this file does not exist, you may easily generate one 
with this command:
<pre>
sencha app package generate packager.json
</pre>

You may now edit the packager file. The following configuration properties 
pertain to iOS development and are required for both **debug** and **release** 
configurations unless otherwise specified:

<ul>
<li><b>applicationName</b><br>  
The name you give your application. This name appears under your application's icon 
on the device's home screen.</li>

<li><b>applicationId</b><br>  
Also known as the Bundle ID, this is a unique identifier for your application. 
For development, all your apps may share the same App ID; however for production, 
the App ID must be unique for each application you submit to the iTunes store. 
Normally the format of the ID follows reverse domain name convention of 
"tld.domain" (for example, com.mydomain.appname).

The <tt>applicationId</tt> must match the application ID you 
enter when creating your App ID.</li>

<li><b>bundleSeedId</b><br>  
This value is the Prefix value found when viewing the App ID in the developer portal. 
It is a 10-character string and is the same for all your applications in your account.</li>

<li><b>configuration</b><br>  
The type of configuration for your native app build: specify <b>Debug</b> for development 
and testing, <b>Release</b> for publishing to the iTunes store.</li>

<li><b>platform</b><br>  
The target platform for your application: specify <b>iOS</b> to test on a real device 
or <b>iOSSimulator</b> to view your application in the iOS Emulator.</li>

<li><b>deviceType</b><br>  
The target device for the application. Specify <b>iPhone</b> or <b>iPad</b>.</li>

<li><b>certificatePath</b><br>  
The path to the location of your certificate file (P12 file).</li>

<li><b>provisionFile</b><br>  
The path to the location of your Provisioning file for this application.</li>

<li><b>permissions</b><br>  
It is safe to leave the permissions set to the default.</li>
</ul>

Save your packager file settings.


## Creating the Packaged Application

Build your native application in the root directory of your application:

	sencha app build native

This command reads your packager.json configuration file (if you are
not using Cordova or PhoneGap), builds your application, 
and places it in the <tt>build</tt> directory. You can locate the resulting 
<tt>app</tt> compressed file in the native build target directory 
by doing a directory listing:
<pre>
$ cd build/&lt;appname&gt;/native/
$ ls
&lt;applicationName&gt;.app
$
</pre>

Where &lt;appname&gt; is the application name you specified when you ran your 
<tt>sencha generate app</tt> command and &lt;applicationName&gt; is the 
value you specified for the <tt>applicationName</tt> property 
in your <tt>packager.json</tt> file.


## Copy and Run your Application

Now the moment you've been waiting for to test your application on an iOS device!

To load your application onto your device: 

<ol>
<li>Launch iTunes and select the <b>Apps</b> screen in your Library.</li>
<li>Bring up a file browser window and locate the <tt>.app</tt> directory where 
your native application was built.</li>
<li>Drag the <tt>.app</tt> directory the the Apps screen in iTunes.</li>
<li>Select your iOS device under the Devices section of iTunes.</li>
<li>Click the <b>Apps</b> tab on your device.</li>
<li>Locate the app you just copied over to your Apps Library.</li>
<li>Click <b>Install.</li>
<li>Click the <b>Apply</b>button to copy the app from your Library to your iOS device.</li>
</ol>

Now switch to your device, find your application and press the icon. 
You should have your basic application all up and running.
