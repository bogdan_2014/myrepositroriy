<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>The source code</title>
  <link href="../resources/prettify/prettify.css" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="../resources/prettify/prettify.js"></script>
  <style type="text/css">
    .highlight { display: block; background-color: #ddd; }
  </style>
  <script type="text/javascript">
    function highlight() {
      document.getElementById(location.hash.replace(/#/, "")).className = "highlight";
    }
  </script>
</head>
<body onload="prettyPrint(); highlight();">
  <pre class="prettyprint lang-js"><span id='Ext-grid-Grid'>/**
</span> * @author Tommy Maintz
 *
 * Grids are an excellent way of showing large amounts of tabular data on the client side. Essentially a supercharged
 * `&lt;table&gt;`, Grid makes it easy to fetch, sort and filter large amounts of data.
 *
 * Grids are composed of two main pieces - a {@link Ext.data.Store Store} full of data and a set of columns to render.
 *
 * ## Basic GridPanel
 *
 *     Ext.create('Ext.data.Store', {
 *         storeId: 'simpsonsStore',
 *         fields: ['name', 'email', 'phone'],
 *         data: {'items': [
 *             { 'name': 'Lisa',  &quot;email&quot;:&quot;lisa@simpsons.com&quot;,  &quot;phone&quot;:&quot;555-111-1224&quot;  },
 *             { 'name': 'Bart',  &quot;email&quot;:&quot;bart@simpsons.com&quot;,  &quot;phone&quot;:&quot;555-222-1234&quot; },
 *             { 'name': 'Homer', &quot;email&quot;:&quot;home@simpsons.com&quot;,  &quot;phone&quot;:&quot;555-222-1244&quot;  },
 *             { 'name': 'Marge', &quot;email&quot;:&quot;marge@simpsons.com&quot;, &quot;phone&quot;:&quot;555-222-1254&quot;  }
 *         ]}
 *     });
 *
 *     Ext.create('Ext.grid.Grid', {
 *         title: 'Simpsons',
 *         store: Ext.data.StoreManager.lookup('simpsonsStore'),
 *         columns: [
 *             { text: 'Name',  dataIndex: 'name', width: 200},
 *             { text: 'Email', dataIndex: 'email', width: 250},
 *             { text: 'Phone', dataIndex: 'phone', width: 120}
 *         ],
 *         height: 200,
 *         width: 400,
 *         renderTo: Ext.getBody()
 *     });
 *
 * The code above produces a simple grid with three columns. We specified a Store which will load JSON data inline.
 * In most apps we would be placing the grid inside another container and wouldn't need to use the
 * {@link #height}, {@link #width} and {@link #renderTo} configurations but they are included here to make it easy to get
 * up and running.
 *
 * The grid we created above will contain a header bar with a title ('Simpsons'), a row of column headers directly underneath
 * and finally the grid rows under the headers.
 *
 * ## Configuring columns
 *
 * By default, each column is sortable and will toggle between ASC and DESC sorting when you click on its header.
 * It's easy to configure each column - here we use the same example as above and just modify the columns config:
 *
 *     columns: [
 *         {
 *             text: 'Name',
 *             dataIndex: 'name',
 *             sortable: false,
 *             width: 250
 *         },
 *         {
 *             text: 'Email',
 *             dataIndex: 'email',
 *             hidden: true
 *         },
 *         {
 *             text: 'Phone',
 *             dataIndex: 'phone',
 *             width: 100
 *         }
 *     ]
 *
 * We turned off sorting on the 'Name' column so clicking its header now has no effect. We also made the Email
 * column hidden by default (it can be shown again by using the {@link Ext.grid.plugin.ViewOptions ViewOptions} plugin).
 * See the {@link Ext.grid.column.Column column docs} for more details.
 *
 * ## Renderers
 *
 * As well as customizing columns, it's easy to alter the rendering of individual cells using renderers. A renderer is
 * tied to a particular column and is passed the value that would be rendered into each cell in that column. For example,
 * we could define a renderer function for the email column to turn each email address into a mailto link:
 *
 *     columns: [
 *         {
 *             text: 'Email',
 *             dataIndex: 'email',
 *             renderer: function(value) {
 *                 return Ext.String.format('&lt;a href=&quot;mailto:{0}&quot;&gt;{1}&lt;/a&gt;', value, value);
 *             }
 *         }
 *     ]
 *
 * See the {@link Ext.grid.column.Column column docs} for more information on renderers.
 *
 * ## Sorting &amp; Filtering
 *
 * Every grid is attached to a {@link Ext.data.Store Store}, which provides multi-sort and filtering capabilities. It's
 * easy to set up a grid to be sorted from the start:
 *
 *     var myGrid = Ext.create('Ext.grid.Panel', {
 *         store: {
 *             fields: ['name', 'email', 'phone'],
 *             sorters: ['name', 'phone']
 *         },
 *         columns: [
 *             { text: 'Name',  dataIndex: 'name' },
 *             { text: 'Email', dataIndex: 'email' }
 *         ]
 *     });
 *
 * Sorting at run time is easily accomplished by simply clicking each column header. If you need to perform sorting on
 * more than one field at run time it's easy to do so by adding new sorters to the store:
 *
 *     myGrid.store.sort([
 *         { property: 'name',  direction: 'ASC' },
 *         { property: 'email', direction: 'DESC' }
 *     ]);
 *
 * See {@link Ext.data.Store} for examples of filtering.
 *
 * ## Plugins and Features
 *
 * Grid supports addition of extra functionality through plugins:
 *
 * - {@link Ext.grid.plugin.ViewOptions ViewOptions} - adds the ability to show/hide columns and reorder them.
 *
 * - {@link Ext.grid.plugin.ColumnResizing ColumnResizing} - allows for the ability to pinch to resize columns.
 *
 * - {@link Ext.grid.plugin.Editable Editable} - editing grid contents an entire row at a time.
 *
 * - {@link Ext.grid.plugin.MultiSelection MultiSelection} - selecting and deleting several rows at a time.
 *
 * - {@link Ext.grid.plugin.PagingToolbar PagingToolbar} - adds a toolbar at the bottom of the grid that allows you to quickly navigate to another page of data.
 *
 * - {@link Ext.grid.plugin.SummaryRow SummaryRow} - adds and pins an additional row to the top of the grid that enables you to display summary data.
 */
Ext.define('Ext.grid.Grid', {
    extend: 'Ext.List',

    requires: [
        'Ext.grid.Row',
        'Ext.grid.column.Column',
        'Ext.grid.column.Date',
        'Ext.grid.column.Template',
        'Ext.grid.HeaderContainer',
        'Ext.grid.HeaderGroup',
        'Ext.TitleBar',
        'Ext.MessageBox'
    ],

    xtype: 'grid',

    config: {
<span id='Ext-grid-Grid-cfg-defaultType'>        defaultType: 'gridrow',
</span>
<span id='Ext-grid-Grid-cfg-infinite'>        /**
</span>         * @cfg {Boolean} infinite
         * This List configuration should always be set to true on a Grid.
         * @hide
         */
        infinite: true,

<span id='Ext-grid-Grid-cfg-columns'>        /**
</span>         * @cfg {Ext.grid.column.Column[]} columns (required)
         * An array of column definition objects which define all columns that appear in this grid.
         * Each column definition provides the header text for the column, and a definition of where
         * the data for that column comes from.
         *
         * This can also be a configuration object for a {Ext.grid.header.Container HeaderContainer}
         * which may override certain default configurations if necessary. For example, the special
         * layout may be overridden to use a simpler layout, or one can set default values shared
         * by all columns:
         *
         *      columns: {
         *          items: [
         *              {
         *                  text: &quot;Column A&quot;
         *                  dataIndex: &quot;field_A&quot;,
         *                  width: 200
         *              },{
         *                  text: &quot;Column B&quot;,
         *                  dataIndex: &quot;field_B&quot;,
         *                  width: 150
         *              },
         *              ...
         *          ]
         *      }
         *
         */
        columns: null,

<span id='Ext-grid-Grid-cfg-baseCls'>        /**
</span>         * @cfg baseCls
         * @inheritdoc
         */
        baseCls: Ext.baseCSSPrefix + 'grid',

<span id='Ext-grid-Grid-cfg-useHeaders'>        /**
</span>         * @cfg {Boolean} useHeaders
         * @hide
         */
        useHeaders: false,

<span id='Ext-grid-Grid-cfg-itemHeight'>        itemHeight: 60,
</span>
<span id='Ext-grid-Grid-cfg-variableHeights'>        /**
</span>         * @cfg {Boolean} variableHeights
         * This configuration is best left to false on a Grid for performance reasons.
         */
        variableHeights: false,

<span id='Ext-grid-Grid-cfg-headerContainer'>        headerContainer: {
</span>            xtype: 'headercontainer'
        },

<span id='Ext-grid-Grid-cfg-striped'>        /**
</span>         * @cfg {Boolean} striped
         * @inherit
         */
        striped: true,

<span id='Ext-grid-Grid-cfg-itemCls'>        itemCls: Ext.baseCSSPrefix + 'list-item',
</span><span id='Ext-grid-Grid-cfg-scrollToTopOnRefresh'>        scrollToTopOnRefresh: false,
</span>
<span id='Ext-grid-Grid-cfg-titleBar'>        titleBar: {
</span>            xtype: 'titlebar',
            docked: 'top'
        },

<span id='Ext-grid-Grid-cfg-title'>        /**
</span>         * @cfg {String} title
         * The title that will be displayed in the TitleBar at the top of this Grid.
         */
        title: ''
    },

<span id='Ext-grid-Grid-event-columnadd'>    /**
</span>     * @event columnadd
     * Fires whenever a column is added to the Grid.
     * @param {Ext.grid.Grid} this The Grid instance
     * @param {Ext.grid.column.Column} column The added column
     * @param {Number} index The index of the added column
     * @param {Ext.EventObject} e The event object
     */

<span id='Ext-grid-Grid-event-columnremove'>    /**
</span>     * @event columnremove
     * Fires whenever a column is removed from the Grid.
     * @param {Ext.grid.Grid} this The Grid instance
     * @param {Ext.grid.column.Column} column The removed column
     * @param {Ext.EventObject} e The event object
     */

<span id='Ext-grid-Grid-event-columnshow'>    /**
</span>     * @event columnshow
     * Fires whenever a column is shown in the Grid
     * @param {Ext.grid.Grid} this The Grid instance
     * @param {Ext.grid.column.Column} column The shown column
     * @param {Ext.EventObject} e The event object
     */

<span id='Ext-grid-Grid-event-columnhide'>    /**
</span>     * @event columnhide
     * Fires whenever a column is hidden in the Grid.
     * @param {Ext.grid.Grid} this The Grid instance
     * @param {Ext.grid.column.Column} column The shown column
     * @param {Ext.EventObject} e The event object
     */

<span id='Ext-grid-Grid-event-columnresize'>    /**
</span>     * @event columnresize
     * Fires whenever a column is resized in the Grid.
     * @param {Ext.grid.Grid} this The Grid instance
     * @param {Ext.grid.column.Column} column The resized column
     * @param {Number} width The new column width
     * @param {Ext.EventObject} e The event object
     */

<span id='Ext-grid-Grid-event-columnsort'>    /**
</span>     * @event columnsort
     * Fires whenever a column is sorted in the Grid
     * @param {Ext.grid.Grid} this The Grid instance
     * @param {Ext.grid.column.Column} column The sorted column
     * @param {String} direction The direction of the sort on this Column. Either 'asc' or 'desc'
     * @param {Ext.EventObject} e The event object
     */

    platformConfig: [{
        theme: ['Windows'],
        itemHeight: 60
    }],

<span id='Ext-grid-Grid-method-beforeInitialize'>    beforeInitialize: function() {
</span>        this.container = Ext.factory({
            xtype: 'container',
            scrollable: {
                scroller: {
                    autoRefresh: false,
                    direction: 'auto',
                    directionLock: true
                }
            }
        });

        this.callParent();
    },

<span id='Ext-grid-Grid-method-initialize'>    initialize: function() {
</span>        var me = this,
            titleBar = me.getTitleBar(),
            headerContainer = me.getHeaderContainer();

        me.callParent();

        if (titleBar) {
            me.container.add(me.getTitleBar());
        }
        me.container.doAdd(headerContainer);

        me.scrollElement.addCls(Ext.baseCSSPrefix + 'grid-scrollelement');
    },

<span id='Ext-grid-Grid-method-onTranslate'>    onTranslate: function(x) {
</span>        this.callParent(arguments);
        this.getHeaderContainer().scrollTo(x);
    },

<span id='Ext-grid-Grid-method-applyTitleBar'>    applyTitleBar: function(titleBar) {
</span>        if (titleBar &amp;&amp; !titleBar.isComponent) {
            titleBar = Ext.factory(titleBar, Ext.TitleBar);
        }
        return titleBar;
    },

<span id='Ext-grid-Grid-method-updateTitle'>    updateTitle: function(title) {
</span>        var titleBar = this.getTitleBar();
        if (titleBar) {
            this.getTitleBar().setTitle(title);
        }
    },

<span id='Ext-grid-Grid-method-applyHeaderContainer'>    applyHeaderContainer: function(headerContainer) {
</span>        if (headerContainer &amp;&amp; !headerContainer.isComponent) {
            headerContainer = Ext.factory(headerContainer, Ext.grid.HeaderContainer);
        }
        return headerContainer;
    },

<span id='Ext-grid-Grid-method-updateHeaderContainer'>    updateHeaderContainer: function(headerContainer, oldHeaderContainer) {
</span>        var me = this;

        if (oldHeaderContainer) {
            oldHeaderContainer.un({
                columnsort: 'onColumnSort',
                columnresize: 'onColumnResize',
                columnshow: 'onColumnShow',
                columnhide: 'onColumnHide',
                columnadd: 'onColumnAdd',
                columnremove: 'onColumnRemove',
                scope: me
            });
        }

        if (headerContainer) {
            headerContainer.on({
                columnsort: 'onColumnSort',
                columnresize: 'onColumnResize',
                columnshow: 'onColumnShow',
                columnhide: 'onColumnHide',
                columnadd: 'onColumnAdd',
                columnremove: 'onColumnRemove',
                scope: me
            });
        }
    },

<span id='Ext-grid-Grid-method-addColumn'>    addColumn: function(column) {
</span>        this.getHeaderContainer().add(column);
    },

<span id='Ext-grid-Grid-method-removeColumn'>    removeColumn: function(column) {
</span>        this.getHeaderContainer().remove(column);
    },

<span id='Ext-grid-Grid-method-insertColumn'>    insertColumn: function(index, column) {
</span>        this.getHeaderContainer().insert(index, column);
    },

<span id='Ext-grid-Grid-method-onColumnAdd'>    onColumnAdd: function(container, column) {
</span>        if (this.isPainted()) {
            var items = this.listItems,
                ln = items.length,
                columnIndex = container.getColumns().indexOf(column),
                i, row;

            for (i = 0; i &lt; ln; i++) {
                row = items[i];
                row.insertColumn(columnIndex, column);
            }

            this.updateTotalColumnWidth();

            this.fireEvent('columnadd', this, column, columnIndex);
        }
    },

<span id='Ext-grid-Grid-method-onColumnRemove'>    onColumnRemove: function(container, column) {
</span>        if (this.isPainted()) {
            var items = this.listItems,
                ln = items.length,
                i, row;

            for (i = 0; i &lt; ln; i++) {
                row = items[i];
                row.removeColumn(column);
            }

            this.updateTotalColumnWidth();

            this.fireEvent('columnremove', this, column);
        }
    },

<span id='Ext-grid-Grid-method-updateColumns'>    updateColumns: function(columns) {
</span>        if (columns &amp;&amp; columns.length) {
            var ln = columns.length,
                i;

            for (i = 0; i &lt; ln; i++) {
                this.addColumn(columns[i]);
            }

            this.updateTotalColumnWidth();
        }
    },

<span id='Ext-grid-Grid-method-getColumns'>    getColumns: function() {
</span>        return this.getHeaderContainer().getColumns();
    },

<span id='Ext-grid-Grid-method-onColumnResize'>    onColumnResize: function(container, column, width) {
</span>        var items = this.listItems,
            ln = items.length,
            i, row;

        for (i = 0; i &lt; ln; i++) {
            row = items[i];
            row.setColumnWidth(column, width);
        }
        this.updateTotalColumnWidth();

        this.fireEvent('columnresize', column, width);
    },

<span id='Ext-grid-Grid-method-onColumnShow'>    onColumnShow: function(container, column) {
</span>        var items = this.listItems,
            ln = items.length,
            i, row;

        this.updateTotalColumnWidth();
        for (i = 0; i &lt; ln; i++) {
            row = items[i];
            row.showColumn(column);
        }

        this.fireEvent('columnshow', this, column);
    },

<span id='Ext-grid-Grid-method-onColumnHide'>    onColumnHide: function(container, column) {
</span>        var items = this.listItems,
            ln = items.length,
            i, row;

        for (i = 0; i &lt; ln; i++) {
            row = items[i];
            row.hideColumn(column);
        }
        this.updateTotalColumnWidth();

        this.fireEvent('columnhide', this, column);
    },

<span id='Ext-grid-Grid-method-onColumnSort'>    onColumnSort: function(container, column, direction) {
</span>        if (this.sortedColumn &amp;&amp; this.sortedColumn !== column) {
            this.sortedColumn.setSortDirection(null);
        }
        this.sortedColumn = column;

        this.getStore().sort(column.getDataIndex(), direction);

        this.fireEvent('columnsort', this, column, direction);
    },

<span id='Ext-grid-Grid-method-getTotalColumnWidth'>    getTotalColumnWidth: function() {
</span>        var me = this,
            columns = me.getColumns(),
            ln = columns.length,
            totalWidth = 0,
            i, column, parent;


        for (i = 0; i &lt; ln; i++) {
            column = columns[i];
            parent = column.getParent();

            if (!column.isHidden() &amp;&amp; (!parent.isHeaderGroup || !parent.isHidden())) {
                totalWidth += column.getWidth();
            }
        }

        return totalWidth;
    },

<span id='Ext-grid-Grid-method-updateTotalColumnWidth'>    updateTotalColumnWidth: function() {
</span>        var me = this,
            scroller = me.getScrollable().getScroller(),
            totalWidth = this.getTotalColumnWidth();

        me.scrollElement.setWidth(totalWidth);

        scroller.setSize({
            x: totalWidth,
            y: scroller.getSize().y
        });
        scroller.refresh();
    },

<span id='Ext-grid-Grid-method-setScrollerHeight'>    setScrollerHeight: function(height) {
</span>        var me = this,
            scroller = me.container.getScrollable().getScroller();

        if (height != scroller.givenSize.y) {
            scroller.setSize({
                x: scroller.givenSize.x,
                y: height
            });
            scroller.refresh();
        }
    },

<span id='Ext-grid-Grid-method-createItem'>    createItem: function(config) {
</span>        var me = this,
            container = me.container,
            listItems = me.listItems,
            item;

        config.grid = me;
        item = Ext.factory(config);
        item.dataview = me;
        item.$height = config.minHeight;

        container.doAdd(item);
        listItems.push(item);

        return item;
    }
});
</pre>
</body>
</html>
